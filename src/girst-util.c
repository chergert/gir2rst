/* girst-util.c
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "girst-util"

#include <string.h>

#include "parser/girst-array.h"
#include "parser/girst-class.h"
#include "parser/girst-doc.h"
#include "parser/girst-glib-signal.h"
#include "parser/girst-instance-parameter.h"
#include "parser/girst-implements.h"
#include "parser/girst-parameter.h"
#include "parser/girst-prerequisite.h"
#include "parser/girst-record.h"
#include "parser/girst-return-value.h"
#include "parser/girst-type.h"
#include "parser/girst-virtual-method.h"

#include "girst-cache.h"
#include "girst-util.h"

typedef enum
{
  GTK_DOC_TOKEN_NONE       = 0,
  GTK_DOC_TOKEN_VAR        = 1,
  GTK_DOC_TOKEN_FUNC       = 2,
  GTK_DOC_TOKEN_MACRO      = 3,
  GTK_DOC_TOKEN_SPACE      = 4,
  GTK_DOC_TOKEN_TYPE       = 5,
  GTK_DOC_TOKEN_CODE       = 6,
  GTK_DOC_TOKEN_SYMBOL     = 7,
  GTK_DOC_TOKEN_MD_TITLE   = 8,
  GTK_DOC_TOKEN_PREFORMAT  = 9,
} GtkDocTokenType;

static gchar *
cleanup_code_c (const gchar *input)
{
  GString *str = g_string_new (NULL);
  g_autofree gchar *lang = NULL;
  const gchar *tmp;

  if ((tmp = strstr (input, "<!-- language=\"")) ||
      (tmp = strstr (input, "<!-- Language=\"")))
    {
      const gchar *endptr;

      g_string_append_len (str, input, tmp - input);

      input = tmp + strlen ("<!-- language=\"");

      if ((endptr = strchr (input, '"')))
        {
          lang = g_strndup (input, endptr - input);
          input = strstr (input, "-->");
          if (input)
            input += strlen ("-->");
        }

      if (input)
        g_string_append (str, input);
    }
  else
    g_string_append (str, input);

  return g_string_free (str, FALSE);
}

static gchar *
gtk_doc_next_token (const gchar     **ptr,
                    GtkDocTokenType  *type)
{
  const gchar *iter = *ptr;
  const gchar *begin = *ptr;
  const gchar *tmp;
  gunichar ch;
  gchar *ret = NULL;

  *type = 0;

  if (*iter == '\0')
    return NULL;

  ch = g_utf8_get_char (iter);

  /* Handle escape char immediately */
  if (ch == '\\')
    {
      gchar esc[8] = {0};
      iter = g_utf8_next_char (iter);
      ch = g_utf8_get_char (iter);
      if (ch != 0)
        g_unichar_to_utf8 (ch, esc);
      *ptr = g_utf8_next_char (iter);
      *type = GTK_DOC_TOKEN_NONE;
      return g_strdup (esc);
    }

  while (g_unichar_isspace (ch))
    {
      /* read up to next non-white-space */
      iter = g_utf8_next_char (iter);
      ch = g_utf8_get_char (iter);
    }

  /* we read some wite space, return it */
  if (iter != begin)
    {
      *ptr = iter;
      *type = GTK_DOC_TOKEN_SPACE;
      return g_strndup (begin, iter - begin);
    }

  /* Special case to eat * as a single token */
  if (*iter == '*')
    {
      *type = GTK_DOC_TOKEN_NONE;
      *ptr = ++iter;
      return g_strdup ("*");
    }

  /* If we see |[, then we are starting a code
   * example block, and we need to consume until
   * we find the matching ]|
   */
  if (g_str_has_prefix (iter, "|[") && (tmp = strstr (iter, "]|")))
    {
      g_autofree gchar *unformat = NULL;

      *type = GTK_DOC_TOKEN_CODE;
      *ptr = &tmp[2];
      iter += 2;
      unformat = g_strndup (iter, tmp - iter);
      return cleanup_code_c (unformat);
    }

  /* sometimes we have markdown titles */
  if (g_str_has_prefix (iter, "# "))
    {
      const gchar *endptr = strchr (iter, '\n');

      if (endptr)
        {
          iter += 2;
          *ptr = endptr;
          *type = GTK_DOC_TOKEN_MD_TITLE;
          return g_strstrip (g_strndup (iter, endptr - iter));
        }
    }

  /* If we are at a `, locate the end ` and return it as preformatted
   * token for special formatting.
   */
  if (ch == '`')
    {
      const gchar *tmp;

      if (!*(++iter))
        return NULL;

      tmp = iter + 1;
      while (*tmp && *tmp != '`')
        tmp = g_utf8_next_char (tmp);

      *type = GTK_DOC_TOKEN_PREFORMAT;
      ret = g_strndup (iter, tmp - iter);

      if (*tmp == '`')
        tmp++;
      *ptr = tmp ;

      return ret;
    }

  /* Read up to whitespace for next word */
  while (ch && !g_unichar_isspace (ch))
    {
      /* if @... gets used, so swallow if necessary */
      if (strncmp (iter, "@...", 4) == 0)
        {
          iter += 4;
          break;
        }

      /* read up to next non-white-space */
      iter = g_utf8_next_char (iter);
      ch = g_utf8_get_char (iter);

      /* special case periods as they can
       * mess up our has_suffix("()") checks
       */
      switch (ch)
        {
        case '.':
        case ',':
        case ';':
        case '*':
        case '-':
        case '%':
        case '\\':
        case '`':
        case '\'':
        case '"':
          goto skip;

        /* don't walk past end of first ) in foo()) */
        case ')':
          iter++;
          goto skip;

        default: ;
        }
    }

skip:
  g_assert (begin != iter);

  ret = g_strndup (begin, iter - begin);
  *ptr = iter;
  *type = GTK_DOC_TOKEN_NONE;

  if (*ret == '#')
    {
      *ret = ' ';
      g_strchug (ret);
      *type = GTK_DOC_TOKEN_TYPE;
    }
  else if (*ret == '%')
    {
      *ret = ' ';
      g_strchug (ret);
      *type = GTK_DOC_TOKEN_SYMBOL;
    }
  else if (*ret == '@')
    {
      *ret = ' ';
      g_strchug (ret);
      *type = GTK_DOC_TOKEN_VAR;
    }
  else if (g_str_has_suffix (ret, "()"))
    {
      guint len = strlen (ret);

      if (g_unichar_isupper (g_utf8_get_char (ret)))
        *type = GTK_DOC_TOKEN_MACRO;
      else
        *type = GTK_DOC_TOKEN_FUNC;

      /* Strip trailing (), we don't need it */
      g_assert (len >= 2);
      ret[len - 2] = '\0';
    }

  return ret;
}

static gchar *
linkify_text_body_c (const gchar *text,
                     const gchar *line_prefix)
{
  GtkDocTokenType type;
  gchar *token;
  GString *str;

  g_assert (text != NULL);

  if (line_prefix == NULL)
    line_prefix = "";

  str = g_string_new (line_prefix);

  for (token = gtk_doc_next_token (&text, &type);
       token != NULL;
       token = gtk_doc_next_token (&text, &type))
    {
      switch (type)
        {
        case GTK_DOC_TOKEN_VAR:
          g_string_append_printf (str, ":ref:`%s <C:%s>`", token, token);
          break;

        case GTK_DOC_TOKEN_FUNC:
          /* TODO: Translate vfunc/func/ctor/etc */
          g_string_append_printf (str, ":ref:`%s() <C:%s>`", token, token);
          break;

        case GTK_DOC_TOKEN_MACRO:
          g_string_append_printf (str, ":ref:`%s() <C:%s>`", token, token);
          break;

        case GTK_DOC_TOKEN_SYMBOL:
          g_string_append_printf (str, ":ref:`%s <C:%s>`", token, token);
          break;

        case GTK_DOC_TOKEN_TYPE:
          if (g_str_equal (token, "TRUE") ||
              g_str_equal (token, "FALSE") ||
              g_str_equal (token, "NULL") ||
              g_str_equal (token, "MIN") ||
              g_str_equal (token, "MAX"))
            g_string_append (str, token);
          else
            g_string_append_printf (str, ":ref:`%s <C:%s>`", token, token);
          break;

        case GTK_DOC_TOKEN_CODE:
          {
            g_auto(GStrv) lines = g_strsplit (g_strstrip (token), "\n", 0);

            g_string_append_printf (str, "\n.. code-block:: c\n");
            g_string_append (str, "   :linenos:\n\n");
            for (guint i = 0; lines[i]; i++)
              g_string_append_printf (str, "   %s\n", lines[i]);
            g_string_append (str, "..\n");
          }
          break;

        case GTK_DOC_TOKEN_MD_TITLE:
          g_string_append_printf (str, "**%s**\n", token);
          break;

        case GTK_DOC_TOKEN_NONE:
          if (*token == '*' || *token == '`' || *token == '\\')
            g_string_append_printf (str, "\\%s ", token);
          else
            g_string_append (str, token);
          break;

        case GTK_DOC_TOKEN_PREFORMAT:
          g_string_append_printf (str, "``%s``", token);
          break;

        case GTK_DOC_TOKEN_SPACE:
        default:
          /* If we have space after a newline, strip it */
          if (strchr (token, '\n'))
            {
              const gchar *iter = token;
              while ((iter = strchr (iter, '\n')))
                {
                  g_string_append (str, "\n");
                  g_string_append (str, line_prefix);
                  iter++;
                }
            }
          else
            g_string_append (str, token);
          break;
        }

      g_free (token);
    }

  return g_string_free (str, FALSE);
}

static gchar *
girst_gtk_doc_c (const gchar *text,
                 const gchar *line_prefix)
{
  return linkify_text_body_c (text, line_prefix);
}

gchar *
girst_doc_to_c (GirstDoc    *doc,
                const gchar *line_prefix)
{
  return girst_gtk_doc_c (girst_doc_get_inner_text (doc), line_prefix);
}

/**
 * girst_gtk_doc:
 * @text: the input text
 * @line_prefix: the prefix for each line
 * @language: the language to use
 *
 * Formats @text, which should be gtk-doc formatted text, into RST
 * suitable text based on the specified language.
 *
 * Returns: (not nullable) (transfer full): A newly allocated string
 */
gchar *
girst_gtk_doc (const gchar   *text,
               const gchar   *line_prefix,
               GirstLanguage  language)
{
  if (!text || !*text || language != GIRST_LANGUAGE_C)
    return g_strdup ("");

  return girst_gtk_doc_c (text, line_prefix);
}

static gchar *
girst_one_line_c (const gchar *text)
{
  gchar *linkify;

  if (text == NULL)
    return g_strdup ("");

  linkify = linkify_text_body_c (text, "");

  g_strdelimit (linkify, "\"", '\'');
  g_strdelimit (linkify, "\n", ' ');

  return g_strstrip (linkify);
}

gchar *
girst_one_line (const gchar   *text,
                GirstLanguage  language)
{
  if (!text || !*text || language != GIRST_LANGUAGE_C)
    return g_strdup ("");

  return girst_one_line_c (text);
}

gchar *
girst_stringify (const gchar *type,
                 const gchar *value)
{
  if (g_strcmp0 (type, "gchar*") == 0)
    return g_strdup_printf ("\"%s\"", value);
  return g_strdup (value);
}

gchar *
girst_title1 (const gchar *title)
{
  if (title && *title)
    {
      GString *str = g_string_new (NULL);
      guint len = g_utf8_strlen (title, -1);

      for (guint i = 0; i < len; i++)
        g_string_append_c (str, '#');
      g_string_append_c (str, '\n');
      g_string_append (str, title);
      g_string_append_c (str, '\n');
      for (guint i = 0; i < len; i++)
        g_string_append_c (str, '#');
      g_string_append_c (str, '\n');

      return g_string_free (str, FALSE);
    }

  return g_strdup ("");
}

gchar *
girst_title_with_char (const gchar *title,
                       gchar        ch)
{
  if (title && *title)
    {
      GString *str = g_string_new (NULL);
      guint len = g_utf8_strlen (title, -1);

      g_string_append (str, title);
      g_string_append_c (str, '\n');
      for (guint i = 0; i < len; i++)
        g_string_append_c (str, ch);
      g_string_append_c (str, '\n');

      return g_string_free (str, FALSE);
    }

  return g_strdup ("");
}

gchar *
girst_title2 (const gchar *title)
{
  return girst_title_with_char (title, '=');
}

gchar *
girst_title3 (const gchar *title)
{
  return girst_title_with_char (title, '-');
}

gchar *
girst_title4 (const gchar *title)
{
  return girst_title_with_char (title, '^');
}

gchar *
girst_type_to_c (GirstType *type)
{
  GirstParserObject *parent;
  const gchar *c_type;
  const gchar *name;

  g_return_val_if_fail (GIRST_IS_TYPE (type), NULL);

  c_type = girst_type_get_c_type (type);
  if (c_type != NULL)
    return g_strdup (c_type);

  name = girst_type_get_name (type);

  for (parent = GIRST_PARSER_OBJECT (type);
       parent != NULL;
       parent = girst_parser_object_get_parent (parent))
    {
      if (GIRST_IS_NAMESPACE (parent))
        return girst_resolve_type (GIRST_NAMESPACE (parent), name, GIRST_LANGUAGE_C);
    }

  return g_strdup (name);
}

gchar *
girst_property_type_name (GirstProperty *prop,
                          GirstLanguage  language)
{
  GirstParserObject *ar;
  GirstParserObject *type = NULL;

  g_return_val_if_fail (GIRST_IS_PROPERTY (prop), NULL);

  if (language != GIRST_LANGUAGE_C)
    return NULL;

  ar = girst_parser_object_first_typed (GIRST_PARSER_OBJECT (prop), GIRST_TYPE_ARRAY);

  if (ar != NULL)
    {
      g_autofree gchar *c_type = NULL;

      type = girst_parser_object_first_typed (ar, GIRST_TYPE_TYPE);

      if (type != NULL)
        {
          if (girst_str_equal0 (girst_type_get_name (GIRST_TYPE (type)), "utf8"))
            return g_strdup ("gchar**");
        }
    }
  else
    {
      type = girst_parser_object_first_typed (GIRST_PARSER_OBJECT (prop), GIRST_TYPE_TYPE);
    }


  if (type != NULL)
    {
      /* TODO: Need better type resolving here */
      return girst_type_to_c (GIRST_TYPE (type));
    }

  return NULL;
}

gchar *
girst_glib_signal_type_flags (GirstGlibSignal *self)
{
  GString *str = g_string_new (NULL);
  const gchar *when;

  when = girst_glib_signal_get_when (self);

  if (girst_str_equal0 (when, "last"))
    g_string_append (str, ":ref:`Run Last <C:GObject.SignalFlags.run_last>`");
  else if (girst_str_equal0 (when, "first"))
    g_string_append (str, ":ref:`Run First <C:GObject.SignalFlags.run_first>`");
  else if (girst_str_equal0 (when, "cleanup"))
    g_string_append (str, ":ref:`Run in Cleanup <C:GObject.SignalFlags.run_cleanup>`");

  if (girst_str_equal0 (girst_glib_signal_get_action (self), "1"))
    g_string_append (str, " / :ref:`Action <C:GObject.SignalFlags.action>`");

  if (girst_str_equal0 (girst_glib_signal_get_detailed (self), "1"))
    g_string_append (str, " / :ref:`Has Details <C:GObject.SignalFlags.detailed>`");

  if (girst_str_equal0 (girst_glib_signal_get_no_recurse (self), "1"))
    g_string_append (str, " / :ref:`No Recurse <C:GObject.SignalFlags.no_recurse>`");

  if (girst_str_equal0 (girst_glib_signal_get_no_hooks (self), "1"))
    g_string_append (str, " / :ref:`No Hooks <C:GObject.SignalFlags.no_hooks>`");

  return g_string_free (str, FALSE);
}

gchar *
girst_property_type_flags (GirstProperty *prop)
{
  GString *str = g_string_new (NULL);

  if (g_strcmp0 (girst_property_get_readable (prop), "0") != 0)
    g_string_append (str, "Read");

  if (g_strcmp0 (girst_property_get_writable (prop), "1") == 0)
    g_string_append_printf (str, "%sWrite", str->len ? " / " : "");

  if (g_strcmp0 (girst_property_get_construct_only (prop), "1") == 0)
    g_string_append_printf (str, "%sConstruct Only", str->len ? " / " : "");
  else if (g_strcmp0 (girst_property_get_construct (prop), "1") == 0)
    g_string_append_printf (str, "%sConstruct", str->len ? " / " : "");

  return g_string_free (str, FALSE);
}

static gpointer
find_ancestor (GirstParserObject *obj,
               GType              type)
{
  while (obj && !g_type_is_a (G_OBJECT_TYPE (obj), type))
    obj = girst_parser_object_get_parent (obj);
  return obj;
}

static GirstParserObject *
girst_resolve (GirstParserObject *relative,
               const gchar       *name)
{
  g_autofree gchar *freeme = NULL;
  GirstNamespace *nsobj;
  const gchar *dot;
  const gchar *ns = NULL;

  nsobj = find_ancestor (relative, GIRST_TYPE_NAMESPACE);
  if (nsobj != NULL)
    ns = girst_namespace_get_name (nsobj);

  if (NULL != (dot = strchr (name, '.')))
    {
      ns = freeme = g_strndup (name, dot - name);
      name = dot + 1;
    }

  return girst_cache_resolve (ns, name);
}

gchar *
girst_resolve_type (GirstNamespace *ns,
                    const gchar    *name,
                    GirstLanguage   language)
{
  g_autofree gchar *freeme = NULL;
  g_autofree gchar *c_type = NULL;
  GirstParserObject *obj;
  const gchar *ns_name;
  const gchar *dot;

  g_return_val_if_fail (GIRST_IS_NAMESPACE (ns), NULL);

  if (language != GIRST_LANGUAGE_C || name == NULL)
    return g_strdup (name);

  ns_name = girst_namespace_get_name (ns);

  if (NULL != (dot = strchr (name, '.')))
    {
      ns_name = freeme = g_strndup (name, dot - name);
      name = dot + 1;
    }

  if (!(obj = girst_cache_resolve (ns_name, name)))
    return g_strdup (name);

  g_object_get (obj, "c-type", &c_type, NULL);

  return g_steal_pointer (&c_type);
}

GType
girst_language_get_type (void)
{
  static GType g_type;

  if (g_once_init_enter (&g_type))
    {
      static const GEnumValue values[] = {
        { GIRST_LANGUAGE_C, "GIRST_LANGUAGE_C", "c" },
        { 0 }
      };
      GType _g_type = g_enum_register_static ("GirstLanguage", values);
      g_once_init_leave (&g_type, _g_type);
    }

  return g_type;
}

gboolean
girst_class_has_type_struct (GirstClass *klass)
{
  return !!girst_class_get_type_struct (klass);
}

/**
 * girst_class_get_type_struct:
 *
 * Returns: (transfer none): A type struct
 */
GirstRecord *
girst_class_get_type_struct (GirstClass *klass)
{
  GirstParserObject *parent;
  const gchar *name;

  g_return_val_if_fail (GIRST_IS_CLASS (klass), NULL);

  name = girst_class_get_name (klass);

  for (parent = GIRST_PARSER_OBJECT (klass);
       parent != NULL;
       parent = girst_parser_object_get_parent (parent))
    {
      if (GIRST_IS_NAMESPACE (parent))
        {
          GPtrArray *children = girst_parser_object_get_children (parent);

          for (guint i = 0; i < children->len; i++)
            {
              GirstParserObject *obj = g_ptr_array_index (children, i);

              if (GIRST_IS_RECORD (obj) &&
                  girst_str_equal0 (name, girst_record_get_glib_is_gtype_struct_for (GIRST_RECORD (obj))))
                return GIRST_RECORD (obj);
            }
        }
    }

  return NULL;
}

/**
 * girst_class_get_subclasses:
 *
 * Returns: (transfer full): A #GListModel
 */
GListModel *
girst_class_get_subclasses (GirstClass *klass)
{
  GirstParserObject *parent;
  GListStore *store;
  const gchar *name;

  g_return_val_if_fail (GIRST_IS_CLASS (klass), NULL);

  store = g_list_store_new (GIRST_TYPE_CLASS);
  name = girst_class_get_name (klass);

  for (parent = GIRST_PARSER_OBJECT (klass);
       parent != NULL;
       parent = girst_parser_object_get_parent (parent))
    {
      if (GIRST_IS_NAMESPACE (parent))
        {
          GPtrArray *children = girst_parser_object_get_children (parent);

          for (guint i = 0; i < children->len; i++)
            {
              GirstParserObject *obj = g_ptr_array_index (children, i);
              const gchar *pname;

              if (!GIRST_IS_CLASS (obj))
                continue;

              pname = girst_class_get_parent (GIRST_CLASS (obj));

              if (girst_str_equal0 (pname, name))
                g_list_store_append (store, obj);
            }
        }
    }

  return G_LIST_MODEL (store);
}

const gchar *
girst_parser_object_namespace (GirstParserObject *obj)
{
  GirstParserObject *parent;

  g_return_val_if_fail (GIRST_IS_PARSER_OBJECT (obj), NULL);

  for (parent = GIRST_PARSER_OBJECT (obj);
       parent != NULL;
       parent = girst_parser_object_get_parent (parent))
    {
      if (GIRST_IS_NAMESPACE (parent))
        return girst_namespace_get_name (GIRST_NAMESPACE (parent));
    }

  return NULL;
}

/**
 * girst_class_get_parent_class:
 *
 * Returns: (transfer none) (nullable): A #GirstClass or %NULL
 */
GirstClass *
girst_class_get_parent_class (GirstClass *klass)
{
  g_autofree gchar *freeme = NULL;
  const gchar *ns_name;
  const gchar *parent;
  const gchar *dot;

  g_return_val_if_fail (GIRST_IS_CLASS (klass), NULL);

  if (!(parent = girst_class_get_parent (klass)))
    return NULL;

  ns_name = girst_parser_object_namespace (GIRST_PARSER_OBJECT (klass));

  if ((dot = strchr (parent, '.')))
    {
      ns_name = freeme = g_strndup (parent, dot - parent);
      parent = dot + 1;
    }

  return GIRST_CLASS (girst_cache_resolve (ns_name, parent));
}

/**
 * girst_class_get_parents:
 *
 * Returns: (transfer full): A #GListModel
 */
GListModel *
girst_class_get_parents (GirstClass *klass)
{
  GirstClass *parent = klass;
  GListStore *store;

  g_return_val_if_fail (GIRST_IS_CLASS (klass), NULL);

  if ((store = g_object_get_data (G_OBJECT (klass), "PARENTS")))
    return g_object_ref (G_LIST_MODEL (store));

  store = g_list_store_new (GIRST_TYPE_CLASS);

  do
    {
      parent = girst_class_get_parent_class (parent);
      if (parent != NULL)
        g_list_store_append (store, parent);
    }
  while (parent != NULL);

  g_object_set_data_full (G_OBJECT (klass), "PARENTS", g_object_ref (store), g_object_unref);

  return G_LIST_MODEL (store);
}

gchar *
girst_return_value_to_c (GirstReturnValue *return_value)
{
  GirstParserObject *obj;
  const gchar *c_type;

  g_return_val_if_fail (GIRST_IS_RETURN_VALUE (return_value), NULL);

  if ((obj = girst_parser_object_first_typed (GIRST_PARSER_OBJECT (return_value), GIRST_TYPE_ARRAY)))
    {
      if ((c_type = girst_array_get_c_type (GIRST_ARRAY (obj))))
        return g_strdup (c_type);

      if ((obj = girst_parser_object_first_typed (obj, GIRST_TYPE_TYPE)))
        {
          if ((c_type = girst_type_get_c_type (GIRST_TYPE (obj))))
            {
              if (girst_str_equal0 (c_type, "utf8"))
                {
                  const gchar *transfer = girst_return_value_get_transfer_ownership (return_value);

                  if (girst_str_equal0 (transfer, "none"))
                    return g_strdup ("const gchar * const *");
                  else if (girst_str_equal0 (transfer, "container"))
                    return g_strdup ("const gchar **");
                  else if (girst_str_equal0 (transfer, "full"))
                    return g_strdup ("gchar **");
                }
            }
        }
    }

  if ((obj = girst_parser_object_first_typed (GIRST_PARSER_OBJECT (return_value), GIRST_TYPE_TYPE)))
    {
      c_type = girst_type_get_c_type (GIRST_TYPE (obj));
      if (c_type != NULL)
        return g_strdup (c_type);
    }

  return g_strdup ("void");
}

gchar *
girst_parameter_type_name (GirstParameter *param,
                           GirstLanguage   language)
{
  GirstType *type;
#if 0
  GirstArray *array;
#endif

  g_return_val_if_fail (GIRST_IS_PARAMETER (param), NULL);

  if (language != GIRST_LANGUAGE_C)
    return NULL;

  if ((type = (GirstType *)girst_parser_object_first_typed (GIRST_PARSER_OBJECT (param), GIRST_TYPE_TYPE)))
    return girst_type_to_c (type);

#if 0
  if ((array = (GirstType *)girst_parser_object_first_typed (GIRST_PARSER_OBJECT (param), GIRST_TYPE_ARRAY)))
    return girst_array_to_c (type);
#endif

  return NULL;
}

gchar *
girst_instance_parameter_type_name (GirstInstanceParameter *param,
                                    GirstLanguage           language)
{
  GirstType *type;

  g_return_val_if_fail (GIRST_IS_INSTANCE_PARAMETER (param), NULL);

  if (language != GIRST_LANGUAGE_C)
    return NULL;

  if ((type = (GirstType *)girst_parser_object_first_typed (GIRST_PARSER_OBJECT (param), GIRST_TYPE_TYPE)))
    return girst_type_to_c (type);

  return NULL;
}

gchar *
girst_parameter_annotations (GirstParameter *param)
{
  const gchar *val;
  GString *str;

  g_return_val_if_fail (GIRST_IS_PARAMETER (param), NULL);

  str = g_string_new (NULL);

  if (girst_str_equal0 (girst_parameter_get_nullable (param), "1") ||
      girst_str_equal0 (girst_parameter_get_allow_none (param), "1"))
    g_string_append (str, " ``nullable``");

  if ((val = girst_parameter_get_direction (param)))
    g_string_append_printf (str, " ``%s``", val);

  if (girst_str_equal0 (girst_parameter_get_optional (param), "1"))
    g_string_append (str, " ``optional``");

  if ((val = girst_parameter_get_transfer_ownership (param)) &&
      !girst_str_equal0 (val, "none"))
    g_string_append_printf (str, " ``transfer %s``", val);

  return g_strstrip (g_string_free (str, FALSE));
}

gchar *
girst_instance_parameter_annotations (GirstInstanceParameter *param)
{
  const gchar *val;
  GString *str;

  g_return_val_if_fail (GIRST_IS_INSTANCE_PARAMETER (param), NULL);

  str = g_string_new (NULL);

  if (girst_str_equal0 (girst_instance_parameter_get_nullable (param), "1") ||
      girst_str_equal0 (girst_instance_parameter_get_allow_none (param), "1"))
    g_string_append (str, " ``nullable``");

  if ((val = girst_instance_parameter_get_direction (param)))
    g_string_append_printf (str, " ``%s``", val);

  if ((val = girst_instance_parameter_get_transfer_ownership (param)) &&
      !girst_str_equal0 (val, "none"))
    g_string_append_printf (str, " ``transfer %s``", val);

  return g_string_free (str, FALSE);
}

gchar *
girst_return_value_annotations (GirstReturnValue *param)
{
  const gchar *val;
  GString *str;

  g_return_val_if_fail (GIRST_IS_RETURN_VALUE (param), NULL);

  str = g_string_new (NULL);

  if (girst_str_equal0 (girst_return_value_get_nullable (param), "1") ||
      girst_str_equal0 (girst_return_value_get_allow_none (param), "1"))
    g_string_append (str, " ``nullable``");

  if ((val = girst_return_value_get_transfer_ownership (param)) &&
      !girst_str_equal0 (val, "none"))
    g_string_append_printf (str, " ``transfer %s``", val);

  return g_string_free (str, FALSE);
}

gboolean
girst_parser_object_has_property (GirstParserObject *obj,
                                  const gchar       *name)
{
  g_return_val_if_fail (GIRST_IS_PARSER_OBJECT (obj), FALSE);
  g_return_val_if_fail (name != NULL, FALSE);

  return g_object_class_find_property (G_OBJECT_GET_CLASS (obj), name) != NULL;
}

gchar *
girst_parser_object_title (GirstParserObject *obj)
{
  g_autofree gchar *title = NULL;

  if (girst_parser_object_has_property (obj, "c-type"))
    g_object_get (obj, "c-type", &title, NULL);
  else if (girst_parser_object_has_property (obj, "c-identifier"))
    g_object_get (obj, "c-type", &title, NULL);
  else if (girst_parser_object_has_property (obj, "name"))
    g_object_get (obj, "name", &title, NULL);

  return g_steal_pointer (&title);
}

/**
 * girst_property_type:
 *
 * Returns: (transfer none) (nullable): A #GirstParserObject or %NULL
 */
GirstParserObject *
girst_property_type (GirstProperty *prop)
{
  GirstParserObject *ret;

  g_return_val_if_fail (GIRST_IS_PROPERTY (prop), NULL);

  if ((ret = girst_parser_object_first_typed (GIRST_PARSER_OBJECT (prop), GIRST_TYPE_ARRAY)) ||
      (ret = girst_parser_object_first_typed (GIRST_PARSER_OBJECT (prop), GIRST_TYPE_TYPE)))
    return ret;

  return NULL;
}

gchar *
girst_parser_object_c_ref_full (GirstParserObject *obj,
                                const gchar       *title,
                                gboolean           add_pointer)
{
  g_return_val_if_fail (GIRST_IS_PARSER_OBJECT (obj), NULL);

  if (GIRST_IS_CLASS (obj) ||
      GIRST_IS_INTERFACE (obj) ||
      GIRST_IS_RECORD (obj) ||
      GIRST_IS_UNION (obj) ||
      GIRST_IS_GLIB_BOXED (obj) ||
      GIRST_IS_ENUMERATION (obj) ||
      GIRST_IS_ALIAS (obj) ||
      GIRST_IS_CALLBACK (obj) ||
      GIRST_IS_BITFIELD (obj))
    {
      g_autofree gchar *c_type = NULL;

      g_object_get (obj, "c-type", &c_type, NULL);

      return g_strdup_printf (":ref:`%s%s <C:%s>`",
                              title ? title : c_type,
                              add_pointer ? "*" : "",
                              c_type);
    }

  if (GIRST_IS_METHOD (obj) ||
      GIRST_IS_FUNCTION (obj) ||
      GIRST_IS_CONSTRUCTOR (obj) ||
      GIRST_IS_CONSTANT (obj) ||
      GIRST_IS_MEMBER (obj))
    {
      g_autofree gchar *c_ident = NULL;

      g_object_get (obj, "c-identifier", &c_ident, NULL);

      return g_strdup_printf (":ref:`%s() <C:%s>`",
                              title ? title : c_ident, c_ident);
    }

  if (GIRST_IS_IMPLEMENTS (obj) || GIRST_IS_PREREQUISITE (obj))
    {
      g_autofree gchar *name = NULL;
      GirstParserObject *impl;

      g_object_get (obj, "name", &name, NULL);
      impl = girst_resolve (obj, name);

      if (impl != NULL && impl != obj)
        return girst_parser_object_c_ref_full (impl, title, FALSE);
    }

  if (GIRST_IS_VIRTUAL_METHOD (obj))
    {
      GirstParserObject *container;

      if ((container = find_ancestor (obj, GIRST_TYPE_CLASS)) ||
          (container = find_ancestor (obj, GIRST_TYPE_INTERFACE)))
        {
          g_autofree gchar *c_type = NULL;
          const gchar *name = girst_virtual_method_get_name (GIRST_VIRTUAL_METHOD (obj));

          g_object_get (container, "c-type", &c_type, NULL);

          if (title)
            return g_strdup_printf (":ref:`%s <C:%s.vfuncs.%s>`", title, c_type, name);
          else
            return g_strdup_printf (":ref:`%s.%s <C:%s.vfuncs.%s>`", c_type, name, c_type, name);
        }
    }

  if (GIRST_IS_PROPERTY (obj))
    {
      GirstParserObject *container;

      if ((container = find_ancestor (obj, GIRST_TYPE_CLASS)) ||
          (container = find_ancestor (obj, GIRST_TYPE_INTERFACE)))
        {
          g_autofree gchar *c_type = NULL;
          const gchar *name = girst_property_get_name (GIRST_PROPERTY (obj));

          g_object_get (container, "c-type", &c_type, NULL);

          if (title)
            return g_strdup_printf (":ref:`%s <C:%s.props.%s>`", title, c_type, name);
          else
            return g_strdup_printf (":ref:`%s:%s <C:%s.props.%s>`", c_type, name, c_type, name);
        }
    }

  if (GIRST_IS_GLIB_SIGNAL (obj))
    {
      GirstParserObject *container;

      if ((container = find_ancestor (obj, GIRST_TYPE_CLASS)) ||
          (container = find_ancestor (obj, GIRST_TYPE_INTERFACE)))
        {
          g_autofree gchar *c_type = NULL;
          const gchar *name = girst_glib_signal_get_name (GIRST_GLIB_SIGNAL (obj));

          g_object_get (container, "c-type", &c_type, NULL);

          if (title)
            return g_strdup_printf (":ref:`%s <C:%s.signals.%s>`", title, c_type, name);
          else
            return g_strdup_printf (":ref:`%s::%s <C:%s.signals.%s>`", c_type, name, c_type, name);
        }
    }

  if (GIRST_IS_TYPE (obj))
    {
      g_autofree gchar *c_type = NULL;
      GirstParserObject *res;
      const gchar *name;

      g_object_get (obj, "c-type", &c_type, NULL);

      if (c_type)
        return g_strdup_printf ("`%s%s`", c_type, add_pointer ? "*" : "");

      name = girst_type_get_name (GIRST_TYPE (obj));
      res = girst_resolve (obj, name);

      if (res && res != obj)
        return girst_parser_object_c_ref_full (res, title, TRUE);
    }

  if (GIRST_IS_RETURN_VALUE (obj))
    {
      GirstParserObject *ar = NULL;
      GirstParserObject *type = NULL;
      g_autofree gchar *sig = NULL;

      sig = girst_return_value_to_c (GIRST_RETURN_VALUE (obj));

      if ((ar = girst_parser_object_first_typed (obj, GIRST_TYPE_ARRAY)))
        type = girst_parser_object_first_typed (ar, GIRST_TYPE_TYPE);
      else
        type = girst_parser_object_first_typed (obj, GIRST_TYPE_TYPE);

      if (type != NULL)
        {
          g_autofree gchar *c_type = NULL;
          GirstParserObject *rel;
          const gchar *name;

          g_object_get (type, "c-type", &c_type, NULL);

          if (c_type == NULL)
            {
              name = girst_type_get_name (GIRST_TYPE (type));
              rel = girst_resolve (obj, name);
              if (girst_parser_object_has_property (rel, "c-type"))
                g_object_get (rel, "c-type", c_type, NULL);
            }

          if (c_type != NULL)
            return g_strdup_printf (":ref:`%s <C:%s>`", title ? title : sig, c_type);
        }

      return g_steal_pointer (&sig);
    }

  return NULL;
}

gchar *
girst_parser_object_c_ref_titled (GirstParserObject *obj,
                                  const gchar       *title)
{
  return girst_parser_object_c_ref_full (obj, title, FALSE);
}

gchar *
girst_parser_object_c_ref (GirstParserObject *obj)
{
  return girst_parser_object_c_ref_full (obj, NULL, FALSE);
}
