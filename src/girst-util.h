/* girst-util.h
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <glib-object.h>

#include "parser/girst-namespace.h"
#include "parser/girst-property.h"

G_BEGIN_DECLS

#define GIRST_TYPE_LANGUAGE (girst_language_get_type())

typedef enum
{
  GIRST_LANGUAGE_C = 'c',
} GirstLanguage;

static inline gboolean
girst_str_equal0 (const gchar *a,
                  const gchar *b)
{
  return a == b || g_strcmp0 (a, b) == 0;
}

static inline gboolean
girst_str_empty0 (const gchar *a)
{
  return !a || !*a;
}

GType  girst_language_get_type (void);
gchar *girst_resolve_type      (GirstNamespace   *ns,
                                const gchar      *type,
                                GirstLanguage     language);
gchar *girst_title1            (const gchar      *title);
gchar *girst_title2            (const gchar      *title);
gchar *girst_title3            (const gchar      *title);
gchar *girst_title4            (const gchar      *title);
gchar *girst_stringify         (const gchar      *type,
                                const gchar      *value);
gchar *girst_gtk_doc           (const gchar      *text,
                                const gchar      *line_prefix,
                                GirstLanguage     language);
gchar *girst_one_line          (const gchar      *text,
                                GirstLanguage     language);

/* Extension functions */
gboolean     girst_class_has_type_struct          (GirstClass             *klass);
GirstRecord *girst_class_get_type_struct          (GirstClass             *klass);
GListModel  *girst_class_get_subclasses           (GirstClass             *klass);
GirstClass  *girst_class_get_parent_class         (GirstClass             *klass);
GListModel  *girst_class_get_parents              (GirstClass             *klass);
gchar       *girst_doc_to_c                       (GirstDoc               *doc,
                                                   const gchar            *line_prefix);
gchar       *girst_type_to_c                      (GirstType              *type);
gchar       *girst_instance_parameter_type_name   (GirstInstanceParameter *param,
                                                   GirstLanguage           language);
gchar       *girst_instance_parameter_annotations (GirstInstanceParameter *param);
gchar       *girst_parameter_type_name            (GirstParameter         *param,
                                                   GirstLanguage           language);
gchar       *girst_parameter_annotations          (GirstParameter         *param);
GirstParserObject
            *girst_property_type                  (GirstProperty          *prop);
gchar       *girst_property_type_name             (GirstProperty          *prop,
                                                   GirstLanguage           language);
gchar       *girst_property_type_flags            (GirstProperty          *property);
gchar       *girst_glib_signal_type_flags         (GirstGlibSignal        *self);
gchar       *girst_return_value_to_c              (GirstReturnValue       *return_value);
gchar       *girst_return_value_annotations       (GirstReturnValue       *return_value);
const gchar *girst_parser_object_namespace        (GirstParserObject      *obj);
gchar       *girst_parser_object_title            (GirstParserObject      *obj);
gboolean     girst_parser_object_has_property     (GirstParserObject      *obj,
                                                   const gchar            *name);
gchar       *girst_parser_object_c_ref            (GirstParserObject      *obj);
gchar       *girst_parser_object_c_ref_titled     (GirstParserObject      *obj,
                                                   const gchar            *title);

G_END_DECLS
