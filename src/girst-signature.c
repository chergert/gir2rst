/* girst-signature.c
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "girst-signature"

#include <string.h>

#include "c-parse-helper.h"

#include "girst-cache.h"
#include "girst-signature.h"
#include "girst-util.h"

#include "parser/girst-alias.h"
#include "parser/girst-bitfield.h"
#include "parser/girst-array.h"
#include "parser/girst-callback.h"
#include "parser/girst-class.h"
#include "parser/girst-constant.h"
#include "parser/girst-constructor.h"
#include "parser/girst-doc.h"
#include "parser/girst-enumeration.h"
#include "parser/girst-function.h"
#include "parser/girst-glib-signal.h"
#include "parser/girst-implements.h"
#include "parser/girst-interface.h"
#include "parser/girst-member.h"
#include "parser/girst-method.h"
#include "parser/girst-namespace.h"
#include "parser/girst-parameter.h"
#include "parser/girst-parser-types.h"
#include "parser/girst-prerequisite.h"
#include "parser/girst-property.h"
#include "parser/girst-record.h"
#include "parser/girst-return-value.h"
#include "parser/girst-type.h"
#include "parser/girst-union.h"
#include "parser/girst-varargs.h"
#include "parser/girst-virtual-method.h"

static gpointer
find_ancestor (GirstParserObject *obj,
               GType              type)
{
  while (obj && !g_type_is_a (G_OBJECT_TYPE (obj), type))
    obj = girst_parser_object_get_parent (obj);
  return obj;
}

static const gchar *
find_namespace_name (GirstParserObject *obj)
{
  if (obj && !GIRST_IS_NAMESPACE (obj))
    obj = find_ancestor (obj, GIRST_TYPE_NAMESPACE);
  if (obj)
    return girst_namespace_get_name (GIRST_NAMESPACE (obj));
  return NULL;
}

static const gchar *
find_object_c_type (GirstParserObject *obj)
{
  while (obj && !GIRST_IS_CLASS (obj) && !GIRST_IS_INTERFACE (obj) && !GIRST_IS_RECORD (obj))
    obj = girst_parser_object_get_parent (obj);
  if (GIRST_IS_CLASS (obj))
    return girst_class_get_c_type (GIRST_CLASS (obj));
  if (GIRST_IS_INTERFACE (obj))
    return girst_interface_get_c_type (GIRST_INTERFACE (obj));
  if (GIRST_IS_RECORD (obj))
    return girst_record_get_c_type (GIRST_RECORD (obj));
  return NULL;
}

static GirstParserObject *
find_ctor_parent (GirstParserObject *obj)
{
  while (obj && !GIRST_IS_CLASS (obj) && !GIRST_IS_RECORD (obj) && !GIRST_IS_INTERFACE (obj))
    obj = girst_parser_object_get_parent (obj);
  return obj;
}

#if 0
static GirstParserObject *
find_enumlike_parent (GirstParserObject *obj)
{
  while (obj && !GIRST_IS_ENUMERATION (obj) && !GIRST_IS_BITFIELD (obj))
    obj = girst_parser_object_get_parent (obj);
  return obj;
}
#endif

static GirstParserObject *
girst_resolve (GirstParserObject *relative,
               const gchar       *name)
{
  g_autofree gchar *freeme = NULL;
  GirstNamespace *nsobj;
  const gchar *dot;
  const gchar *ns = NULL;

  if (name == NULL || relative == NULL)
    return NULL;

  nsobj = find_ancestor (relative, GIRST_TYPE_NAMESPACE);
  if (nsobj != NULL)
    ns = girst_namespace_get_name (nsobj);

  if (NULL != (dot = strchr (name, '.')))
    {
      ns = freeme = g_strndup (name, dot - name);
      name = dot + 1;
    }

  return girst_cache_resolve (ns, name);
}

gchar *
girst_return_value_get_lhs_ctype (GirstReturnValue *self)
{
  GirstParserObject *obj;

  g_return_val_if_fail (GIRST_IS_RETURN_VALUE (self), NULL);

  if ((obj = girst_parser_object_first_typed (GIRST_PARSER_OBJECT (self), GIRST_TYPE_ARRAY)))
    return girst_array_get_lhs_ctype (GIRST_ARRAY (obj));

  if ((obj = girst_parser_object_first_typed (GIRST_PARSER_OBJECT (self), GIRST_TYPE_TYPE)))
    return girst_type_get_lhs_ctype (GIRST_TYPE (obj));

  return g_strdup ("void");
}

gchar *
girst_alias_get_lhs_ctype (GirstAlias *self)
{
  g_return_val_if_fail (GIRST_IS_ALIAS (self), NULL);

  return g_strdup (girst_alias_get_c_type (self));
}

gchar *
girst_array_get_lhs_ctype (GirstArray *self)
{
  GirstParserObject *obj;
  const gchar *c_type;

  g_return_val_if_fail (GIRST_IS_ARRAY (self), NULL);

  if ((c_type = girst_array_get_c_type (self)))
    return g_strdup (c_type);

  if ((obj = girst_parser_object_first_typed (GIRST_PARSER_OBJECT (self), GIRST_TYPE_TYPE)))
    {
      g_autofree gchar *res = girst_type_get_lhs_ctype (GIRST_TYPE (obj));

      if (res != NULL)
        return g_strdup_printf ("%s*", res);
    }

  return NULL;
}

gchar *
girst_property_get_lhs_ctype (GirstProperty *self)
{
  GirstParserObject *obj;

  g_return_val_if_fail (GIRST_IS_PROPERTY (self), NULL);

  if ((obj = girst_parser_object_first_typed (GIRST_PARSER_OBJECT (self), GIRST_TYPE_ARRAY)))
    return girst_array_get_lhs_ctype (GIRST_ARRAY (obj));

  if ((obj = girst_parser_object_first_typed (GIRST_PARSER_OBJECT (self), GIRST_TYPE_TYPE)))
    return girst_type_get_lhs_ctype (GIRST_TYPE (obj));

  return NULL;
}

gchar *
girst_parameter_get_lhs_ctype (GirstParameter *self)
{
  GirstParserObject *obj;
  g_autofree gchar *res = NULL;

  g_return_val_if_fail (GIRST_IS_PARAMETER (self), NULL);

  if ((obj = girst_parser_object_first_typed (GIRST_PARSER_OBJECT (self), GIRST_TYPE_ARRAY)))
    res = girst_array_get_lhs_ctype (GIRST_ARRAY (obj));

  if (!res && (obj = girst_parser_object_first_typed (GIRST_PARSER_OBJECT (self), GIRST_TYPE_TYPE)))
    res = girst_type_get_lhs_ctype (GIRST_TYPE (obj));

  if (!res && (obj = girst_parser_object_first_typed (GIRST_PARSER_OBJECT (self), GIRST_TYPE_VARARGS)))
    return g_strdup ("...");

  return g_steal_pointer (&res);
}

gchar *
girst_instance_parameter_get_lhs_ctype (GirstInstanceParameter *self)
{
  GirstParserObject *obj;

  g_return_val_if_fail (GIRST_IS_INSTANCE_PARAMETER (self), NULL);

  if ((obj = girst_parser_object_first_typed (GIRST_PARSER_OBJECT (self), GIRST_TYPE_TYPE)))
    return g_strdup (girst_type_get_c_type (GIRST_TYPE (obj)));

  return NULL;
}

gchar *
girst_glib_signal_get_lhs_ctype (GirstGlibSignal *self)
{
  GirstParserObject *obj;

  g_return_val_if_fail (GIRST_IS_GLIB_SIGNAL (self), NULL);

  obj = girst_parser_object_first_typed (GIRST_PARSER_OBJECT (self), GIRST_TYPE_RETURN_VALUE);
  if (obj == NULL)
    return g_strdup ("void");
  return girst_return_value_get_lhs_ctype (GIRST_RETURN_VALUE (obj));
}

gchar *
girst_type_get_lhs_ctype (GirstType *self)
{
  GirstParserObject *obj;
  const gchar *c_type;
  const gchar *name;

  g_return_val_if_fail (GIRST_IS_TYPE (self), NULL);

  if ((c_type = girst_type_get_c_type (self)))
    return g_strdup (c_type);

  name = girst_type_get_name (self);

  if (g_strcmp0 (name, "utf8") == 0)
    return g_strdup ("gchar*");

  if ((obj = girst_resolve (GIRST_PARSER_OBJECT (self), name)))
    {
      g_autofree gchar *res = girst_parser_object_get_lhs_ctype (obj);

      if (GIRST_IS_RECORD (obj) ||
          GIRST_IS_CLASS (obj) ||
          GIRST_IS_INTERFACE (obj) ||
          GIRST_IS_UNION (obj) ||
          GIRST_IS_IMPLEMENTS (obj) ||
          GIRST_IS_PREREQUISITE (obj))
        return g_strdup_printf ("%s*", res);

      return g_steal_pointer (&res);
    }

  return NULL;
}

gchar *
girst_union_get_lhs_ctype (GirstUnion *self)
{
  g_return_val_if_fail (GIRST_IS_UNION (self), NULL);

  return g_strdup (girst_union_get_c_type (self));
}

gchar *
girst_record_get_lhs_ctype (GirstRecord *self)
{
  g_return_val_if_fail (GIRST_IS_RECORD (self), NULL);

  return g_strdup (girst_record_get_c_type (self));
}

gchar *
girst_constructor_get_lhs_ctype (GirstConstructor *self)
{
  GirstParserObject *obj;

  g_return_val_if_fail (GIRST_IS_CONSTRUCTOR (self), NULL);

  if ((obj = find_ctor_parent (GIRST_PARSER_OBJECT (self))))
    {
      g_autofree gchar *res = girst_parser_object_get_lhs_ctype (obj);

      return g_strdup_printf ("%s*", res);
    }

  return g_strdup ("void");
}

static GirstParserObject *
girst_constructor_get_lhs (GirstConstructor *self)
{
  g_return_val_if_fail (GIRST_IS_CONSTRUCTOR (self), NULL);

  return find_ctor_parent (GIRST_PARSER_OBJECT (self));
}

gchar *
girst_function_get_lhs_ctype (GirstFunction *self)
{
  GirstParserObject *obj;

  g_return_val_if_fail (GIRST_IS_FUNCTION (self), NULL);

  if ((obj = girst_parser_object_first_typed (GIRST_PARSER_OBJECT (self), GIRST_TYPE_RETURN_VALUE)))
    return girst_return_value_get_lhs_ctype (GIRST_RETURN_VALUE (obj));

  return g_strdup ("void");
}

gchar *
girst_method_get_lhs_ctype (GirstMethod *self)
{
  GirstParserObject *obj;

  g_return_val_if_fail (GIRST_IS_METHOD (self), NULL);

  if ((obj = girst_parser_object_first_typed (GIRST_PARSER_OBJECT (self), GIRST_TYPE_RETURN_VALUE)))
    return girst_return_value_get_lhs_ctype (GIRST_RETURN_VALUE (obj));

  return g_strdup ("void");
}

gchar *
girst_virtual_method_get_lhs_ctype (GirstVirtualMethod *self)
{
  GirstParserObject *obj;

  g_return_val_if_fail (GIRST_IS_VIRTUAL_METHOD (self), NULL);

  if ((obj = girst_parser_object_first_typed (GIRST_PARSER_OBJECT (self), GIRST_TYPE_RETURN_VALUE)))
    return girst_return_value_get_lhs_ctype (GIRST_RETURN_VALUE (obj));

  return g_strdup ("void");
}

gchar *
girst_callback_get_lhs_ctype (GirstCallback *self)
{
  GirstParserObject *obj;

  g_return_val_if_fail (GIRST_IS_CALLBACK (self), NULL);

  if ((obj = girst_parser_object_first_typed (GIRST_PARSER_OBJECT (self), GIRST_TYPE_RETURN_VALUE)))
    return girst_return_value_get_lhs_ctype (GIRST_RETURN_VALUE (obj));

  return g_strdup ("void");
}

gchar *
girst_constant_get_lhs_ctype (GirstConstant *self)
{
  GirstParserObject *obj;

  g_return_val_if_fail (GIRST_IS_CONSTANT (self), NULL);

  if ((obj = girst_parser_object_first_typed (GIRST_PARSER_OBJECT (self), GIRST_TYPE_TYPE)))
    return girst_type_get_lhs_ctype (GIRST_TYPE (obj));

  return NULL;
}

gchar *
girst_class_get_lhs_ctype (GirstClass *self)
{
  g_return_val_if_fail (GIRST_IS_CLASS (self), NULL);

  return g_strdup (girst_class_get_c_type (self));
}

gchar *
girst_interface_get_lhs_ctype (GirstInterface *self)
{
  g_return_val_if_fail (GIRST_IS_INTERFACE (self), NULL);

  return g_strdup (girst_interface_get_c_type (self));
}

gchar *
girst_enumeration_get_lhs_ctype (GirstEnumeration *self)
{
  g_return_val_if_fail (GIRST_IS_ENUMERATION (self), NULL);

  return g_strdup (girst_enumeration_get_c_type (self));
}

gchar *
girst_member_get_lhs_ctype (GirstMember *self)
{
  g_return_val_if_fail (GIRST_IS_MEMBER (self), NULL);

  return g_strdup (girst_member_get_c_identifier (self));
}

gchar *
girst_bitfield_get_lhs_ctype (GirstBitfield *self)
{
  g_return_val_if_fail (GIRST_IS_BITFIELD (self), NULL);

  return g_strdup (girst_bitfield_get_c_type (self));
}

gchar *
girst_prerequisite_get_lhs_ctype (GirstPrerequisite *self)
{
  GirstParserObject *obj;
  const gchar *name;

  g_return_val_if_fail (GIRST_IS_PREREQUISITE (self), NULL);

  name = girst_prerequisite_get_name (self);
  obj = girst_resolve (GIRST_PARSER_OBJECT (self), name);
  if (obj != NULL)
    return girst_parser_object_get_lhs_ctype (obj);

  return NULL;
}

gchar *
girst_implements_get_lhs_ctype (GirstImplements *self)
{
  GirstParserObject *obj;
  const gchar *name;

  g_return_val_if_fail (GIRST_IS_IMPLEMENTS (self), NULL);

  name = girst_implements_get_name (self);
  obj = girst_resolve (GIRST_PARSER_OBJECT (self), name);
  if (obj != NULL)
    return girst_parser_object_get_lhs_ctype (obj);

  return g_strdup (name);
}

gchar *
girst_varargs_get_lhs_ctype (GirstVarargs *self)
{
  return g_strdup ("...");
}

gchar *
girst_parser_object_get_lhs_ctype (GirstParserObject *self)
{
  g_return_val_if_fail (GIRST_IS_PARSER_OBJECT (self), NULL);

  if (GIRST_IS_ALIAS (self))
    return girst_alias_get_lhs_ctype (GIRST_ALIAS (self));

  if (GIRST_IS_ARRAY (self))
    return girst_array_get_lhs_ctype (GIRST_ARRAY (self));

  if (GIRST_IS_BITFIELD (self))
    return girst_bitfield_get_lhs_ctype (GIRST_BITFIELD (self));

  if (GIRST_IS_CALLBACK (self))
    return girst_callback_get_lhs_ctype (GIRST_CALLBACK (self));

  if (GIRST_IS_CLASS (self))
    return girst_class_get_lhs_ctype (GIRST_CLASS (self));

  if (GIRST_IS_FUNCTION (self))
    return girst_function_get_lhs_ctype (GIRST_FUNCTION (self));

  if (GIRST_IS_CONSTRUCTOR (self))
    return girst_constructor_get_lhs_ctype (GIRST_CONSTRUCTOR (self));

  if (GIRST_IS_GLIB_SIGNAL (self))
    return girst_glib_signal_get_lhs_ctype (GIRST_GLIB_SIGNAL (self));

  if (GIRST_IS_INSTANCE_PARAMETER (self))
    return girst_instance_parameter_get_lhs_ctype (GIRST_INSTANCE_PARAMETER (self));

  if (GIRST_IS_INTERFACE (self))
    return girst_interface_get_lhs_ctype (GIRST_INTERFACE (self));

  if (GIRST_IS_CONSTANT (self))
    return girst_constant_get_lhs_ctype (GIRST_CONSTANT (self));

  if (GIRST_IS_ENUMERATION (self))
    return girst_enumeration_get_lhs_ctype (GIRST_ENUMERATION (self));

  if (GIRST_IS_MEMBER (self))
    return girst_member_get_lhs_ctype (GIRST_MEMBER (self));

  if (GIRST_IS_METHOD (self))
    return girst_method_get_lhs_ctype (GIRST_METHOD (self));

  if (GIRST_IS_PARAMETER (self))
    return girst_parameter_get_lhs_ctype (GIRST_PARAMETER (self));

  if (GIRST_IS_PROPERTY (self))
    return girst_property_get_lhs_ctype (GIRST_PROPERTY (self));

  if (GIRST_IS_RECORD (self))
    return girst_record_get_lhs_ctype (GIRST_RECORD (self));

  if (GIRST_IS_RETURN_VALUE (self))
    return girst_return_value_get_lhs_ctype (GIRST_RETURN_VALUE (self));

  if (GIRST_IS_VIRTUAL_METHOD (self))
    return girst_virtual_method_get_lhs_ctype (GIRST_VIRTUAL_METHOD (self));

  if (GIRST_IS_PREREQUISITE (self))
    return girst_prerequisite_get_lhs_ctype (GIRST_PREREQUISITE (self));

  if (GIRST_IS_IMPLEMENTS (self))
    return girst_implements_get_lhs_ctype (GIRST_IMPLEMENTS (self));

  if (GIRST_IS_UNION (self))
    return girst_union_get_lhs_ctype (GIRST_UNION (self));

  if (GIRST_IS_VARARGS (self))
    return girst_varargs_get_lhs_ctype (GIRST_VARARGS (self));

  g_warning ("Unknown dispatch: %s", G_OBJECT_TYPE_NAME (self));

  return NULL;
}

static GirstParserObject *
girst_array_get_lhs (GirstArray *self)
{
  GirstParserObject *obj;
  const gchar *name;

  g_return_val_if_fail (GIRST_IS_ARRAY (self), NULL);

  if ((name = girst_array_get_name (self)))
    return girst_resolve (GIRST_PARSER_OBJECT (self), name);

  if ((obj = girst_parser_object_first_typed (GIRST_PARSER_OBJECT (self), GIRST_TYPE_TYPE)))
    {
      name = girst_type_get_name (GIRST_TYPE (obj));
      return girst_resolve (obj, name);
    }

  return NULL;
}

static GirstParserObject *
girst_type_get_lhs (GirstType *self)
{
  const gchar *name;

  g_return_val_if_fail (GIRST_IS_TYPE (self), NULL);

  name = girst_type_get_name (self);
  return girst_resolve (GIRST_PARSER_OBJECT (self), name);
}

static GirstParserObject *
girst_prerequisite_get_lhs (GirstPrerequisite *self)
{
  const gchar *name;

  g_return_val_if_fail (GIRST_IS_PREREQUISITE (self), NULL);

  name = girst_prerequisite_get_name (self);
  return girst_resolve (GIRST_PARSER_OBJECT (self), name);
}

static GirstParserObject *
girst_implements_get_lhs (GirstImplements *self)
{
  const gchar *name;

  g_return_val_if_fail (GIRST_IS_IMPLEMENTS (self), NULL);

  name = girst_implements_get_name (self);
  return girst_resolve (GIRST_PARSER_OBJECT (self), name);
}

static GirstParserObject *
girst_return_value_get_lhs (GirstReturnValue *self)
{
  GirstParserObject *obj;

  g_return_val_if_fail (GIRST_IS_RETURN_VALUE (self), NULL);

  if ((obj = girst_parser_object_first_typed (GIRST_PARSER_OBJECT (self), GIRST_TYPE_ARRAY)))
    return girst_array_get_lhs (GIRST_ARRAY (obj));

  if ((obj = girst_parser_object_first_typed (GIRST_PARSER_OBJECT (self), GIRST_TYPE_TYPE)))
    return girst_type_get_lhs (GIRST_TYPE (obj));

  return NULL;
}

static GirstParserObject *
girst_typeish_get_lhs (GirstParserObject *self)
{
  GirstParserObject *obj;

  g_return_val_if_fail (GIRST_IS_PARSER_OBJECT (self), NULL);

  if ((obj = girst_parser_object_first_typed (GIRST_PARSER_OBJECT (self), GIRST_TYPE_ARRAY)))
    return girst_array_get_lhs (GIRST_ARRAY (obj));

  if ((obj = girst_parser_object_first_typed (GIRST_PARSER_OBJECT (self), GIRST_TYPE_TYPE)))
    return girst_type_get_lhs (GIRST_TYPE (obj));

  return NULL;
}

static GirstParserObject *
girst_funcish_get_lhs (GirstParserObject *self)
{
  GirstParserObject *obj;

  g_return_val_if_fail (GIRST_IS_PARSER_OBJECT (self), NULL);

  if ((obj = girst_parser_object_first_typed (GIRST_PARSER_OBJECT (self), GIRST_TYPE_RETURN_VALUE)))
    return girst_return_value_get_lhs (GIRST_RETURN_VALUE (obj));

  return NULL;
}

/**
 * girst_parser_object_get_lhs:
 *
 * Returns: (transfer none): a parser object or %NULL
 */
GirstParserObject *
girst_parser_object_get_lhs (GirstParserObject *self)
{
  g_return_val_if_fail (GIRST_IS_PARSER_OBJECT (self), NULL);

  if (GIRST_IS_ARRAY (self))
    return girst_array_get_lhs (GIRST_ARRAY (self));

  if (GIRST_IS_CALLBACK (self))
    return girst_funcish_get_lhs (self);

  if (GIRST_IS_CLASS (self))
    return self;

  if (GIRST_IS_CONSTRUCTOR (self))
    return girst_constructor_get_lhs (GIRST_CONSTRUCTOR (self));

  if (GIRST_IS_BITFIELD (self))
    return self;

  if (GIRST_IS_ENUMERATION (self))
    return self;

  if (GIRST_IS_FUNCTION (self))
    return girst_funcish_get_lhs (self);

  if (GIRST_IS_GLIB_SIGNAL (self))
    return girst_funcish_get_lhs (self);

  if (GIRST_IS_INSTANCE_PARAMETER (self))
    return girst_typeish_get_lhs (self);

  if (GIRST_IS_INTERFACE (self))
    return self;

  if (GIRST_IS_METHOD (self))
    return girst_funcish_get_lhs (self);

  if (GIRST_IS_MEMBER (self))
    return girst_typeish_get_lhs (self);

  if (GIRST_IS_PARAMETER (self))
    return girst_typeish_get_lhs (self);

  if (GIRST_IS_PROPERTY (self))
    return girst_typeish_get_lhs (self);

  if (GIRST_IS_IMPLEMENTS (self))
    return girst_implements_get_lhs (GIRST_IMPLEMENTS (self));

  if (GIRST_IS_PREREQUISITE (self))
    return girst_prerequisite_get_lhs (GIRST_PREREQUISITE (self));

  if (GIRST_IS_RECORD (self))
    return self;

  if (GIRST_IS_RETURN_VALUE (self))
    return girst_return_value_get_lhs (GIRST_RETURN_VALUE (self));

  if (GIRST_IS_CONSTANT (self))
    return girst_typeish_get_lhs (self);

  if (GIRST_IS_VIRTUAL_METHOD (self))
    return girst_funcish_get_lhs (self);

  return NULL;
}

gchar *
girst_clink_full (const gchar       *word,
                  GirstParserObject *dest,
                  const gchar       *section)
{
  if (word == NULL)
    return NULL;

  if (section == NULL)
    section = "";

  /* If no link, do nothing */
  if (dest == NULL)
    return g_strdup (word);

  if (GIRST_IS_CLASS (dest))
    {
      const gchar *name = girst_class_get_c_type (GIRST_CLASS (dest));

      return g_strdup_printf (":ref:`%s <C:%s%s>`", word, name, section);
    }

  if (GIRST_IS_INTERFACE (dest))
    {
      const gchar *name = girst_interface_get_c_type (GIRST_INTERFACE (dest));

      return g_strdup_printf (":ref:`%s <C:%s%s>`", word, name, section);
    }

  if (GIRST_IS_ALIAS (dest))
    {
      const gchar *name = girst_alias_get_c_type (GIRST_ALIAS (dest));

      return g_strdup_printf (":ref:`%s <C:%s%s>`", word, name, section);
    }

  if (GIRST_IS_GLIB_SIGNAL (dest))
    {
      const gchar *name = girst_glib_signal_get_name (GIRST_GLIB_SIGNAL (dest));
      const gchar *c_type = find_object_c_type (dest);

      return g_strdup_printf (":ref:`%s <C:%s::%s%s>`", word, c_type, name, section);
    }

  if (GIRST_IS_PROPERTY (dest))
    {
      const gchar *name = girst_property_get_name (GIRST_PROPERTY (dest));
      const gchar *c_type = find_object_c_type (dest);

      return g_strdup_printf (":ref:`%s <C:%s:%s%s>`", word, c_type, name, section);
    }

  if (GIRST_IS_METHOD (dest))
    {
      const gchar *c_identifier = girst_method_get_c_identifier (GIRST_METHOD (dest));

      return g_strdup_printf (":ref:`%s <C:%s%s>`", word, c_identifier, section);
    }

  if (GIRST_IS_VIRTUAL_METHOD (dest))
    {
      const gchar *name = girst_virtual_method_get_name (GIRST_VIRTUAL_METHOD (dest));
      const gchar *c_type = find_object_c_type (dest);

      return g_strdup_printf (":ref:`%s <C:%s.%s%s>`", word, c_type, name, section);
    }

  if (GIRST_IS_ENUMERATION (dest))
    {
      const gchar *c_type = girst_enumeration_get_c_type (GIRST_ENUMERATION (dest));

      return g_strdup_printf (":ref:`%s <C:%s%s>`", word, c_type, section);
    }

  if (GIRST_IS_BITFIELD (dest))
    {
      const gchar *c_type = girst_bitfield_get_c_type (GIRST_BITFIELD (dest));

      return g_strdup_printf (":ref:`%s <C:%s%s>`", word, c_type, section);
    }

  if (GIRST_IS_CALLBACK (dest))
    {
      const gchar *c_type = girst_callback_get_c_type (GIRST_CALLBACK (dest));

      return g_strdup_printf (":ref:`%s <C:%s%s>`", word, c_type, section);
    }

  if (GIRST_IS_MEMBER (dest))
    {
      const gchar *c_identifier = girst_member_get_c_identifier (GIRST_MEMBER (dest));

      return g_strdup_printf (":ref:`%s <C:%s%s>`", word, c_identifier, section);
    }

  if (GIRST_IS_FUNCTION (dest))
    {
      const gchar *c_identifier = girst_function_get_c_identifier (GIRST_FUNCTION (dest));

      return g_strdup_printf (":ref:`%s <C:%s%s>`", word, c_identifier, section);
    }

  if (GIRST_IS_CONSTRUCTOR (dest))
    {
      const gchar *c_identifier = girst_constructor_get_c_identifier (GIRST_CONSTRUCTOR (dest));

      return g_strdup_printf (":ref:`%s <C:%s%s>`", word, c_identifier, section);
    }

  if (GIRST_IS_CONSTANT (dest))
    {
      const gchar *c_type = girst_constant_get_c_type (GIRST_CONSTANT (dest));
      if (c_type == NULL)
        c_type = girst_constant_get_c_identifier (GIRST_CONSTANT (dest));
      return g_strdup_printf (":ref:`%s <C:%s%s>`", word, c_type, section);
    }

  if (GIRST_IS_RECORD (dest))
    {
      const gchar *c_type = girst_record_get_c_type (GIRST_RECORD (dest));

      return g_strdup_printf (":ref:`%s <C:%s%s>`", word, c_type, section);
    }

  if (GIRST_IS_IMPLEMENTS (dest))
    {
      const gchar *name = girst_implements_get_name (GIRST_IMPLEMENTS (dest));
      GirstParserObject *obj = girst_resolve (dest, name);
      if (obj != NULL)
        return girst_clink (obj);
      return g_strdup (name);
    }

  if (GIRST_IS_NAMESPACE (dest))
    {
      const gchar *name = girst_namespace_get_name (GIRST_NAMESPACE (dest));
      const gchar *version = girst_namespace_get_version (GIRST_NAMESPACE (dest));

      return g_strdup_printf (":ref:`%s <C:%s-%s%s>`", word, name, version, section);
    }

  return g_strdup (word);
}

gchar *
girst_clink (GirstParserObject *self)
{
  if (self == NULL)
    return NULL;

  if (GIRST_IS_CLASS (self))
    {
      const gchar *name = girst_class_get_c_type (GIRST_CLASS (self));

      return g_strdup_printf ("<C:%s>", name);
    }

  if (GIRST_IS_INTERFACE (self))
    {
      const gchar *name = girst_interface_get_c_type (GIRST_INTERFACE (self));

      return g_strdup_printf ("<C:%s>", name);
    }

  if (GIRST_IS_ALIAS (self))
    {
      const gchar *name = girst_alias_get_c_type (GIRST_ALIAS (self));

      return g_strdup_printf ("<C:%s>", name);
    }

  if (GIRST_IS_GLIB_SIGNAL (self))
    {
      const gchar *name = girst_glib_signal_get_name (GIRST_GLIB_SIGNAL (self));
      const gchar *c_type = find_object_c_type (self);

      return g_strdup_printf ("<C:%s::%s>", c_type, name);
    }

  if (GIRST_IS_PROPERTY (self))
    {
      const gchar *name = girst_property_get_name (GIRST_PROPERTY (self));
      const gchar *c_type = find_object_c_type (self);

      return g_strdup_printf ("<C:%s:%s>", c_type, name);
    }

  if (GIRST_IS_METHOD (self))
    {
      const gchar *c_identifier = girst_method_get_c_identifier (GIRST_METHOD (self));

      return g_strdup_printf ("<C:%s>", c_identifier);
    }

  if (GIRST_IS_VIRTUAL_METHOD (self))
    {
      const gchar *name = girst_virtual_method_get_name (GIRST_VIRTUAL_METHOD (self));
      const gchar *c_type = find_object_c_type (self);

      return g_strdup_printf ("<C:%s.%s>", c_type, name);
    }

  if (GIRST_IS_ENUMERATION (self))
    {
      const gchar *c_type = girst_enumeration_get_c_type (GIRST_ENUMERATION (self));

      return g_strdup_printf ("<C:%s>", c_type);
    }

  if (GIRST_IS_BITFIELD (self))
    {
      const gchar *name = girst_bitfield_get_c_type (GIRST_BITFIELD (self));

      return g_strdup_printf ("<C:%s>", name);
    }

  if (GIRST_IS_MEMBER (self))
    {
      const gchar *c_identifier = girst_member_get_c_identifier (GIRST_MEMBER (self));
      return g_strdup_printf ("<C:%s>", c_identifier);
    }

  if (GIRST_IS_FUNCTION (self))
    {
      const gchar *c_identifier = girst_function_get_c_identifier (GIRST_FUNCTION (self));

      return g_strdup_printf ("<C:%s>", c_identifier);
    }

  if (GIRST_IS_CONSTRUCTOR (self))
    {
      const gchar *name = girst_constructor_get_c_identifier (GIRST_CONSTRUCTOR (self));

      return g_strdup_printf ("<C:%s>", name);
    }

  if (GIRST_IS_CONSTANT (self))
    {
      const gchar *name = girst_constant_get_c_type (GIRST_CONSTANT (self));
      if (name == NULL)
        name = girst_constant_get_c_identifier (GIRST_CONSTANT (self));
      return g_strdup_printf ("<C:%s>", name);
    }

  if (GIRST_IS_RECORD (self))
    {
      const gchar *c_type = girst_record_get_c_type (GIRST_RECORD (self));
      const gchar *peer = girst_record_get_glib_is_gtype_struct_for (GIRST_RECORD (self));
      GirstParserObject *obj;

      /* Prefer the object instance over class */
      if ((obj = girst_resolve (self, peer)))
        return girst_clink (obj);

      return g_strdup_printf ("<C:%s>", c_type);
    }

  if (GIRST_IS_IMPLEMENTS (self))
    {
      const gchar *name = girst_implements_get_name (GIRST_IMPLEMENTS (self));
      GirstParserObject *obj = girst_resolve (self, name);
      if (obj != NULL)
        return girst_clink (obj);
      return NULL;
    }

  if (GIRST_IS_NAMESPACE (self))
    {
      const gchar *nsname = find_namespace_name (self);

      return g_strdup_printf ("<C:%s>", nsname);
    }

  return NULL;
}

gboolean
girst_parser_object_has_parent_type (GirstParserObject *self)
{
  g_return_val_if_fail (GIRST_IS_PARSER_OBJECT (self), FALSE);

  return GIRST_IS_CLASS (self) &&
         !girst_str_empty0 (girst_class_get_parent (GIRST_CLASS (self)));
}

static gboolean
girst_parser_object_has_or_parent_has (GirstParserObject *self,
                                       GType              type)
{
  g_return_val_if_fail (GIRST_IS_PARSER_OBJECT (self), FALSE);

  if (girst_parser_object_has_child_typed (self, type))
    return TRUE;

  if (GIRST_IS_CLASS (self))
    {
      g_autoptr(GListModel) parents = girst_class_get_parents (GIRST_CLASS (self));
      guint n_items = g_list_model_get_n_items (parents);

      for (guint i = 0; i < n_items; i++)
        {
          g_autoptr(GirstParserObject) parent = g_list_model_get_item (parents, i);

          if (girst_parser_object_has_child_typed (parent, type))
            return TRUE;
        }
    }

  return FALSE;
}

gboolean
girst_parser_object_has_signals (GirstParserObject *self)
{
  return girst_parser_object_has_or_parent_has (self, GIRST_TYPE_GLIB_SIGNAL);
}

gboolean
girst_parser_object_has_vfuncs (GirstParserObject *self)
{
  return girst_parser_object_has_or_parent_has (self, GIRST_TYPE_VIRTUAL_METHOD);
}

gboolean
girst_parser_object_has_props (GirstParserObject *self)
{
  return girst_parser_object_has_or_parent_has (self, GIRST_TYPE_PROPERTY);
}

gboolean
girst_parser_object_has_methods (GirstParserObject *self)
{
  return girst_parser_object_has_or_parent_has (self, GIRST_TYPE_METHOD);
}

gboolean
girst_parser_object_has_functions (GirstParserObject *self)
{
  return girst_parser_object_has_or_parent_has (self, GIRST_TYPE_FUNCTION);
}

/**
 * girst_parser_object_get_parameters:
 *
 * Returns: (transfer full): A #GListModel
 */
GListModel *
girst_parser_object_get_parameters (GirstParserObject *self)
{
  GListStore *store;
  GirstParserObject *obj;

  g_return_val_if_fail (GIRST_IS_PARSER_OBJECT (self), NULL);

  store = g_list_store_new (GIRST_TYPE_PARSER_OBJECT);

  obj = girst_parser_object_first_typed (self, GIRST_TYPE_PARAMETERS);

  if (obj != NULL)
    {
      GPtrArray *ar = girst_parser_object_get_children (obj);

      for (guint i = 0; i < ar->len; i++)
        {
          GirstParserObject *child = g_ptr_array_index (ar, i);

          if (GIRST_IS_INSTANCE_PARAMETER (child) || GIRST_IS_PARAMETER (child))
            g_list_store_append (store, child);
        }
    }

  return G_LIST_MODEL (store);
}

gchar *
girst_parser_object_short_cdoc (GirstParserObject *self)
{
  GirstParserObject *obj;

  g_return_val_if_fail (GIRST_IS_PARSER_OBJECT (self), NULL);

  if ((obj = girst_parser_object_first_typed (self, GIRST_TYPE_DOC)))
    return girst_one_line (girst_doc_get_inner_text (GIRST_DOC (obj)), GIRST_LANGUAGE_C);

  return NULL;
}

static gchar *
format_parameter (const Parameter *param,
                  guint            max_type,
                  guint            max_star)
{
  GString *str;
  guint i;

  if (param->ellipsis)
    return g_strdup ("...");

  str = g_string_new (param->type);

  for (i = str->len; i < max_type; i++)
    g_string_append_c (str, ' ');

  g_string_append_c (str, ' ');

  for (i = max_star; i > 0; i--)
    {
      if (i <= param->n_star)
        g_string_append_c (str, '*');
      else
        g_string_append_c (str, ' ');
    }

  g_string_append (str, param->name);

  return g_string_free (str, FALSE);
}

static gchar *
reformat_parameters (const gchar *input,
                     guint        prefix)
{
  GSList *params = parse_parameters (input);
  g_autoptr(GString) join = g_string_new (",\n");
  GString *str = g_string_new (NULL);
  guint max_star = 0;
  guint max_type = 0;

  if (girst_str_empty0 (input) || girst_str_equal0 (input, "void"))
    {
      g_string_append (str, "void");
      goto finish;
    }

  for (guint i = 0; i < prefix; i++)
    g_string_append_c (join, ' ');

  for (const GSList *iter = params; iter; iter = iter->next)
    {
      Parameter *p = iter->data;

      if (p->n_star)
        max_star = MAX (max_star, p->n_star);
      if (p->type)
        max_type = MAX (max_type, strlen (p->type));
    }

  for (const GSList *iter = params; iter; iter = iter->next)
    {
      g_autofree gchar *param_str = NULL;

      if (iter != params)
        g_string_append_len (str, join->str, join->len);
      param_str = format_parameter (iter->data, max_type, max_star);
      g_string_append (str, param_str);
    }

finish:
  g_slist_free_full (params, (GDestroyNotify)parameter_free);
  return g_string_free (str, FALSE);
}

gchar *
girst_parser_object_get_signature (GirstParserObject *self)
{
  g_autoptr(GString) str = NULL;
  g_autoptr(GString) param_str = NULL;
  const gchar *container_c_type = NULL;
  g_autofree gchar *formatted = NULL;
  GirstParserObject *return_value;
  GirstParserObject *params;
  gboolean found_a_param = FALSE;
  guint space = 0;
  guint begin;

  g_return_val_if_fail (GIRST_IS_PARSER_OBJECT (self), NULL);

  if (!GIRST_IS_FUNCTION (self) &&
      !GIRST_IS_CALLBACK (self) &&
      !GIRST_IS_CONSTRUCTOR (self) &&
      !GIRST_IS_GLIB_SIGNAL (self) &&
      !GIRST_IS_VIRTUAL_METHOD (self) &&
      !GIRST_IS_METHOD (self))
    return NULL;

  container_c_type = find_object_c_type (self);

  str = g_string_new (NULL);
  param_str = g_string_new (NULL);

  /* Add our return type on a single line */

  if ((return_value = girst_parser_object_first_typed (self, GIRST_TYPE_RETURN_VALUE)))
    {
      g_autofree gchar *return_c_type = girst_return_value_get_lhs_ctype (GIRST_RETURN_VALUE (return_value));

      g_string_append (str, return_c_type);
    }
  else
    {
      g_string_append (str, "void");
    }

  g_string_append (str, "\n   ");
  begin = str->len;

  /* Now the function name (or callback/vtable definition) */

  if (GIRST_IS_CALLBACK (self))
    {
      const gchar *c_type = girst_callback_get_c_type (GIRST_CALLBACK (self));
      const gchar *name = girst_callback_get_name (GIRST_CALLBACK (self));

      if (c_type != NULL)
        g_string_append_printf (str, "(*%s) ", c_type);
      else
        g_string_append_printf (str, "%s ", name);
    }
  else if (GIRST_IS_FUNCTION (self))
    {
      const gchar *c_identifier = girst_function_get_c_identifier (GIRST_FUNCTION (self));

      g_string_append_printf (str, "%s ", c_identifier);
    }
  else if (GIRST_IS_CONSTRUCTOR (self))
    {
      const gchar *c_identifier = girst_constructor_get_c_identifier (GIRST_CONSTRUCTOR (self));

      g_string_append_printf (str, "%s ", c_identifier);
    }
  else if (GIRST_IS_METHOD (self))
    {
      const gchar *c_identifier = girst_method_get_c_identifier (GIRST_METHOD (self));

      g_string_append_printf (str, "%s ", c_identifier);
    }
  else if (GIRST_IS_GLIB_SIGNAL (self))
    {
      g_string_append (str, "user_function ");
    }
  else if (GIRST_IS_VIRTUAL_METHOD (self))
    {
      const gchar *c_identifier = girst_virtual_method_get_c_identifier (GIRST_VIRTUAL_METHOD (self));
      const gchar *name = girst_virtual_method_get_name (GIRST_VIRTUAL_METHOD (self));

      if (c_identifier != NULL)
        g_string_append_printf (str, "%s ", c_identifier);
      else
        g_string_append_printf (str, "(*%s) ", name);
    }
  else
    g_return_val_if_reached (NULL);

  g_string_append_c (str, '(');

  space = str->len - begin;

  /* XXX: Until we have a better way to ensure this
   *      is indented from the template code.
   */
  space += 3;

  params = girst_parser_object_first_typed (self, GIRST_TYPE_PARAMETERS);

  if (GIRST_IS_GLIB_SIGNAL (self))
    {
      g_string_append_printf (param_str, "%s *self, ", container_c_type);
      found_a_param = TRUE;
    }

  if (params != NULL)
    {
      GPtrArray *children;

      children = girst_parser_object_get_children (params);

      for (guint i = 0; i < children->len; i++)
        {
          GirstParserObject *child = g_ptr_array_index (children, i);
          g_autofree gchar *c_type = NULL;
          g_autofree gchar *name = NULL;

          if (!GIRST_IS_PARAMETER (child) &&
              !GIRST_IS_INSTANCE_PARAMETER (child))
            continue;

          found_a_param = TRUE;

          g_string_append_c (param_str, ' ');

          g_object_get (child, "name", &name, NULL);
          c_type = girst_parser_object_get_lhs_ctype (child);

          if (g_str_equal (name, "..."))
            g_string_append (param_str, "..., ");
          else
            g_string_append_printf (param_str, "%s %s, ", c_type, name);
        }
    }

  if (GIRST_IS_GLIB_SIGNAL (self))
    g_string_append (param_str, "gpointer user_data, ");

  if (found_a_param && param_str->len > 2)
    {
      g_string_truncate (param_str, param_str->len - 2);
      formatted = reformat_parameters (param_str->str, space);
      g_string_append (str, formatted);
      g_string_append (str, ");");
    }
  else
    {
      g_string_append (str, "void);");
    }

  return g_string_free (g_steal_pointer (&str), FALSE);
}
