/* girst-varargs.c
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "girst-varargs"

#include "girst-varargs.h"

struct _GirstVarargs
{
  GObject parent_instance;
};

G_DEFINE_TYPE (GirstVarargs, girst_varargs, GIRST_TYPE_PARSER_OBJECT)

static gboolean
girst_varargs_ingest (GirstParserObject *object,
                      GMarkupParseContext *context,
                      const gchar *element_name,
                      const gchar **attribute_names,
                      const gchar **attribute_values,
                      GError **error)
{
  GirstVarargs *self = (GirstVarargs *)object;

  g_assert (GIRST_IS_VARARGS (self));
  g_assert (g_str_equal (element_name, "varargs"));


  return TRUE;
}

static void
girst_varargs_printf (GirstParserObject *object,
                      GString *str,
                      guint depth)
{
  GirstVarargs *self = (GirstVarargs *)object;
  guint i;

  g_assert (GIRST_IS_VARARGS (self));

  for (i = 0; i < depth; i++)
    g_string_append (str, "  ");
  g_string_append (str, "<varargs");


  g_string_append (str, "/>\n");
}

static void
girst_varargs_finalize (GObject *object)
{

  G_OBJECT_CLASS (girst_varargs_parent_class)->finalize (object);
}

static void
girst_varargs_class_init (GirstVarargsClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GirstParserObjectClass *parent_class = GIRST_PARSER_OBJECT_CLASS (klass);

  object_class->finalize = girst_varargs_finalize;

  parent_class->ingest = girst_varargs_ingest;
  parent_class->printf = girst_varargs_printf;
}

static void
girst_varargs_init (GirstVarargs *self)
{
}

GirstVarargs *
girst_varargs_new (GirstParserContext *parser_context)
{
  return g_object_new (GIRST_TYPE_VARARGS,
                       "parser-context", parser_context,
                       NULL);
}
