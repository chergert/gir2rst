/* girst-instance-parameter.c
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "girst-instance-parameter"

#include "girst-instance-parameter.h"

#include "girst-doc-version.h"
#include "girst-doc-stability.h"
#include "girst-doc.h"
#include "girst-doc-deprecated.h"
#include "girst-type.h"

struct _GirstInstanceParameter
{
  GObject parent_instance;
  const gchar *name;
  const gchar *nullable;
  const gchar *allow_none;
  const gchar *direction;
  const gchar *caller_allocates;
  const gchar *transfer_ownership;
  GPtrArray *children;
};

G_DEFINE_TYPE (GirstInstanceParameter, girst_instance_parameter, GIRST_TYPE_PARSER_OBJECT)

enum {
  PROP_0,
  PROP_NAME,
  PROP_NULLABLE,
  PROP_ALLOW_NONE,
  PROP_DIRECTION,
  PROP_CALLER_ALLOCATES,
  PROP_TRANSFER_OWNERSHIP,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static GPtrArray *
girst_instance_parameter_get_children (GirstParserObject *object)
{
  GirstInstanceParameter *self = (GirstInstanceParameter *)object;

  g_assert (GIRST_IS_INSTANCE_PARAMETER (self));

  return self->children;
}

static void
girst_instance_parameter_start_element (GMarkupParseContext *context,
                                        const gchar *element_name,
                                        const gchar **attribute_names,
                                        const gchar **attribute_values,
                                        gpointer user_data,
                                        GError **error)
{
  GirstInstanceParameter *self = user_data;
  GirstParserContext *parser_context;

  g_assert (GIRST_IS_INSTANCE_PARAMETER (self));
  g_assert (context != NULL);
  g_assert (element_name != NULL);
  g_assert (attribute_names != NULL);
  g_assert (attribute_values != NULL);

  parser_context = girst_parser_object_get_parser_context (GIRST_PARSER_OBJECT (self));

  if (FALSE) {}
  else if (g_str_equal (element_name, "doc-version"))
    {
      g_autoptr(GirstDocVersion) child = NULL;

      child = girst_doc_version_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "doc-stability"))
    {
      g_autoptr(GirstDocStability) child = NULL;

      child = girst_doc_stability_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "doc"))
    {
      g_autoptr(GirstDoc) child = NULL;

      child = girst_doc_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "doc-deprecated"))
    {
      g_autoptr(GirstDocDeprecated) child = NULL;

      child = girst_doc_deprecated_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "type"))
    {
      g_autoptr(GirstType) child = NULL;

      child = girst_type_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
}

static void
girst_instance_parameter_end_element (GMarkupParseContext *context,
                                      const gchar *element_name,
                                      gpointer user_data,
                                      GError **error)
{
  g_assert (GIRST_IS_INSTANCE_PARAMETER (user_data));
  g_assert (context != NULL);
  g_assert (element_name != NULL);

  if (FALSE) {}
  else if (g_str_equal (element_name, "doc-version"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "doc-stability"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "doc"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "doc-deprecated"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "type"))
    {
      g_markup_parse_context_pop (context);
    }
}

static const GMarkupParser markup_parser = {
  girst_instance_parameter_start_element,
  girst_instance_parameter_end_element,
  NULL,
  NULL,
  NULL,
};

static gboolean
girst_instance_parameter_ingest (GirstParserObject *object,
                                 GMarkupParseContext *context,
                                 const gchar *element_name,
                                 const gchar **attribute_names,
                                 const gchar **attribute_values,
                                 GError **error)
{
  GirstInstanceParameter *self = (GirstInstanceParameter *)object;
  GirstParserContext *parser_context;
  const gchar *name = NULL;
  const gchar *nullable = NULL;
  const gchar *allow_none = NULL;
  const gchar *direction = NULL;
  const gchar *caller_allocates = NULL;
  const gchar *transfer_ownership = NULL;

  g_assert (GIRST_IS_INSTANCE_PARAMETER (self));
  g_assert (g_str_equal (element_name, "instance-parameter"));

  parser_context = girst_parser_object_get_parser_context (GIRST_PARSER_OBJECT (self));


  if (!girst_g_markup_collect_attributes (element_name, attribute_names, attribute_values, error,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "name", &name,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "nullable", &nullable,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "allow-none", &allow_none,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "direction", &direction,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "caller-allocates", &caller_allocates,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "transfer-ownership", &transfer_ownership,
                                          G_MARKUP_COLLECT_INVALID, NULL, NULL))
    return FALSE;

  self->name = girst_parser_context_intern_string (parser_context, name);
  self->nullable = girst_parser_context_intern_string (parser_context, nullable);
  self->allow_none = girst_parser_context_intern_string (parser_context, allow_none);
  self->direction = girst_parser_context_intern_string (parser_context, direction);
  self->caller_allocates = girst_parser_context_intern_string (parser_context, caller_allocates);
  self->transfer_ownership = girst_parser_context_intern_string (parser_context, transfer_ownership);

  g_markup_parse_context_push (context, &markup_parser, self);

  return TRUE;
}

static void
girst_instance_parameter_printf (GirstParserObject *object,
                                 GString *str,
                                 guint depth)
{
  GirstInstanceParameter *self = (GirstInstanceParameter *)object;
  guint i;

  g_assert (GIRST_IS_INSTANCE_PARAMETER (self));

  for (i = 0; i < depth; i++)
    g_string_append (str, "  ");
  g_string_append (str, "<instance-parameter");

  if (self->name != NULL)
    g_string_append_printf (str, " name=\"%s\"", self->name);
  if (self->nullable != NULL)
    g_string_append_printf (str, " nullable=\"%s\"", self->nullable);
  if (self->allow_none != NULL)
    g_string_append_printf (str, " allow-none=\"%s\"", self->allow_none);
  if (self->direction != NULL)
    g_string_append_printf (str, " direction=\"%s\"", self->direction);
  if (self->caller_allocates != NULL)
    g_string_append_printf (str, " caller-allocates=\"%s\"", self->caller_allocates);
  if (self->transfer_ownership != NULL)
    g_string_append_printf (str, " transfer-ownership=\"%s\"", self->transfer_ownership);

  if (self->children != NULL && self->children->len > 0)
    {
      g_string_append (str, ">\n");

      for (i = 0; i < self->children->len; i++)
        girst_parser_object_printf (g_ptr_array_index (self->children, i), str, depth + 1);

      for (i = 0; i < depth; i++)
        g_string_append (str, "  ");
      g_string_append (str, "</instance-parameter>\n");
    }
  else
    {
      g_string_append (str, "/>\n");
    }
}

static void
girst_instance_parameter_get_property (GObject    *object,
                                       guint       prop_id,
                                       GValue     *value,
                                       GParamSpec *pspec)
{
  GirstInstanceParameter *self = (GirstInstanceParameter *)object;

  switch (prop_id)
    {
    case PROP_NAME:
      g_value_set_string (value, self->name);
      break;

    case PROP_NULLABLE:
      g_value_set_string (value, self->nullable);
      break;

    case PROP_ALLOW_NONE:
      g_value_set_string (value, self->allow_none);
      break;

    case PROP_DIRECTION:
      g_value_set_string (value, self->direction);
      break;

    case PROP_CALLER_ALLOCATES:
      g_value_set_string (value, self->caller_allocates);
      break;

    case PROP_TRANSFER_OWNERSHIP:
      g_value_set_string (value, self->transfer_ownership);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
girst_instance_parameter_set_property (GObject      *object,
                                       guint         prop_id,
                                       const GValue *value,
                                       GParamSpec   *pspec)
{
  GirstInstanceParameter *self = (GirstInstanceParameter *)object;
  GirstParserContext *context = girst_parser_object_get_parser_context (GIRST_PARSER_OBJECT (self));

  switch (prop_id)
    {
    case PROP_NAME:
      self->name = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_NULLABLE:
      self->nullable = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_ALLOW_NONE:
      self->allow_none = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_DIRECTION:
      self->direction = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_CALLER_ALLOCATES:
      self->caller_allocates = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_TRANSFER_OWNERSHIP:
      self->transfer_ownership = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
girst_instance_parameter_finalize (GObject *object)
{
  GirstInstanceParameter *self = (GirstInstanceParameter *)object;

  g_clear_pointer (&self->children, g_ptr_array_unref);

  G_OBJECT_CLASS (girst_instance_parameter_parent_class)->finalize (object);
}

static void
girst_instance_parameter_class_init (GirstInstanceParameterClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GirstParserObjectClass *parent_class = GIRST_PARSER_OBJECT_CLASS (klass);

  object_class->get_property = girst_instance_parameter_get_property;
  object_class->set_property = girst_instance_parameter_set_property;
  object_class->finalize = girst_instance_parameter_finalize;

  parent_class->ingest = girst_instance_parameter_ingest;
  parent_class->printf = girst_instance_parameter_printf;
  parent_class->get_children = girst_instance_parameter_get_children;

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "name",
                         "name",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_NULLABLE] =
    g_param_spec_string ("nullable",
                         "nullable",
                         "nullable",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_ALLOW_NONE] =
    g_param_spec_string ("allow-none",
                         "allow-none",
                         "allow-none",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_DIRECTION] =
    g_param_spec_string ("direction",
                         "direction",
                         "direction",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_CALLER_ALLOCATES] =
    g_param_spec_string ("caller-allocates",
                         "caller-allocates",
                         "caller-allocates",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_TRANSFER_OWNERSHIP] =
    g_param_spec_string ("transfer-ownership",
                         "transfer-ownership",
                         "transfer-ownership",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
girst_instance_parameter_init (GirstInstanceParameter *self)
{
  self->children = g_ptr_array_new_with_free_func (g_object_unref);
}

const gchar *
girst_instance_parameter_get_name (GirstInstanceParameter *self)
{
  g_return_val_if_fail (GIRST_IS_INSTANCE_PARAMETER (self), NULL);

  return self->name;
}

const gchar *
girst_instance_parameter_get_nullable (GirstInstanceParameter *self)
{
  g_return_val_if_fail (GIRST_IS_INSTANCE_PARAMETER (self), NULL);

  return self->nullable;
}

const gchar *
girst_instance_parameter_get_allow_none (GirstInstanceParameter *self)
{
  g_return_val_if_fail (GIRST_IS_INSTANCE_PARAMETER (self), NULL);

  return self->allow_none;
}

const gchar *
girst_instance_parameter_get_direction (GirstInstanceParameter *self)
{
  g_return_val_if_fail (GIRST_IS_INSTANCE_PARAMETER (self), NULL);

  return self->direction;
}

const gchar *
girst_instance_parameter_get_caller_allocates (GirstInstanceParameter *self)
{
  g_return_val_if_fail (GIRST_IS_INSTANCE_PARAMETER (self), NULL);

  return self->caller_allocates;
}

const gchar *
girst_instance_parameter_get_transfer_ownership (GirstInstanceParameter *self)
{
  g_return_val_if_fail (GIRST_IS_INSTANCE_PARAMETER (self), NULL);

  return self->transfer_ownership;
}

GirstInstanceParameter *
girst_instance_parameter_new (GirstParserContext *parser_context)
{
  return g_object_new (GIRST_TYPE_INSTANCE_PARAMETER,
                       "parser-context", parser_context,
                       NULL);
}
