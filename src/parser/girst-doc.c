/* girst-doc.c
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "girst-doc"

#include "girst-doc.h"

struct _GirstDoc
{
  GObject parent_instance;
  GString *text;
  const gchar *xml_space;
  const gchar *xml_whitespace;
};

G_DEFINE_TYPE (GirstDoc, girst_doc, GIRST_TYPE_PARSER_OBJECT)

enum {
  PROP_0,
  PROP_INNER_TEXT,
  PROP_XML_SPACE,
  PROP_XML_WHITESPACE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

const gchar *
girst_doc_get_inner_text (GirstDoc *self)
{
  return self->text != NULL ? self->text->str : NULL;
}

static void
girst_doc_text (GMarkupParseContext *context,
                const gchar *text,
                gsize text_len,
                gpointer user_data,
                GError **error)
{
  GirstDoc *self = user_data;

  g_assert (context != NULL);
  g_assert (text != NULL);
  g_assert (GIRST_IS_DOC (self));

  if (self->text == NULL)
    self->text = g_string_new_len (text, text_len);
  else
    g_string_append_len (self->text, text, text_len);
}

static const GMarkupParser markup_parser = {
  NULL,
  NULL,
  girst_doc_text,
  NULL,
  NULL,
};

static gboolean
girst_doc_ingest (GirstParserObject *object,
                  GMarkupParseContext *context,
                  const gchar *element_name,
                  const gchar **attribute_names,
                  const gchar **attribute_values,
                  GError **error)
{
  GirstDoc *self = (GirstDoc *)object;
  GirstParserContext *parser_context;
  const gchar *xml_space = NULL;
  const gchar *xml_whitespace = NULL;

  g_assert (GIRST_IS_DOC (self));
  g_assert (g_str_equal (element_name, "doc"));

  parser_context = girst_parser_object_get_parser_context (GIRST_PARSER_OBJECT (self));


  if (!girst_g_markup_collect_attributes (element_name, attribute_names, attribute_values, error,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "xml:space", &xml_space,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "xml:whitespace", &xml_whitespace,
                                          G_MARKUP_COLLECT_INVALID, NULL, NULL))
    return FALSE;

  self->xml_space = girst_parser_context_intern_string (parser_context, xml_space);
  self->xml_whitespace = girst_parser_context_intern_string (parser_context, xml_whitespace);

  g_markup_parse_context_push (context, &markup_parser, self);

  return TRUE;
}

static void
girst_doc_printf (GirstParserObject *object,
                  GString *str,
                  guint depth)
{
  GirstDoc *self = (GirstDoc *)object;
  guint i;

  g_assert (GIRST_IS_DOC (self));

  for (i = 0; i < depth; i++)
    g_string_append (str, "  ");
  g_string_append (str, "<doc");

  if (self->xml_space != NULL)
    g_string_append_printf (str, " xml:space=\"%s\"", self->xml_space);
  if (self->xml_whitespace != NULL)
    g_string_append_printf (str, " xml:whitespace=\"%s\"", self->xml_whitespace);

  if (self->text != NULL && self->text->len)
    {
      g_autofree gchar *escaped = g_markup_escape_text (self->text->str, self->text->len);
      g_string_append_printf (str, ">%s</doc>\n", escaped);
    }
  else
    g_string_append (str, "></doc>\n");
}

static void
girst_doc_get_property (GObject    *object,
                        guint       prop_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
  GirstDoc *self = (GirstDoc *)object;

  switch (prop_id)
    {
    case PROP_INNER_TEXT:
      g_value_set_string (value, girst_doc_get_inner_text (self));
      break;

    case PROP_XML_SPACE:
      g_value_set_string (value, self->xml_space);
      break;

    case PROP_XML_WHITESPACE:
      g_value_set_string (value, self->xml_whitespace);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
girst_doc_set_property (GObject      *object,
                        guint         prop_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
  GirstDoc *self = (GirstDoc *)object;
  GirstParserContext *context = girst_parser_object_get_parser_context (GIRST_PARSER_OBJECT (self));

  switch (prop_id)
    {
    case PROP_XML_SPACE:
      self->xml_space = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_XML_WHITESPACE:
      self->xml_whitespace = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
girst_doc_finalize (GObject *object)
{
  GirstDoc *self = (GirstDoc *)object;

  if (self->text != NULL)
    {
      g_string_free (self->text, TRUE);
      self->text = NULL;
    }


  G_OBJECT_CLASS (girst_doc_parent_class)->finalize (object);
}

static void
girst_doc_class_init (GirstDocClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GirstParserObjectClass *parent_class = GIRST_PARSER_OBJECT_CLASS (klass);

  object_class->get_property = girst_doc_get_property;
  object_class->set_property = girst_doc_set_property;
  object_class->finalize = girst_doc_finalize;

  parent_class->ingest = girst_doc_ingest;
  parent_class->printf = girst_doc_printf;

  properties [PROP_INNER_TEXT] =
    g_param_spec_string ("inner-text",
                         "Inner Text",
                         "Inner Text",
                         NULL,
                         (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  properties [PROP_XML_SPACE] =
    g_param_spec_string ("xml-space",
                         "xml-space",
                         "xml-space",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_XML_WHITESPACE] =
    g_param_spec_string ("xml-whitespace",
                         "xml-whitespace",
                         "xml-whitespace",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
girst_doc_init (GirstDoc *self)
{
}

const gchar *
girst_doc_get_xml_space (GirstDoc *self)
{
  g_return_val_if_fail (GIRST_IS_DOC (self), NULL);

  return self->xml_space;
}

const gchar *
girst_doc_get_xml_whitespace (GirstDoc *self)
{
  g_return_val_if_fail (GIRST_IS_DOC (self), NULL);

  return self->xml_whitespace;
}

GirstDoc *
girst_doc_new (GirstParserContext *parser_context)
{
  return g_object_new (GIRST_TYPE_DOC,
                       "parser-context", parser_context,
                       NULL);
}
