/* girst-doc.h
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GIRST_DOC_H
#define GIRST_DOC_H

#include "girst-parser-types.h"

G_BEGIN_DECLS

GirstDoc *girst_doc_new (GirstParserContext *parser_context);

const gchar *girst_doc_get_inner_text (GirstDoc *self);

const gchar *girst_doc_get_xml_space (GirstDoc *self);

const gchar *girst_doc_get_xml_whitespace (GirstDoc *self);

G_END_DECLS

#endif /* GIRST_DOC */
