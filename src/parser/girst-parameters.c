/* girst-parameters.c
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "girst-parameters"

#include "girst-parameters.h"

#include "girst-parameter.h"
#include "girst-instance-parameter.h"

struct _GirstParameters
{
  GObject parent_instance;
  GPtrArray *children;
};

G_DEFINE_TYPE (GirstParameters, girst_parameters, GIRST_TYPE_PARSER_OBJECT)

static GPtrArray *
girst_parameters_get_children (GirstParserObject *object)
{
  GirstParameters *self = (GirstParameters *)object;

  g_assert (GIRST_IS_PARAMETERS (self));

  return self->children;
}

static void
girst_parameters_start_element (GMarkupParseContext *context,
                                const gchar *element_name,
                                const gchar **attribute_names,
                                const gchar **attribute_values,
                                gpointer user_data,
                                GError **error)
{
  GirstParameters *self = user_data;
  GirstParserContext *parser_context;

  g_assert (GIRST_IS_PARAMETERS (self));
  g_assert (context != NULL);
  g_assert (element_name != NULL);
  g_assert (attribute_names != NULL);
  g_assert (attribute_values != NULL);

  parser_context = girst_parser_object_get_parser_context (GIRST_PARSER_OBJECT (self));

  if (FALSE) {}
  else if (g_str_equal (element_name, "parameter"))
    {
      g_autoptr(GirstParameter) child = NULL;

      child = girst_parameter_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "instance-parameter"))
    {
      g_autoptr(GirstInstanceParameter) child = NULL;

      child = girst_instance_parameter_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
}

static void
girst_parameters_end_element (GMarkupParseContext *context,
                              const gchar *element_name,
                              gpointer user_data,
                              GError **error)
{
  g_assert (GIRST_IS_PARAMETERS (user_data));
  g_assert (context != NULL);
  g_assert (element_name != NULL);

  if (FALSE) {}
  else if (g_str_equal (element_name, "parameter"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "instance-parameter"))
    {
      g_markup_parse_context_pop (context);
    }
}

static const GMarkupParser markup_parser = {
  girst_parameters_start_element,
  girst_parameters_end_element,
  NULL,
  NULL,
  NULL,
};

static gboolean
girst_parameters_ingest (GirstParserObject *object,
                         GMarkupParseContext *context,
                         const gchar *element_name,
                         const gchar **attribute_names,
                         const gchar **attribute_values,
                         GError **error)
{
  GirstParameters *self = (GirstParameters *)object;

  g_assert (GIRST_IS_PARAMETERS (self));
  g_assert (g_str_equal (element_name, "parameters"));


  g_markup_parse_context_push (context, &markup_parser, self);

  return TRUE;
}

static void
girst_parameters_printf (GirstParserObject *object,
                         GString *str,
                         guint depth)
{
  GirstParameters *self = (GirstParameters *)object;
  guint i;

  g_assert (GIRST_IS_PARAMETERS (self));

  for (i = 0; i < depth; i++)
    g_string_append (str, "  ");
  g_string_append (str, "<parameters");


  if (self->children != NULL && self->children->len > 0)
    {
      g_string_append (str, ">\n");

      for (i = 0; i < self->children->len; i++)
        girst_parser_object_printf (g_ptr_array_index (self->children, i), str, depth + 1);

      for (i = 0; i < depth; i++)
        g_string_append (str, "  ");
      g_string_append (str, "</parameters>\n");
    }
  else
    {
      g_string_append (str, "/>\n");
    }
}

static void
girst_parameters_finalize (GObject *object)
{
  GirstParameters *self = (GirstParameters *)object;

  g_clear_pointer (&self->children, g_ptr_array_unref);

  G_OBJECT_CLASS (girst_parameters_parent_class)->finalize (object);
}

static void
girst_parameters_class_init (GirstParametersClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GirstParserObjectClass *parent_class = GIRST_PARSER_OBJECT_CLASS (klass);

  object_class->finalize = girst_parameters_finalize;

  parent_class->ingest = girst_parameters_ingest;
  parent_class->printf = girst_parameters_printf;
  parent_class->get_children = girst_parameters_get_children;
}

static void
girst_parameters_init (GirstParameters *self)
{
  self->children = g_ptr_array_new_with_free_func (g_object_unref);
}

GirstParameters *
girst_parameters_new (GirstParserContext *parser_context)
{
  return g_object_new (GIRST_TYPE_PARAMETERS,
                       "parser-context", parser_context,
                       NULL);
}
