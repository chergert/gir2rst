/* girst-property.h
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GIRST_PROPERTY_H
#define GIRST_PROPERTY_H

#include "girst-parser-types.h"

G_BEGIN_DECLS

GirstProperty *girst_property_new (GirstParserContext *parser_context);

const gchar *girst_property_get_introspectable (GirstProperty *self);

const gchar *girst_property_get_deprecated (GirstProperty *self);

const gchar *girst_property_get_deprecated_version (GirstProperty *self);

const gchar *girst_property_get_version (GirstProperty *self);

const gchar *girst_property_get_stability (GirstProperty *self);

const gchar *girst_property_get_name (GirstProperty *self);

const gchar *girst_property_get_writable (GirstProperty *self);

const gchar *girst_property_get_readable (GirstProperty *self);

const gchar *girst_property_get_construct (GirstProperty *self);

const gchar *girst_property_get_construct_only (GirstProperty *self);

const gchar *girst_property_get_transfer_ownership (GirstProperty *self);

G_END_DECLS

#endif /* GIRST_PROPERTY */
