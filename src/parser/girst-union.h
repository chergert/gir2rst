/* girst-union.h
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GIRST_UNION_H
#define GIRST_UNION_H

#include "girst-parser-types.h"

G_BEGIN_DECLS

GirstUnion *girst_union_new (GirstParserContext *parser_context);

const gchar *girst_union_get_introspectable (GirstUnion *self);

const gchar *girst_union_get_deprecated (GirstUnion *self);

const gchar *girst_union_get_deprecated_version (GirstUnion *self);

const gchar *girst_union_get_version (GirstUnion *self);

const gchar *girst_union_get_stability (GirstUnion *self);

const gchar *girst_union_get_name (GirstUnion *self);

const gchar *girst_union_get_c_type (GirstUnion *self);

const gchar *girst_union_get_c_symbol_prefix (GirstUnion *self);

const gchar *girst_union_get_glib_get_type (GirstUnion *self);

const gchar *girst_union_get_glib_type_name (GirstUnion *self);

G_END_DECLS

#endif /* GIRST_UNION */
