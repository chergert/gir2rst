/* girst-constant.h
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GIRST_CONSTANT_H
#define GIRST_CONSTANT_H

#include "girst-parser-types.h"

G_BEGIN_DECLS

GirstConstant *girst_constant_new (GirstParserContext *parser_context);

const gchar *girst_constant_get_introspectable (GirstConstant *self);

const gchar *girst_constant_get_deprecated (GirstConstant *self);

const gchar *girst_constant_get_deprecated_version (GirstConstant *self);

const gchar *girst_constant_get_version (GirstConstant *self);

const gchar *girst_constant_get_stability (GirstConstant *self);

const gchar *girst_constant_get_name (GirstConstant *self);

const gchar *girst_constant_get_value (GirstConstant *self);

const gchar *girst_constant_get_c_type (GirstConstant *self);

const gchar *girst_constant_get_c_identifier (GirstConstant *self);

G_END_DECLS

#endif /* GIRST_CONSTANT */
