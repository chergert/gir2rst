/* girst-glib-boxed.h
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GIRST_GLIB_BOXED_H
#define GIRST_GLIB_BOXED_H

#include "girst-parser-types.h"

G_BEGIN_DECLS

GirstGlibBoxed *girst_glib_boxed_new (GirstParserContext *parser_context);

const gchar *girst_glib_boxed_get_introspectable (GirstGlibBoxed *self);

const gchar *girst_glib_boxed_get_deprecated (GirstGlibBoxed *self);

const gchar *girst_glib_boxed_get_deprecated_version (GirstGlibBoxed *self);

const gchar *girst_glib_boxed_get_version (GirstGlibBoxed *self);

const gchar *girst_glib_boxed_get_stability (GirstGlibBoxed *self);

const gchar *girst_glib_boxed_get_glib_name (GirstGlibBoxed *self);

const gchar *girst_glib_boxed_get_c_symbol_prefix (GirstGlibBoxed *self);

const gchar *girst_glib_boxed_get_glib_type_name (GirstGlibBoxed *self);

const gchar *girst_glib_boxed_get_glib_get_type (GirstGlibBoxed *self);

G_END_DECLS

#endif /* GIRST_GLIB_BOXED */
