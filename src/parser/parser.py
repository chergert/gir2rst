#!/usr/bin/env python3

import os
import sys
from lxml.etree import ElementTree

import helper

TAG_GRAMMAR = '{http://relaxng.org/ns/structure/1.0}grammar'
TAG_REF = '{http://relaxng.org/ns/structure/1.0}ref'
TAG_START = '{http://relaxng.org/ns/structure/1.0}start'
TAG_DEFINE = '{http://relaxng.org/ns/structure/1.0}define'
TAG_ELEMENT = '{http://relaxng.org/ns/structure/1.0}element'
TAG_OPTIONAL = '{http://relaxng.org/ns/structure/1.0}optional'
TAG_ATTRIBUTE = '{http://relaxng.org/ns/structure/1.0}attribute'
TAG_CHOICE = '{http://relaxng.org/ns/structure/1.0}choice'
TAG_DATA = '{http://relaxng.org/ns/structure/1.0}data'
TAG_EMPTY = '{http://relaxng.org/ns/structure/1.0}empty'
TAG_INTERLEAVE = '{http://relaxng.org/ns/structure/1.0}interleave'
TAG_TEXT = '{http://relaxng.org/ns/structure/1.0}text'
TAG_ZERO_OR_MORE = '{http://relaxng.org/ns/structure/1.0}zeroOrMore'

def resolve(node):
    return helper.findDefine(node.getroottree(), node.attrib['name'])

class Attribute:
    klass = None
    name = None
    node = None

    def __init__(self, name, node):
        self.name = name
        self.node = node

    def __repr__(self):
        return 'Attribute(%r)' % self.name

    def state(self):
        state = self.klass.state()
        state['field'] = self.klass.generator.field(self.name)
        state['FIELD'] = state['field'].upper()
        state['attr'] = self.name
        state['propname'] = self.name.replace(':','-')
        return state

    """
    @property
    def optional(self):
        node = self.node
        while node is not None:
            if node.tag == TAG_OPTIONAL:
                return True
            node = node.getparent()
        return False
    """

    @classmethod
    def discover(self, node):
        for child in helper.getChildAttributes(node):
            attr = Attribute(child.attrib['name'], child)
            yield attr

class StringElement:
    @classmethod
    def discover(self, node):
        for child in node.iter():
            if child.tag == TAG_TEXT:
                return True
        return False

class Class:
    generator = None
    node = None
    name = None
    attributes = None
    string_elements = None
    children = None
    is_text = False

    def __init__(self, generator, node):
        assert node.tag == TAG_ELEMENT
        self.generator = generator
        self.node = node
        self.attributes = []
        self.string_elements = []
        self.children = []
        self.name = node.attrib['name']

    def __repr__(self):
        string = '<%s' % self.name
        if self.attributes:
            for attr in self.attributes:
                string += " %s" % attr.name
        if not self.children:
            if self.is_text:
                string += "></%s>" % self.name
            else:
                string += "/>"
            return string
        string += ">\n"
        for child in self.children:
            string += "  <%s/>\n" % child.name
        string += "</%s>" % self.name
        return string

    def getFilePrefix(self):
        return self.generator.type_name(self.name).replace('_','-')

    def resolve(self):
        for attr in Attribute.discover(self.node):
            attr.klass = self
            self.attributes.append(attr)
        if StringElement.discover(self.node):
            self.is_text = True
        self.resolveChildren()

    def resolveChildren(self):
        for child in helper.getChildElements(self.node):
            other = self.generator.classes[child.attrib['name']]
            self.children.append(other)

    def state(self):
        state = self.generator.state()
        state.update({
            'element_name': self.node.attrib['name'],
            'TypeName': self.generator.TypeName(self.name),
            'type_name': self.generator.type_name(self.name),
            'typealign': ' ' * len(self.generator.type_name(self.name)),
            'type_name_dashed': self.generator.type_name(self.name).replace('_','-'),
            'TYPE_NAME': self.generator.type_name(self.name).upper(),
            'NAME': self.generator.TYPE_NAME(self.name)[len(self.generator.symbol_prefix)+1:],
        })
        return state

    def write(self):
        header = self.generator.openFile(self.getFilePrefix()+'.h')
        header.write('''\
#ifndef %(TYPE_NAME)s_H
#define %(TYPE_NAME)s_H

#include "%(prefix_dashed)s-parser-types.h"

G_BEGIN_DECLS

%(TypeName)s *%(type_name)s_new (%(Prefix)sParserContext *parser_context);
''' % self.state())

        if self.is_text:
            header.write('''
const gchar *%(type_name)s_get_inner_text (%(TypeName)s *self);
''' % self.state())

        for attr in self.attributes:
            header.write('''
const gchar *%(type_name)s_get_%(field)s (%(TypeName)s *self);
''' % attr.state())

        header.write('''
G_END_DECLS

#endif /* %(TYPE_NAME)s */
''' % self.state())

        source = self.generator.openFile(self.getFilePrefix()+'.c')

        source.write('''\
#define G_LOG_DOMAIN "%(type_name_dashed)s"

#include "%(type_name_dashed)s.h"
''' % self.state())

        if self.children:
            source.write('\n')
        for child in self.children:
            source.write('#include "%(type_name_dashed)s.h"\n' % child.state())

        source.write('''
struct _%(TypeName)s
{
  GObject parent_instance;
''' % self.state())

        if self.is_text:
            source.write('''\
  GString *text;
''')

        for attr in self.attributes:
            source.write('''\
  const gchar *%(field)s;
''' % attr.state())

        if self.children:
            source.write('''\
  GPtrArray *children;
''')

        source.write('''\
};

G_DEFINE_TYPE (%(TypeName)s, %(type_name)s, %(PREFIX)s_TYPE_PARSER_OBJECT)
''' % self.state())

        if self.attributes:
            source.write('''
enum {
  PROP_0,
''')
            if self.is_text:
                source.write('  PROP_INNER_TEXT,\n')
            for attr in self.attributes:
                source.write('''\
  PROP_%(FIELD)s,
''' % attr.state())
            source.write('''\
  N_PROPS
};

static GParamSpec *properties [N_PROPS];
''')

        if self.children:
            source.write('''
static GPtrArray *
%(type_name)s_get_children (%(Prefix)sParserObject *object)
{
  %(TypeName)s *self = (%(TypeName)s *)object;

  g_assert (%(PREFIX)s_IS_%(NAME)s (self));

  return self->children;
}
''' % self.state())

            source.write('''
static void
%(type_name)s_start_element (GMarkupParseContext *context,
%(typealign)s                const gchar *element_name,
%(typealign)s                const gchar **attribute_names,
%(typealign)s                const gchar **attribute_values,
%(typealign)s                gpointer user_data,
%(typealign)s                GError **error)
{
  %(TypeName)s *self = user_data;
  %(Prefix)sParserContext *parser_context;

  g_assert (%(PREFIX)s_IS_%(NAME)s (self));
  g_assert (context != NULL);
  g_assert (element_name != NULL);
  g_assert (attribute_names != NULL);
  g_assert (attribute_values != NULL);

  parser_context = %(prefix)s_parser_object_get_parser_context (%(PREFIX)s_PARSER_OBJECT (self));

''' % self.state())

            source.write("  if (FALSE) {}\n")
            for child in self.children:
                source.write('''\
  else if (g_str_equal (element_name, "%(element_name)s"))
    {
      g_autoptr(%(TypeName)s) child = NULL;

      child = %(type_name)s_new (parser_context);

      if (!%(prefix)s_parser_object_ingest (%(PREFIX)s_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _%(prefix)s_parser_object_set_parent (%(PREFIX)s_PARSER_OBJECT (child), %(PREFIX)s_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
''' % child.state())

            source.write('''\
}
''')

            source.write('''
static void
%(type_name)s_end_element (GMarkupParseContext *context,
%(typealign)s              const gchar *element_name,
%(typealign)s              gpointer user_data,
%(typealign)s              GError **error)
{
  g_assert (%(PREFIX)s_IS_%(NAME)s (user_data));
  g_assert (context != NULL);
  g_assert (element_name != NULL);

''' % self.state())

            source.write("  if (FALSE) {}\n")
            for child in self.children:
                if child.children or child.is_text:
                    source.write('''\
  else if (g_str_equal (element_name, "%(element_name)s"))
    {
      g_markup_parse_context_pop (context);
    }
''' % child.state())

            source.write('''\
}
''')

        if self.is_text:
            source.write('''
const gchar *
%(type_name)s_get_inner_text (%(TypeName)s *self)
{
  return self->text != NULL ? self->text->str : NULL;
}

static void
%(type_name)s_text (GMarkupParseContext *context,
%(typealign)s       const gchar *text,
%(typealign)s       gsize text_len,
%(typealign)s       gpointer user_data,
%(typealign)s       GError **error)
{
  %(TypeName)s *self = user_data;

  g_assert (context != NULL);
  g_assert (text != NULL);
  g_assert (%(PREFIX)s_IS_%(NAME)s (self));

  if (self->text == NULL)
    self->text = g_string_new_len (text, text_len);
  else
    g_string_append_len (self->text, text, text_len);
}
''' % self.state())

        if self.is_text or self.children:
            source.write('''
static const GMarkupParser markup_parser = {
''')
        if self.children:
            source.write('''\
  %(type_name)s_start_element,
  %(type_name)s_end_element,
  NULL,
''' % self.state())
        elif self.is_text:
            source.write('''\
  NULL,
  NULL,
  %(type_name)s_text,
''' % self.state())

        if self.is_text or self.children:
            source.write('''\
  NULL,
  NULL,
};
''')

        source.write('''
static gboolean
%(type_name)s_ingest (%(Prefix)sParserObject *object,
%(typealign)s         GMarkupParseContext *context,
%(typealign)s         const gchar *element_name,
%(typealign)s         const gchar **attribute_names,
%(typealign)s         const gchar **attribute_values,
%(typealign)s         GError **error)
{
  %(TypeName)s *self = (%(TypeName)s *)object;
''' % self.state())

        if self.attributes:
            source.write('''\
  %(Prefix)sParserContext *parser_context;
''' % self.state())

        for attr in self.attributes:
            source.write('''\
  const gchar *%(field)s = NULL;
''' % attr.state())

        source.write('''
  g_assert (%(PREFIX)s_IS_%(NAME)s (self));
  g_assert (g_str_equal (element_name, "%(element_name)s"));

''' % self.state())

        if self.attributes:
            source.write('''\
  parser_context = %(prefix)s_parser_object_get_parser_context (%(PREFIX)s_PARSER_OBJECT (self));

''' % self.state())

            source.write('''
  if (!%(prefix)s_g_markup_collect_attributes (element_name, attribute_names, attribute_values, error,
''' % self.state())
            for attr in self.attributes:
                # always use optional for now
                # so that we can read in even invalid files.
                source.write('''\
       %(align)s                              G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "%(attr)s", &%(field)s,
''' % attr.state())
            source.write('''\
       %(align)s                              G_MARKUP_COLLECT_INVALID, NULL, NULL))
    return FALSE;

''' % self.state())

            for attr in self.attributes:
                source.write('''\
  self->%(field)s = %(prefix)s_parser_context_intern_string (parser_context, %(field)s);
''' % attr.state())

        if self.children or self.is_text:
            source.write('''
  g_markup_parse_context_push (context, &markup_parser, self);
''')

        source.write('''
  return TRUE;
}
''' % self.state())

        source.write('''
static void
%(type_name)s_printf (%(Prefix)sParserObject *object,
%(typealign)s         GString *str,
%(typealign)s         guint depth)
{
  %(TypeName)s *self = (%(TypeName)s *)object;
  guint i;

  g_assert (%(PREFIX)s_IS_%(NAME)s (self));

  for (i = 0; i < depth; i++)
    g_string_append (str, "  ");
  g_string_append (str, "<%(element_name)s");

''' % self.state())

        for attr in self.attributes:
            source.write('''\
  if (self->%(field)s != NULL)
    g_string_append_printf (str, " %(attr)s=\\"%%s\\"", self->%(field)s);
''' % attr.state())

        if self.children:
            source.write('''
  if (self->children != NULL && self->children->len > 0)
    {
      g_string_append (str, ">\\n");

      for (i = 0; i < self->children->len; i++)
        %(prefix)s_parser_object_printf (g_ptr_array_index (self->children, i), str, depth + 1);

      for (i = 0; i < depth; i++)
        g_string_append (str, "  ");
      g_string_append (str, "</%(element_name)s>\\n");
    }
  else
    {
      g_string_append (str, "/>\\n");
    }
''' % self.state())
        elif self.is_text:
            source.write('''
  if (self->text != NULL && self->text->len)
    {
      g_autofree gchar *escaped = g_markup_escape_text (self->text->str, self->text->len);
      g_string_append_printf (str, ">%%s</%(element_name)s>\\n", escaped);
    }
  else
    g_string_append (str, "></%(element_name)s>\\n");
''' % self.state())
        else:
            source.write('''
  g_string_append (str, "/>\\n");
''')

        source.write('''\
}
''')

        if self.attributes:
            source.write('''
static void
%(type_name)s_get_property (GObject    *object,
%(typealign)s               guint       prop_id,
%(typealign)s               GValue     *value,
%(typealign)s               GParamSpec *pspec)
{
  %(TypeName)s *self = (%(TypeName)s *)object;

  switch (prop_id)
    {
''' % self.state())

            if self.is_text:
                source.write('''\
    case PROP_INNER_TEXT:
      g_value_set_string (value, %(type_name)s_get_inner_text (self));
      break;

''' % self.state())

            for attr in self.attributes:
                source.write('''\
    case PROP_%(FIELD)s:
      g_value_set_string (value, self->%(field)s);
      break;

''' % attr.state())

            source.write('''\
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}
''' % self.state())

            source.write('''
static void
%(type_name)s_set_property (GObject      *object,
%(typealign)s               guint         prop_id,
%(typealign)s               const GValue *value,
%(typealign)s               GParamSpec   *pspec)
{
  %(TypeName)s *self = (%(TypeName)s *)object;
  %(Prefix)sParserContext *context = %(prefix)s_parser_object_get_parser_context (%(PREFIX)s_PARSER_OBJECT (self));

  switch (prop_id)
    {
''' % self.state())

            for attr in self.attributes:
                source.write('''\
    case PROP_%(FIELD)s:
      self->%(field)s = %(prefix)s_parser_context_intern_string (context, g_value_get_string (value));
      break;

''' % attr.state())

            source.write('''\
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}
''' % self.state())

        source.write('''
static void
%(type_name)s_finalize (GObject *object)
{
''' % self.state())

        if self.is_text or self.children:
            source.write('''\
  %(TypeName)s *self = (%(TypeName)s *)object;

''' % self.state())

        if self.is_text:
            source.write('''\
  if (self->text != NULL)
    {
      g_string_free (self->text, TRUE);
      self->text = NULL;
    }

''')

        if self.children:
            source.write('''\
  g_clear_pointer (&self->children, g_ptr_array_unref);
''')

        source.write('''
  G_OBJECT_CLASS (%(type_name)s_parent_class)->finalize (object);
}
''' % self.state())

        source.write('''
static void
%(type_name)s_class_init (%(TypeName)sClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  %(Prefix)sParserObjectClass *parent_class = %(PREFIX)s_PARSER_OBJECT_CLASS (klass);

''' % self.state())

        if self.attributes:
            source.write('''\
  object_class->get_property = %(type_name)s_get_property;
  object_class->set_property = %(type_name)s_set_property;
''' % self.state())

        source.write('''\
  object_class->finalize = %(type_name)s_finalize;

  parent_class->ingest = %(type_name)s_ingest;
  parent_class->printf = %(type_name)s_printf;
''' % self.state())

        if self.children:
            source.write('''\
  parent_class->get_children = %(type_name)s_get_children;
''' % self.state())

        if self.attributes:
            if self.is_text:
                source.write('''\

  properties [PROP_INNER_TEXT] =
    g_param_spec_string ("inner-text",
                         "Inner Text",
                         "Inner Text",
                         NULL,
                         (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));
''' % self.state())

            for attr in self.attributes:
                source.write('''\

  properties [PROP_%(FIELD)s] =
    g_param_spec_string ("%(propname)s",
                         "%(propname)s",
                         "%(propname)s",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
''' % attr.state())

            source.write('''
  g_object_class_install_properties (object_class, N_PROPS, properties);
''')

        source.write('''\
}
''')

        source.write('''
static void
%(type_name)s_init (%(TypeName)s *self)
{
''' % self.state())

        if self.children:
            source.write('''\
  self->children = g_ptr_array_new_with_free_func (g_object_unref);
''');

        source.write('''\
}
''')

        for attr in self.attributes:
            source.write('''
const gchar *
%(type_name)s_get_%(field)s (%(TypeName)s *self)
{
  g_return_val_if_fail (%(PREFIX)s_IS_%(NAME)s (self), NULL);

  return self->%(field)s;
}
''' % attr.state())

        source.write('''
%(TypeName)s *
%(type_name)s_new (%(Prefix)sParserContext *parser_context)
{
  return g_object_new (%(PREFIX)s_TYPE_%(NAME)s,
                       "parser-context", parser_context,
                       NULL);
}
''' % self.state())

    @classmethod
    def nodeIsAClass(klass, node):
        if node.tag != TAG_ELEMENT:
            return False
        return True

class Generator:
    etree = None
    classes = None
    type_prefix = None
    symbol_prefix = None
    directory = None

    def __init__(self, girfile, type_prefix, symbol_prefix, directory):
        self.classes = {}
        self.etree = ElementTree()
        self.etree.parse(open(girfile))
        self.type_prefix = type_prefix
        self.symbol_prefix = symbol_prefix
        self.directory = directory
        try:
            os.mkdir(directory)
        except FileExistsError:
            pass

    def process(self):
        # First we need to find all defines that look like they will
        # need a type defined for them.
        for node in helper.getClasses(self.etree.getroot()):
            klass = Class(self, node)
            self.classes[klass.name] = klass

            for child in helper.getChildClasses(node):
                klass = Class(self, child)
                self.classes[klass.name] = klass

        # Now we need to go resolve everything we need once we have
        # a complete view of the tree.
        for klass in self.classes.values():
            klass.resolve()

    def findClassForDefine(self, name):
        for klass in self.classes.values():
            node = klass.node
            while node is not None:
                if node.tag == TAG_ELEMENT and node.attrib['name'] == name:
                    return klass
                node = node.getparent()
        return None

    def openFile(self, filename):
        f = open(os.path.join(self.directory, filename), 'w')
        self.writeHeader(f)
        return f

    def writeHeader(self, stream):
        state = {'filename': os.path.basename(stream.name)}
        stream.write('''\
/* %(filename)s
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

''' % state)

    def getStartElement(self):
        for node in self.etree.iter():
            if node.tag == TAG_START:
                ref = resolve(node.getchildren()[0])
                if ref is not None:
                    return ref.getchildren()[0].attrib['name']

    def getStartType(self):
        for node in self.etree.iter():
            if node.tag == TAG_START:
                return node.getchildren()[0].attrib['name']

    def writeFiles(self):
        self.writeParserTypes()
        self.writeParserObject()
        self.writeParser()
        for klass in self.classes.values():
            klass.write()

    def state(self):
        return {
            'prefix_dashed': self.symbol_prefix.replace('_','-'),
            'PREFIX': self.symbol_prefix.upper(),
            'Prefix': self.type_prefix,
            'prefix': self.symbol_prefix,
            'align': ' ' * len(self.symbol_prefix),
            'StartType': self.TypeName(self.getStartType()),
            'start_type': self.type_name(self.getStartType()),
            'start_type_dashed': self.type_name_dashed(self.getStartType()),
            'start_element': self.getStartElement(),
        }

    def writeParserTypes(self):
        filename = self.type_name_dashed('parser-types') + '.h'
        stream = self.openFile(filename)
        stream.write('''\
#ifndef %(PREFIX)s_PARSER_TYPES_H
#define %(PREFIX)s_PARSER_TYPES_H

#include <gio/gio.h>

G_BEGIN_DECLS

typedef struct _%(Prefix)sParserContext %(Prefix)sParserContext;

#define %(PREFIX)s_TYPE_PARSER_OBJECT (%(prefix)s_parser_object_get_type())
#define %(PREFIX)s_TYPE_PARSER_CONTEXT (%(prefix)s_parser_context_get_type())

G_DECLARE_DERIVABLE_TYPE (%(Prefix)sParserObject, %(prefix)s_parser_object, %(PREFIX)s, PARSER_OBJECT, GObject)

struct _%(Prefix)sParserObjectClass
{
  GObjectClass parent_class;

  GPtrArray *(*get_children) (%(Prefix)sParserObject *self);
  gboolean   (*ingest)       (%(Prefix)sParserObject *self,
                              GMarkupParseContext *context,
                              const gchar *element_name,
                              const gchar **attribute_names,
                              const gchar **attribute_values,
                              GError **error);
  void       (*printf)       (%(Prefix)sParserObject *self,
                              GString *str,
                              guint depth);
};

''' % self.state())

        for klass in self.classes.values():
            stream.write('''\
#define %(PREFIX)s_TYPE_%(NAME)s (%(type_name)s_get_type())
''' % klass.state())

        for klass in self.classes.values():
            stream.write('''\
G_DECLARE_FINAL_TYPE (%(TypeName)s, %(type_name)s, %(PREFIX)s, %(NAME)s, %(Prefix)sParserObject)
''' % klass.state())

        stream.write('''
%(Prefix)sParserObject *%(prefix)s_parser_object_first_typed (%(Prefix)sParserObject *self, GType child_type);
GPtrArray  *%(prefix)s_parser_object_get_children       (%(Prefix)sParserObject *self);
gboolean    %(prefix)s_parser_object_ingest             (%(Prefix)sParserObject *self,
            %(align)s                                   GMarkupParseContext *context,
            %(align)s                                   const gchar *element_name,
            %(align)s                                   const gchar **attribute_names,
            %(align)s                                   const gchar **attribute_values,
            %(align)s                                   GError **error);
void        %(prefix)s_parser_object_printf             (%(Prefix)sParserObject *self,
            %(align)s                                   GString *str,
            %(align)s                                   guint depth);
GListModel *%(prefix)s_parser_object_get_children_typed (%(Prefix)sParserObject *self,
            %(align)s                                     GType type);
gboolean    %(prefix)s_parser_object_has_child_typed    (%(Prefix)sParserObject *self,
            %(align)s                                    GType type);

void _%(prefix)s_parser_object_set_parent (%(Prefix)sParserObject *self,
      %(align)s                           %(Prefix)sParserObject *parent) G_GNUC_INTERNAL;
%(Prefix)sParserObject *%(prefix)s_parser_object_get_parent (%(Prefix)sParserObject *self);

%(Prefix)sParserContext *%(prefix)s_parser_object_get_parser_context (%(Prefix)sParserObject *self);

gboolean   %(prefix)s_g_markup_collect_attributes (const gchar *element_name,
           %(align)s                              const gchar **attribute_names,
           %(align)s                              const gchar **attribute_values,
           %(align)s                              GError **error,
           %(align)s                              GMarkupCollectType first_type,
           %(align)s                              const gchar *first_attr,
           %(align)s                              ...);

GType %(prefix)s_parser_context_get_type (void);
%(Prefix)sParserContext *%(prefix)s_parser_context_new (void);
%(Prefix)sParserContext *%(prefix)s_parser_context_ref (%(Prefix)sParserContext *self);
void %(prefix)s_parser_context_unref (%(Prefix)sParserContext *self);
const gchar *%(prefix)s_parser_context_intern_string (%(Prefix)sParserContext *self, const gchar *string);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (%(Prefix)sParserContext, %(prefix)s_parser_context_unref)
''' % self.state())

        stream.write('''
G_END_DECLS

#endif /* %(PREFIX)s_PARSER_TYPES_H */
''' % self.state())

    def writeParserObject(self):
        filename = self.type_name_dashed('parser-object') + '.c'
        stream = self.openFile(filename)
        stream.write('''\
#define G_LOG_DOMAIN "%(prefix_dashed)s-parser-object"

#include <gio/gio.h>

#include "%(prefix_dashed)s-parser-types.h"

typedef struct
{
  %(Prefix)sParserObject  *parent;
  %(Prefix)sParserContext *parser_context;
} %(Prefix)sParserObjectPrivate;

struct _%(Prefix)sParserContext
{
  volatile gint ref_count;
  GStringChunk *strings;
};

enum {
  PROP_0,
  PROP_PARSER_CONTEXT,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static guint
get_n_items (GListModel *model)
{
  %(Prefix)sParserObject *self = (%(Prefix)sParserObject *)model;
  GPtrArray *children = %(prefix)s_parser_object_get_children (self);
  return children ? children->len : 0;
}

static GType
get_item_type (GListModel *model)
{
  return %(PREFIX)s_TYPE_PARSER_OBJECT;
}

static gpointer
get_item (GListModel *model,
          guint       position)
{
  %(Prefix)sParserObject *self = (%(Prefix)sParserObject *)model;
  GPtrArray *children = %(prefix)s_parser_object_get_children (self);
  if (children && position < children->len)
    return g_object_ref (g_ptr_array_index (children, position));
  return NULL;
}

static void
list_model_iface_init (GListModelInterface *iface)
{
  iface->get_n_items = get_n_items;
  iface->get_item_type = get_item_type;
  iface->get_item = get_item;
}

G_DEFINE_TYPE_WITH_CODE (%(Prefix)sParserObject, %(prefix)s_parser_object, G_TYPE_OBJECT,
                         G_ADD_PRIVATE (%(Prefix)sParserObject)
                         G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL, list_model_iface_init))
G_DEFINE_BOXED_TYPE (%(Prefix)sParserContext, %(prefix)s_parser_context, %(prefix)s_parser_context_ref, %(prefix)s_parser_context_unref)

%(Prefix)sParserContext *
%(prefix)s_parser_context_new (void)
{
  %(Prefix)sParserContext *ret;

  ret = g_slice_new0 (%(Prefix)sParserContext);
  ret->ref_count = 1;
  ret->strings = g_string_chunk_new (4096);

  return ret;
}

%(Prefix)sParserContext *
%(prefix)s_parser_context_ref (%(Prefix)sParserContext *self)
{
  g_return_val_if_fail (self != NULL, NULL);
  g_return_val_if_fail (self->ref_count > 0, NULL);

  g_atomic_int_inc (&self->ref_count);

  return self;
}

void
%(prefix)s_parser_context_unref (%(Prefix)sParserContext *self)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (self->ref_count > 0);

  if (g_atomic_int_dec_and_test (&self->ref_count))
    {
      g_string_chunk_free (self->strings);
      g_slice_free (%(Prefix)sParserContext, self);
    }
}

const gchar *
%(prefix)s_parser_context_intern_string (%(Prefix)sParserContext *self,
%(align)s                               const gchar *string)
{
  g_return_val_if_fail (self != NULL, NULL);

  /* treat empty as null to simplify code */
  if (string == NULL || *string == '\\0')
    return NULL;
  return g_string_chunk_insert_const (self->strings, string);
}

/**
 * %(prefix)s_parser_object_get_parser_context:
 * @self: A #%(Prefix)sParserObject
 *
 * Gets the parser context associated with this object.
 *
 * Returns: (transfer none): A #%(Prefix)sParserContext
 */
%(Prefix)sParserContext *
%(prefix)s_parser_object_get_parser_context (%(Prefix)sParserObject *self)
{
  %(Prefix)sParserObjectPrivate *priv = %(prefix)s_parser_object_get_instance_private (self);

  g_return_val_if_fail (%(PREFIX)s_IS_PARSER_OBJECT (self), NULL);

  return priv->parser_context;
}

void
_%(prefix)s_parser_object_set_parent (%(Prefix)sParserObject *self,
 %(align)s                           %(Prefix)sParserObject *parent)
{
  %(Prefix)sParserObjectPrivate *priv = %(prefix)s_parser_object_get_instance_private (self);

  g_return_if_fail (%(PREFIX)s_IS_PARSER_OBJECT (self));
  g_return_if_fail (!parent || %(PREFIX)s_IS_PARSER_OBJECT (parent));

  priv->parent = parent;
}

/**
 * %(prefix)s_parser_object_get_parent:
 *
 * Gets the parent #%(Prefix)sParserObject that was parsed
 * from the underlying document.
 *
 * Returns: (transfer none) (nullable): A #%(Prefix)sParserObject or %%NULL
 *   if the object is the root of the tree.
 */
%(Prefix)sParserObject *
%(prefix)s_parser_object_get_parent (%(Prefix)sParserObject *self)
{
  %(Prefix)sParserObjectPrivate *priv = %(prefix)s_parser_object_get_instance_private (self);

  g_return_val_if_fail (%(PREFIX)s_IS_PARSER_OBJECT (self), NULL);

  return priv->parent;
}

static void
%(prefix)s_parser_object_finalize (GObject *object)
{
  %(Prefix)sParserObject *self = (%(Prefix)sParserObject *)object;
  %(Prefix)sParserObjectPrivate *priv = %(prefix)s_parser_object_get_instance_private (self);

  g_clear_pointer (&priv->parser_context, %(prefix)s_parser_context_unref);

  G_OBJECT_CLASS (%(prefix)s_parser_object_parent_class)->finalize (object);
}

static void
%(prefix)s_parser_object_get_property (GObject    *object,
%(align)s                             guint       prop_id,
%(align)s                             GValue     *value,
%(align)s                             GParamSpec *pspec)
{
  %(Prefix)sParserObject *self = (%(Prefix)sParserObject *)object;

  switch (prop_id)
    {
    case PROP_PARSER_CONTEXT:
      g_value_set_boxed (value, %(prefix)s_parser_object_get_parser_context (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
%(prefix)s_parser_object_set_property (GObject      *object,
%(align)s                             guint         prop_id,
%(align)s                             const GValue *value,
%(align)s                             GParamSpec   *pspec)
{
  %(Prefix)sParserObject *self = (%(Prefix)sParserObject *)object;
  %(Prefix)sParserObjectPrivate *priv = %(prefix)s_parser_object_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_PARSER_CONTEXT:
      priv->parser_context = g_value_dup_boxed (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
%(prefix)s_parser_object_class_init (%(Prefix)sParserObjectClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = %(prefix)s_parser_object_finalize;
  object_class->get_property = %(prefix)s_parser_object_get_property;
  object_class->set_property = %(prefix)s_parser_object_set_property;

  properties [PROP_PARSER_CONTEXT] =
    g_param_spec_boxed ("parser-context",
                        "Parser Context",
                        "The parser context used for shared allocations",
                        %(PREFIX)s_TYPE_PARSER_CONTEXT,
                        (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
%(prefix)s_parser_object_init (%(Prefix)sParserObject *self)
{
}

/**
 * %(prefix)s_parser_object_first_typed:
 *
 * Gets the first child matching @child_type.
 *
 * Returns: (nullable) (transfer none): A #%(Prefix)sParserObject or %%NULL
 */
%(Prefix)sParserObject *
%(prefix)s_parser_object_first_typed (%(Prefix)sParserObject *self, GType child_type)
{
  GPtrArray *ar = %(prefix)s_parser_object_get_children (self);

  if (ar != NULL)
    {
      for (guint i = 0; i < ar->len; i++)
        {
          %(Prefix)sParserObject *obj = g_ptr_array_index (ar, i);
          if (g_type_is_a (G_OBJECT_TYPE (obj), child_type))
            return obj;
        }
    }

  return NULL;
}

/**
 * %(prefix)s_parser_object_get_children:
 * @self: An #%(Prefix)sParserObject
 *
 * Gets all of the children of @self in the order they were parsed.
 *
 * Returns: (nullable) (transfer none) (element-type %(Prefix)s.ParserObject):
 *   An array of %(Prefix)sParserObject or %%NULL.
 */
GPtrArray *
%(prefix)s_parser_object_get_children (%(Prefix)sParserObject *self)
{
  g_return_val_if_fail (%(PREFIX)s_IS_PARSER_OBJECT (self), NULL);

  if (%(PREFIX)s_PARSER_OBJECT_GET_CLASS (self)->get_children)
    return %(PREFIX)s_PARSER_OBJECT_GET_CLASS (self)->get_children (self);

  return NULL;
}

/**
 * %(prefix)s_parser_object_get_children_typed:
 * @self: An #%(Prefix)sParserObject
 * @type: A #GType
 *
 * Gets all children of @self which are of type @type.
 *
 * Returns: (transfer full): An #GListStore of children matching @type.
 */
GListModel *
%(prefix)s_parser_object_get_children_typed (%(Prefix)sParserObject *self,
%(align)s                                   GType type)
{
  GListStore *ret;
  GPtrArray *ar;

  g_return_val_if_fail (%(PREFIX)s_IS_PARSER_OBJECT (self), NULL);

  ret = g_list_store_new (type);

  ar = %(prefix)s_parser_object_get_children (self);

  if (ar != NULL)
    {
      guint i;

      for (i = 0; i < ar->len; i++)
        {
          %(Prefix)sParserObject *item = g_ptr_array_index (ar, i);

          if (g_type_is_a (G_OBJECT_TYPE (item), type))
            g_list_store_append (ret, item);
        }
    }

  return G_LIST_MODEL (ret);
}

gboolean
%(prefix)s_parser_object_has_child_typed (%(Prefix)sParserObject *self,
%(align)s                                GType type)
{
  GPtrArray *ar;
  guint i;

  g_return_val_if_fail (%(PREFIX)s_IS_PARSER_OBJECT (self), FALSE);

  ar = %(prefix)s_parser_object_get_children (self);

  if (ar != NULL)
    {
      for (i = 0; i < ar->len; i++)
        {
          GObject *object = g_ptr_array_index (ar, i);

          if (g_type_is_a (G_TYPE_FROM_INSTANCE (object), type))
            return TRUE;
        }
    }

  return FALSE;
}

gboolean
%(prefix)s_parser_object_ingest (%(Prefix)sParserObject *self,
%(align)s                       GMarkupParseContext *context,
%(align)s                       const gchar *element_name,
%(align)s                       const gchar **attribute_names,
%(align)s                       const gchar **attribute_values,
%(align)s                       GError **error)
{
  g_return_val_if_fail (%(PREFIX)s_IS_PARSER_OBJECT (self), FALSE);
  g_return_val_if_fail (context != NULL, FALSE);
  g_return_val_if_fail (element_name != NULL, FALSE);
  g_return_val_if_fail (attribute_names != NULL, FALSE);
  g_return_val_if_fail (attribute_values != NULL, FALSE);

  if (%(PREFIX)s_PARSER_OBJECT_GET_CLASS (self)->ingest)
    return %(PREFIX)s_PARSER_OBJECT_GET_CLASS (self)->ingest (self, context, element_name, attribute_names, attribute_values, error);

  return TRUE;
}

void
%(prefix)s_parser_object_printf (%(Prefix)sParserObject *self,
%(align)s                       GString *str,
%(align)s                       guint depth)
{
  g_return_if_fail (%(PREFIX)s_IS_PARSER_OBJECT (self));
  g_return_if_fail (str != NULL);

  if (%(PREFIX)s_PARSER_OBJECT_GET_CLASS (self)->printf)
    %(PREFIX)s_PARSER_OBJECT_GET_CLASS (self)->printf (self, str, depth);
}
''' % self.state())

    def writeParser(self):
        filename = self.type_name_dashed('parser') + '.h'
        header = self.openFile(filename)
        header.write('''\
#ifndef %(PREFIX)s_PARSER_H
#define %(PREFIX)s_PARSER_H

#include <gio/gio.h>

#include "%(prefix_dashed)s-parser-types.h"
#include "%(start_type_dashed)s.h"

G_BEGIN_DECLS

#define %(PREFIX)s_TYPE_PARSER (%(prefix)s_parser_get_type())

G_DECLARE_FINAL_TYPE (%(Prefix)sParser, %(prefix)s_parser, %(PREFIX)s, PARSER, GObject)

void %(prefix)s_parser_ensure_types (void);
%(Prefix)sParser *%(prefix)s_parser_new (void);
%(StartType)s *%(prefix)s_parser_parse_file (%(Prefix)sParser *self,
               %(align)s                    GFile *file,
               %(align)s                    GCancellable *cancellable,
               %(align)s                    GError **error);

G_END_DECLS

#endif /* %(PREFIX)s_PARSER_H */
''' % self.state())

        filename = self.type_name_dashed('parser') + '.c'
        source = self.openFile(filename)
        source.write('''\
#define G_LOG_DOMAIN "%(prefix_dashed)s-parser"

#include "%(prefix_dashed)s-parser.h"

struct _%(Prefix)sParser
{
  GObject parent_instance;
};

G_DEFINE_TYPE (%(Prefix)sParser, %(prefix)s_parser, G_TYPE_OBJECT)

static void
%(prefix)s_parser_class_init (%(Prefix)sParserClass *klass)
{
}

static void
%(prefix)s_parser_init (%(Prefix)sParser *self)
{
}

static void
%(prefix)s_start_element (GMarkupParseContext *context,
%(align)s                const gchar *element_name,
%(align)s                const gchar **attribute_names,
%(align)s                const gchar **attribute_values,
%(align)s                gpointer user_data,
%(align)s                GError **error)
{
  %(StartType)s **result = user_data;

  g_assert (context != NULL);
  g_assert (element_name != NULL);
  g_assert (attribute_names != NULL);
  g_assert (attribute_values != NULL);
  g_assert (result != NULL);

  if (g_str_equal (element_name, "%(start_element)s"))
    {
      g_autoptr(%(StartType)s) child = NULL;
      g_autoptr(%(Prefix)sParserContext) parser_context = NULL;

      parser_context = %(prefix)s_parser_context_new ();
      child = %(start_type)s_new (parser_context);

      if (%(prefix)s_parser_object_ingest (%(PREFIX)s_PARSER_OBJECT (child),
          %(align)s                       context,
          %(align)s                       element_name,
          %(align)s                       attribute_names,
          %(align)s                       attribute_values,
          %(align)s                       error))
        {
          g_clear_object (result);
          *result = g_steal_pointer (&child);
        }
    }
}

static void
%(prefix)s_end_element (GMarkupParseContext *context,
%(align)s              const gchar *element_name,
%(align)s              gpointer user_data,
%(align)s              GError **error)
{
  g_assert (context != NULL);
  g_assert (element_name != NULL);

  if (g_str_equal (element_name, "%(start_element)s"))
    {
      g_markup_parse_context_pop (context);
    }
}

static const GMarkupParser markup_parser = {
  %(prefix)s_start_element,
  %(prefix)s_end_element,
  NULL,
  NULL,
  NULL,
};

/**
 * %(prefix)s_parser_parse_file:
 * @self: A #%(Prefix)sParser
 * @file: A #GFile
 * @cancellable: (nullable): A #GCancellable or %%NULL.
 * @error: A location for a #GError or %%NULL.
 *
 * Parses the contents of @file and returns the toplevel object.
 *
 * Returns: (transfer full): An #%(StartType)s or %%NULL upon failure.
 */
%(StartType)s *
%(prefix)s_parser_parse_file (%(Prefix)sParser *self,
%(align)s                    GFile *file,
%(align)s                    GCancellable *cancellable,
%(align)s                    GError **error)
{
  g_autoptr(GMarkupParseContext) context = NULL;
  g_autoptr(%(StartType)s) ret = NULL;
  g_autofree gchar *content = NULL;
  gsize content_len = 0;

  g_return_val_if_fail (%(PREFIX)s_IS_PARSER (self), NULL);
  g_return_val_if_fail (G_IS_FILE (file), NULL);
  g_return_val_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable), NULL);

  if (!g_file_load_contents (file, cancellable, &content, &content_len, NULL, error))
    return NULL;

  context = g_markup_parse_context_new (&markup_parser, 0, &ret, NULL);

  if (!g_markup_parse_context_parse (context, content, content_len, error))
    return NULL;

  if (!g_markup_parse_context_end_parse (context, error))
    return NULL;

  if (ret == NULL)
    {
      g_set_error (error,
                   G_MARKUP_ERROR,
                   G_MARKUP_ERROR_INVALID_CONTENT,
                   "Failed to locate \\"%(start_element)s\\" element");
      return NULL;
    }

  return g_steal_pointer (&ret);
}

%(Prefix)sParser *
%(prefix)s_parser_new (void)
{
  return g_object_new (%(PREFIX)s_TYPE_PARSER, NULL);
}

static gboolean
find_pair (const gchar **attribute_names,
           const gchar **attribute_values,
           const gchar *name,
           const gchar **value)
{
  guint i;

  *value = NULL;

  g_assert (attribute_names != NULL);
  g_assert (attribute_values != NULL);
  g_assert (name != NULL);
  g_assert (value != NULL);

  for (i = 0; attribute_names[i]; i++)
    {
      if (g_strcmp0 (attribute_names[i], name) == 0)
        {
          *value = attribute_values[i];
          return TRUE;
        }
    }

  return FALSE;
}

gboolean
%(prefix)s_g_markup_collect_attributes (const gchar *element_name,
%(align)s                              const gchar **attribute_names,
%(align)s                              const gchar **attribute_values,
%(align)s                              GError **error,
%(align)s                              GMarkupCollectType first_type,
%(align)s                              const gchar *first_attr,
%(align)s                              ...)
{
  GMarkupCollectType type = first_type;
  const gchar *attr = first_attr;
  const gchar *value;
  gpointer *data;
  gboolean ret = FALSE;
  va_list args;

  g_return_val_if_fail (element_name != NULL, FALSE);
  g_return_val_if_fail (attribute_names != NULL, FALSE);
  g_return_val_if_fail (attribute_values != NULL, FALSE);

  if (first_type == G_MARKUP_COLLECT_INVALID)
    return TRUE;

  va_start (args, first_attr);

  for (;;)
    {
      data = va_arg (args, gpointer *);

      if (data != NULL)
        *data = NULL;

      if (find_pair (attribute_names, attribute_values, attr, &value))
        {
          const gchar *names[] = { attr, NULL };
          const gchar *values[] = { value, NULL };

          if (!g_markup_collect_attributes (element_name,
                                            names,
                                            values,
                                            error,
                                            type, attr, data,
                                            G_MARKUP_COLLECT_INVALID))
            goto failure;

        }
      else if (!(type & G_MARKUP_COLLECT_OPTIONAL))
        {
          g_set_error (error,
                       G_MARKUP_ERROR,
                       G_MARKUP_ERROR_MISSING_ATTRIBUTE,
                       "The attribute \\"%%s\\" of element \\"%%s\\" is missing",
                       attr, element_name);
          goto failure;
        }

      type = va_arg (args, GMarkupCollectType);
      if (type == G_MARKUP_COLLECT_INVALID)
        break;

      attr = va_arg (args, const gchar *);
    }

  ret = TRUE;

failure:
  va_end (args);

  return ret;
}
''' % self.state())

        source.write('''\
void
%(prefix)s_parser_ensure_types (void)
{
  g_type_ensure (%(PREFIX)s_TYPE_PARSER_OBJECT);
''' % self.state())

        for klass in self.classes.values():
            source.write('''\
  g_type_ensure (%(PREFIX)s_TYPE_%(NAME)s);
''' % klass.state())

        source.write('''\
}
''' % self.state())

    def parts(self, name):
        parts = []
        for ch in name:
            if ch.isupper():
                parts.append('')
            parts[-1] += ch
        return parts

    def field(self, name):
        return name.replace(':','_').replace('-','_').lower()

    def TypeName(self, name):
        return self.type_prefix + self.to_name(name)

    def to_name(self, name):
        name = name.replace(':','-')
        parts = name.split('-')
        parts = [p.capitalize() if p[0].islower() else p for p in parts]
        return ''.join(parts)

    def type_name(self, name):
        parts = self.parts(self.TypeName(name))
        return '_'.join([p.lower() for p in parts])

    def type_name_dashed(self, name):
        parts = self.parts(self.TypeName(name))
        return '-'.join([p.lower() for p in parts])

    def TYPE_NAME(self, name):
        return self.type_name(name).upper()

    def PREFIX(self):
        return self.symbol_prefix.upper()

    def NAME(self, name):
        return self.type_name(name)[len(self.symbol_prefix)+1:].upper()

if __name__ == '__main__':
    girfile, type_prefix, symbol_prefix = sys.argv[1:4]
    generator = Generator(girfile, type_prefix, symbol_prefix, ".")
    generator.process()
    generator.writeFiles()
    print('\n'.join(repr(c) for c in generator.classes.values()))
