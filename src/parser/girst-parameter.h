/* girst-parameter.h
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GIRST_PARAMETER_H
#define GIRST_PARAMETER_H

#include "girst-parser-types.h"

G_BEGIN_DECLS

GirstParameter *girst_parameter_new (GirstParserContext *parser_context);

const gchar *girst_parameter_get_name (GirstParameter *self);

const gchar *girst_parameter_get_nullable (GirstParameter *self);

const gchar *girst_parameter_get_allow_none (GirstParameter *self);

const gchar *girst_parameter_get_introspectable (GirstParameter *self);

const gchar *girst_parameter_get_closure (GirstParameter *self);

const gchar *girst_parameter_get_destroy (GirstParameter *self);

const gchar *girst_parameter_get_scope (GirstParameter *self);

const gchar *girst_parameter_get_direction (GirstParameter *self);

const gchar *girst_parameter_get_caller_allocates (GirstParameter *self);

const gchar *girst_parameter_get_optional (GirstParameter *self);

const gchar *girst_parameter_get_skip (GirstParameter *self);

const gchar *girst_parameter_get_transfer_ownership (GirstParameter *self);

G_END_DECLS

#endif /* GIRST_PARAMETER */
