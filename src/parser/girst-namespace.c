/* girst-namespace.c
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "girst-namespace"

#include "girst-namespace.h"

#include "girst-alias.h"
#include "girst-class.h"
#include "girst-interface.h"
#include "girst-record.h"
#include "girst-enumeration.h"
#include "girst-function.h"
#include "girst-union.h"
#include "girst-bitfield.h"
#include "girst-callback.h"
#include "girst-constant.h"
#include "girst-annotation.h"
#include "girst-glib-boxed.h"

struct _GirstNamespace
{
  GObject parent_instance;
  const gchar *name;
  const gchar *version;
  const gchar *c_identifier_prefixes;
  const gchar *c_symbol_prefixes;
  const gchar *c_prefix;
  const gchar *shared_library;
  GPtrArray *children;
};

G_DEFINE_TYPE (GirstNamespace, girst_namespace, GIRST_TYPE_PARSER_OBJECT)

enum {
  PROP_0,
  PROP_NAME,
  PROP_VERSION,
  PROP_C_IDENTIFIER_PREFIXES,
  PROP_C_SYMBOL_PREFIXES,
  PROP_C_PREFIX,
  PROP_SHARED_LIBRARY,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static GPtrArray *
girst_namespace_get_children (GirstParserObject *object)
{
  GirstNamespace *self = (GirstNamespace *)object;

  g_assert (GIRST_IS_NAMESPACE (self));

  return self->children;
}

static void
girst_namespace_start_element (GMarkupParseContext *context,
                               const gchar *element_name,
                               const gchar **attribute_names,
                               const gchar **attribute_values,
                               gpointer user_data,
                               GError **error)
{
  GirstNamespace *self = user_data;
  GirstParserContext *parser_context;

  g_assert (GIRST_IS_NAMESPACE (self));
  g_assert (context != NULL);
  g_assert (element_name != NULL);
  g_assert (attribute_names != NULL);
  g_assert (attribute_values != NULL);

  parser_context = girst_parser_object_get_parser_context (GIRST_PARSER_OBJECT (self));

  if (FALSE) {}
  else if (g_str_equal (element_name, "alias"))
    {
      g_autoptr(GirstAlias) child = NULL;

      child = girst_alias_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "class"))
    {
      g_autoptr(GirstClass) child = NULL;

      child = girst_class_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "interface"))
    {
      g_autoptr(GirstInterface) child = NULL;

      child = girst_interface_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "record"))
    {
      g_autoptr(GirstRecord) child = NULL;

      child = girst_record_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "enumeration"))
    {
      g_autoptr(GirstEnumeration) child = NULL;

      child = girst_enumeration_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "function"))
    {
      g_autoptr(GirstFunction) child = NULL;

      child = girst_function_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "union"))
    {
      g_autoptr(GirstUnion) child = NULL;

      child = girst_union_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "bitfield"))
    {
      g_autoptr(GirstBitfield) child = NULL;

      child = girst_bitfield_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "callback"))
    {
      g_autoptr(GirstCallback) child = NULL;

      child = girst_callback_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "constant"))
    {
      g_autoptr(GirstConstant) child = NULL;

      child = girst_constant_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "annotation"))
    {
      g_autoptr(GirstAnnotation) child = NULL;

      child = girst_annotation_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "glib:boxed"))
    {
      g_autoptr(GirstGlibBoxed) child = NULL;

      child = girst_glib_boxed_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
}

static void
girst_namespace_end_element (GMarkupParseContext *context,
                             const gchar *element_name,
                             gpointer user_data,
                             GError **error)
{
  g_assert (GIRST_IS_NAMESPACE (user_data));
  g_assert (context != NULL);
  g_assert (element_name != NULL);

  if (FALSE) {}
  else if (g_str_equal (element_name, "alias"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "class"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "interface"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "record"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "enumeration"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "function"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "union"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "bitfield"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "callback"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "constant"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "glib:boxed"))
    {
      g_markup_parse_context_pop (context);
    }
}

static const GMarkupParser markup_parser = {
  girst_namespace_start_element,
  girst_namespace_end_element,
  NULL,
  NULL,
  NULL,
};

static gboolean
girst_namespace_ingest (GirstParserObject *object,
                        GMarkupParseContext *context,
                        const gchar *element_name,
                        const gchar **attribute_names,
                        const gchar **attribute_values,
                        GError **error)
{
  GirstNamespace *self = (GirstNamespace *)object;
  GirstParserContext *parser_context;
  const gchar *name = NULL;
  const gchar *version = NULL;
  const gchar *c_identifier_prefixes = NULL;
  const gchar *c_symbol_prefixes = NULL;
  const gchar *c_prefix = NULL;
  const gchar *shared_library = NULL;

  g_assert (GIRST_IS_NAMESPACE (self));
  g_assert (g_str_equal (element_name, "namespace"));

  parser_context = girst_parser_object_get_parser_context (GIRST_PARSER_OBJECT (self));


  if (!girst_g_markup_collect_attributes (element_name, attribute_names, attribute_values, error,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "name", &name,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "version", &version,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "c:identifier-prefixes", &c_identifier_prefixes,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "c:symbol-prefixes", &c_symbol_prefixes,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "c:prefix", &c_prefix,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "shared-library", &shared_library,
                                          G_MARKUP_COLLECT_INVALID, NULL, NULL))
    return FALSE;

  self->name = girst_parser_context_intern_string (parser_context, name);
  self->version = girst_parser_context_intern_string (parser_context, version);
  self->c_identifier_prefixes = girst_parser_context_intern_string (parser_context, c_identifier_prefixes);
  self->c_symbol_prefixes = girst_parser_context_intern_string (parser_context, c_symbol_prefixes);
  self->c_prefix = girst_parser_context_intern_string (parser_context, c_prefix);
  self->shared_library = girst_parser_context_intern_string (parser_context, shared_library);

  g_markup_parse_context_push (context, &markup_parser, self);

  return TRUE;
}

static void
girst_namespace_printf (GirstParserObject *object,
                        GString *str,
                        guint depth)
{
  GirstNamespace *self = (GirstNamespace *)object;
  guint i;

  g_assert (GIRST_IS_NAMESPACE (self));

  for (i = 0; i < depth; i++)
    g_string_append (str, "  ");
  g_string_append (str, "<namespace");

  if (self->name != NULL)
    g_string_append_printf (str, " name=\"%s\"", self->name);
  if (self->version != NULL)
    g_string_append_printf (str, " version=\"%s\"", self->version);
  if (self->c_identifier_prefixes != NULL)
    g_string_append_printf (str, " c:identifier-prefixes=\"%s\"", self->c_identifier_prefixes);
  if (self->c_symbol_prefixes != NULL)
    g_string_append_printf (str, " c:symbol-prefixes=\"%s\"", self->c_symbol_prefixes);
  if (self->c_prefix != NULL)
    g_string_append_printf (str, " c:prefix=\"%s\"", self->c_prefix);
  if (self->shared_library != NULL)
    g_string_append_printf (str, " shared-library=\"%s\"", self->shared_library);

  if (self->children != NULL && self->children->len > 0)
    {
      g_string_append (str, ">\n");

      for (i = 0; i < self->children->len; i++)
        girst_parser_object_printf (g_ptr_array_index (self->children, i), str, depth + 1);

      for (i = 0; i < depth; i++)
        g_string_append (str, "  ");
      g_string_append (str, "</namespace>\n");
    }
  else
    {
      g_string_append (str, "/>\n");
    }
}

static void
girst_namespace_get_property (GObject    *object,
                              guint       prop_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
  GirstNamespace *self = (GirstNamespace *)object;

  switch (prop_id)
    {
    case PROP_NAME:
      g_value_set_string (value, self->name);
      break;

    case PROP_VERSION:
      g_value_set_string (value, self->version);
      break;

    case PROP_C_IDENTIFIER_PREFIXES:
      g_value_set_string (value, self->c_identifier_prefixes);
      break;

    case PROP_C_SYMBOL_PREFIXES:
      g_value_set_string (value, self->c_symbol_prefixes);
      break;

    case PROP_C_PREFIX:
      g_value_set_string (value, self->c_prefix);
      break;

    case PROP_SHARED_LIBRARY:
      g_value_set_string (value, self->shared_library);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
girst_namespace_set_property (GObject      *object,
                              guint         prop_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  GirstNamespace *self = (GirstNamespace *)object;
  GirstParserContext *context = girst_parser_object_get_parser_context (GIRST_PARSER_OBJECT (self));

  switch (prop_id)
    {
    case PROP_NAME:
      self->name = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_VERSION:
      self->version = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_C_IDENTIFIER_PREFIXES:
      self->c_identifier_prefixes = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_C_SYMBOL_PREFIXES:
      self->c_symbol_prefixes = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_C_PREFIX:
      self->c_prefix = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_SHARED_LIBRARY:
      self->shared_library = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
girst_namespace_finalize (GObject *object)
{
  GirstNamespace *self = (GirstNamespace *)object;

  g_clear_pointer (&self->children, g_ptr_array_unref);

  G_OBJECT_CLASS (girst_namespace_parent_class)->finalize (object);
}

static void
girst_namespace_class_init (GirstNamespaceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GirstParserObjectClass *parent_class = GIRST_PARSER_OBJECT_CLASS (klass);

  object_class->get_property = girst_namespace_get_property;
  object_class->set_property = girst_namespace_set_property;
  object_class->finalize = girst_namespace_finalize;

  parent_class->ingest = girst_namespace_ingest;
  parent_class->printf = girst_namespace_printf;
  parent_class->get_children = girst_namespace_get_children;

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "name",
                         "name",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_VERSION] =
    g_param_spec_string ("version",
                         "version",
                         "version",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_C_IDENTIFIER_PREFIXES] =
    g_param_spec_string ("c-identifier-prefixes",
                         "c-identifier-prefixes",
                         "c-identifier-prefixes",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_C_SYMBOL_PREFIXES] =
    g_param_spec_string ("c-symbol-prefixes",
                         "c-symbol-prefixes",
                         "c-symbol-prefixes",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_C_PREFIX] =
    g_param_spec_string ("c-prefix",
                         "c-prefix",
                         "c-prefix",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_SHARED_LIBRARY] =
    g_param_spec_string ("shared-library",
                         "shared-library",
                         "shared-library",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
girst_namespace_init (GirstNamespace *self)
{
  self->children = g_ptr_array_new_with_free_func (g_object_unref);
}

const gchar *
girst_namespace_get_name (GirstNamespace *self)
{
  g_return_val_if_fail (GIRST_IS_NAMESPACE (self), NULL);

  return self->name;
}

const gchar *
girst_namespace_get_version (GirstNamespace *self)
{
  g_return_val_if_fail (GIRST_IS_NAMESPACE (self), NULL);

  return self->version;
}

const gchar *
girst_namespace_get_c_identifier_prefixes (GirstNamespace *self)
{
  g_return_val_if_fail (GIRST_IS_NAMESPACE (self), NULL);

  return self->c_identifier_prefixes;
}

const gchar *
girst_namespace_get_c_symbol_prefixes (GirstNamespace *self)
{
  g_return_val_if_fail (GIRST_IS_NAMESPACE (self), NULL);

  return self->c_symbol_prefixes;
}

const gchar *
girst_namespace_get_c_prefix (GirstNamespace *self)
{
  g_return_val_if_fail (GIRST_IS_NAMESPACE (self), NULL);

  return self->c_prefix;
}

const gchar *
girst_namespace_get_shared_library (GirstNamespace *self)
{
  g_return_val_if_fail (GIRST_IS_NAMESPACE (self), NULL);

  return self->shared_library;
}

GirstNamespace *
girst_namespace_new (GirstParserContext *parser_context)
{
  return g_object_new (GIRST_TYPE_NAMESPACE,
                       "parser-context", parser_context,
                       NULL);
}
