/* girst-return-value.h
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GIRST_RETURN_VALUE_H
#define GIRST_RETURN_VALUE_H

#include "girst-parser-types.h"

G_BEGIN_DECLS

GirstReturnValue *girst_return_value_new (GirstParserContext *parser_context);

const gchar *girst_return_value_get_introspectable (GirstReturnValue *self);

const gchar *girst_return_value_get_nullable (GirstReturnValue *self);

const gchar *girst_return_value_get_closure (GirstReturnValue *self);

const gchar *girst_return_value_get_scope (GirstReturnValue *self);

const gchar *girst_return_value_get_destroy (GirstReturnValue *self);

const gchar *girst_return_value_get_skip (GirstReturnValue *self);

const gchar *girst_return_value_get_allow_none (GirstReturnValue *self);

const gchar *girst_return_value_get_transfer_ownership (GirstReturnValue *self);

G_END_DECLS

#endif /* GIRST_RETURN_VALUE */
