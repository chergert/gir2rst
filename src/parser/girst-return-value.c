/* girst-return-value.c
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "girst-return-value"

#include "girst-return-value.h"

#include "girst-doc-version.h"
#include "girst-doc-stability.h"
#include "girst-doc.h"
#include "girst-doc-deprecated.h"
#include "girst-type.h"
#include "girst-array.h"

struct _GirstReturnValue
{
  GObject parent_instance;
  const gchar *introspectable;
  const gchar *nullable;
  const gchar *closure;
  const gchar *scope;
  const gchar *destroy;
  const gchar *skip;
  const gchar *allow_none;
  const gchar *transfer_ownership;
  GPtrArray *children;
};

G_DEFINE_TYPE (GirstReturnValue, girst_return_value, GIRST_TYPE_PARSER_OBJECT)

enum {
  PROP_0,
  PROP_INTROSPECTABLE,
  PROP_NULLABLE,
  PROP_CLOSURE,
  PROP_SCOPE,
  PROP_DESTROY,
  PROP_SKIP,
  PROP_ALLOW_NONE,
  PROP_TRANSFER_OWNERSHIP,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static GPtrArray *
girst_return_value_get_children (GirstParserObject *object)
{
  GirstReturnValue *self = (GirstReturnValue *)object;

  g_assert (GIRST_IS_RETURN_VALUE (self));

  return self->children;
}

static void
girst_return_value_start_element (GMarkupParseContext *context,
                                  const gchar *element_name,
                                  const gchar **attribute_names,
                                  const gchar **attribute_values,
                                  gpointer user_data,
                                  GError **error)
{
  GirstReturnValue *self = user_data;
  GirstParserContext *parser_context;

  g_assert (GIRST_IS_RETURN_VALUE (self));
  g_assert (context != NULL);
  g_assert (element_name != NULL);
  g_assert (attribute_names != NULL);
  g_assert (attribute_values != NULL);

  parser_context = girst_parser_object_get_parser_context (GIRST_PARSER_OBJECT (self));

  if (FALSE) {}
  else if (g_str_equal (element_name, "doc-version"))
    {
      g_autoptr(GirstDocVersion) child = NULL;

      child = girst_doc_version_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "doc-stability"))
    {
      g_autoptr(GirstDocStability) child = NULL;

      child = girst_doc_stability_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "doc"))
    {
      g_autoptr(GirstDoc) child = NULL;

      child = girst_doc_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "doc-deprecated"))
    {
      g_autoptr(GirstDocDeprecated) child = NULL;

      child = girst_doc_deprecated_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "type"))
    {
      g_autoptr(GirstType) child = NULL;

      child = girst_type_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "array"))
    {
      g_autoptr(GirstArray) child = NULL;

      child = girst_array_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
}

static void
girst_return_value_end_element (GMarkupParseContext *context,
                                const gchar *element_name,
                                gpointer user_data,
                                GError **error)
{
  g_assert (GIRST_IS_RETURN_VALUE (user_data));
  g_assert (context != NULL);
  g_assert (element_name != NULL);

  if (FALSE) {}
  else if (g_str_equal (element_name, "doc-version"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "doc-stability"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "doc"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "doc-deprecated"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "type"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "array"))
    {
      g_markup_parse_context_pop (context);
    }
}

static const GMarkupParser markup_parser = {
  girst_return_value_start_element,
  girst_return_value_end_element,
  NULL,
  NULL,
  NULL,
};

static gboolean
girst_return_value_ingest (GirstParserObject *object,
                           GMarkupParseContext *context,
                           const gchar *element_name,
                           const gchar **attribute_names,
                           const gchar **attribute_values,
                           GError **error)
{
  GirstReturnValue *self = (GirstReturnValue *)object;
  GirstParserContext *parser_context;
  const gchar *introspectable = NULL;
  const gchar *nullable = NULL;
  const gchar *closure = NULL;
  const gchar *scope = NULL;
  const gchar *destroy = NULL;
  const gchar *skip = NULL;
  const gchar *allow_none = NULL;
  const gchar *transfer_ownership = NULL;

  g_assert (GIRST_IS_RETURN_VALUE (self));
  g_assert (g_str_equal (element_name, "return-value"));

  parser_context = girst_parser_object_get_parser_context (GIRST_PARSER_OBJECT (self));


  if (!girst_g_markup_collect_attributes (element_name, attribute_names, attribute_values, error,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "introspectable", &introspectable,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "nullable", &nullable,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "closure", &closure,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "scope", &scope,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "destroy", &destroy,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "skip", &skip,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "allow-none", &allow_none,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "transfer-ownership", &transfer_ownership,
                                          G_MARKUP_COLLECT_INVALID, NULL, NULL))
    return FALSE;

  self->introspectable = girst_parser_context_intern_string (parser_context, introspectable);
  self->nullable = girst_parser_context_intern_string (parser_context, nullable);
  self->closure = girst_parser_context_intern_string (parser_context, closure);
  self->scope = girst_parser_context_intern_string (parser_context, scope);
  self->destroy = girst_parser_context_intern_string (parser_context, destroy);
  self->skip = girst_parser_context_intern_string (parser_context, skip);
  self->allow_none = girst_parser_context_intern_string (parser_context, allow_none);
  self->transfer_ownership = girst_parser_context_intern_string (parser_context, transfer_ownership);

  g_markup_parse_context_push (context, &markup_parser, self);

  return TRUE;
}

static void
girst_return_value_printf (GirstParserObject *object,
                           GString *str,
                           guint depth)
{
  GirstReturnValue *self = (GirstReturnValue *)object;
  guint i;

  g_assert (GIRST_IS_RETURN_VALUE (self));

  for (i = 0; i < depth; i++)
    g_string_append (str, "  ");
  g_string_append (str, "<return-value");

  if (self->introspectable != NULL)
    g_string_append_printf (str, " introspectable=\"%s\"", self->introspectable);
  if (self->nullable != NULL)
    g_string_append_printf (str, " nullable=\"%s\"", self->nullable);
  if (self->closure != NULL)
    g_string_append_printf (str, " closure=\"%s\"", self->closure);
  if (self->scope != NULL)
    g_string_append_printf (str, " scope=\"%s\"", self->scope);
  if (self->destroy != NULL)
    g_string_append_printf (str, " destroy=\"%s\"", self->destroy);
  if (self->skip != NULL)
    g_string_append_printf (str, " skip=\"%s\"", self->skip);
  if (self->allow_none != NULL)
    g_string_append_printf (str, " allow-none=\"%s\"", self->allow_none);
  if (self->transfer_ownership != NULL)
    g_string_append_printf (str, " transfer-ownership=\"%s\"", self->transfer_ownership);

  if (self->children != NULL && self->children->len > 0)
    {
      g_string_append (str, ">\n");

      for (i = 0; i < self->children->len; i++)
        girst_parser_object_printf (g_ptr_array_index (self->children, i), str, depth + 1);

      for (i = 0; i < depth; i++)
        g_string_append (str, "  ");
      g_string_append (str, "</return-value>\n");
    }
  else
    {
      g_string_append (str, "/>\n");
    }
}

static void
girst_return_value_get_property (GObject    *object,
                                 guint       prop_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
  GirstReturnValue *self = (GirstReturnValue *)object;

  switch (prop_id)
    {
    case PROP_INTROSPECTABLE:
      g_value_set_string (value, self->introspectable);
      break;

    case PROP_NULLABLE:
      g_value_set_string (value, self->nullable);
      break;

    case PROP_CLOSURE:
      g_value_set_string (value, self->closure);
      break;

    case PROP_SCOPE:
      g_value_set_string (value, self->scope);
      break;

    case PROP_DESTROY:
      g_value_set_string (value, self->destroy);
      break;

    case PROP_SKIP:
      g_value_set_string (value, self->skip);
      break;

    case PROP_ALLOW_NONE:
      g_value_set_string (value, self->allow_none);
      break;

    case PROP_TRANSFER_OWNERSHIP:
      g_value_set_string (value, self->transfer_ownership);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
girst_return_value_set_property (GObject      *object,
                                 guint         prop_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
  GirstReturnValue *self = (GirstReturnValue *)object;
  GirstParserContext *context = girst_parser_object_get_parser_context (GIRST_PARSER_OBJECT (self));

  switch (prop_id)
    {
    case PROP_INTROSPECTABLE:
      self->introspectable = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_NULLABLE:
      self->nullable = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_CLOSURE:
      self->closure = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_SCOPE:
      self->scope = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_DESTROY:
      self->destroy = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_SKIP:
      self->skip = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_ALLOW_NONE:
      self->allow_none = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_TRANSFER_OWNERSHIP:
      self->transfer_ownership = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
girst_return_value_finalize (GObject *object)
{
  GirstReturnValue *self = (GirstReturnValue *)object;

  g_clear_pointer (&self->children, g_ptr_array_unref);

  G_OBJECT_CLASS (girst_return_value_parent_class)->finalize (object);
}

static void
girst_return_value_class_init (GirstReturnValueClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GirstParserObjectClass *parent_class = GIRST_PARSER_OBJECT_CLASS (klass);

  object_class->get_property = girst_return_value_get_property;
  object_class->set_property = girst_return_value_set_property;
  object_class->finalize = girst_return_value_finalize;

  parent_class->ingest = girst_return_value_ingest;
  parent_class->printf = girst_return_value_printf;
  parent_class->get_children = girst_return_value_get_children;

  properties [PROP_INTROSPECTABLE] =
    g_param_spec_string ("introspectable",
                         "introspectable",
                         "introspectable",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_NULLABLE] =
    g_param_spec_string ("nullable",
                         "nullable",
                         "nullable",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_CLOSURE] =
    g_param_spec_string ("closure",
                         "closure",
                         "closure",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_SCOPE] =
    g_param_spec_string ("scope",
                         "scope",
                         "scope",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_DESTROY] =
    g_param_spec_string ("destroy",
                         "destroy",
                         "destroy",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_SKIP] =
    g_param_spec_string ("skip",
                         "skip",
                         "skip",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_ALLOW_NONE] =
    g_param_spec_string ("allow-none",
                         "allow-none",
                         "allow-none",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_TRANSFER_OWNERSHIP] =
    g_param_spec_string ("transfer-ownership",
                         "transfer-ownership",
                         "transfer-ownership",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
girst_return_value_init (GirstReturnValue *self)
{
  self->children = g_ptr_array_new_with_free_func (g_object_unref);
}

const gchar *
girst_return_value_get_introspectable (GirstReturnValue *self)
{
  g_return_val_if_fail (GIRST_IS_RETURN_VALUE (self), NULL);

  return self->introspectable;
}

const gchar *
girst_return_value_get_nullable (GirstReturnValue *self)
{
  g_return_val_if_fail (GIRST_IS_RETURN_VALUE (self), NULL);

  return self->nullable;
}

const gchar *
girst_return_value_get_closure (GirstReturnValue *self)
{
  g_return_val_if_fail (GIRST_IS_RETURN_VALUE (self), NULL);

  return self->closure;
}

const gchar *
girst_return_value_get_scope (GirstReturnValue *self)
{
  g_return_val_if_fail (GIRST_IS_RETURN_VALUE (self), NULL);

  return self->scope;
}

const gchar *
girst_return_value_get_destroy (GirstReturnValue *self)
{
  g_return_val_if_fail (GIRST_IS_RETURN_VALUE (self), NULL);

  return self->destroy;
}

const gchar *
girst_return_value_get_skip (GirstReturnValue *self)
{
  g_return_val_if_fail (GIRST_IS_RETURN_VALUE (self), NULL);

  return self->skip;
}

const gchar *
girst_return_value_get_allow_none (GirstReturnValue *self)
{
  g_return_val_if_fail (GIRST_IS_RETURN_VALUE (self), NULL);

  return self->allow_none;
}

const gchar *
girst_return_value_get_transfer_ownership (GirstReturnValue *self)
{
  g_return_val_if_fail (GIRST_IS_RETURN_VALUE (self), NULL);

  return self->transfer_ownership;
}

GirstReturnValue *
girst_return_value_new (GirstParserContext *parser_context)
{
  return g_object_new (GIRST_TYPE_RETURN_VALUE,
                       "parser-context", parser_context,
                       NULL);
}
