/* girst-glib-signal.h
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GIRST_GLIB_SIGNAL_H
#define GIRST_GLIB_SIGNAL_H

#include "girst-parser-types.h"

G_BEGIN_DECLS

GirstGlibSignal *girst_glib_signal_new (GirstParserContext *parser_context);

const gchar *girst_glib_signal_get_introspectable (GirstGlibSignal *self);

const gchar *girst_glib_signal_get_deprecated (GirstGlibSignal *self);

const gchar *girst_glib_signal_get_deprecated_version (GirstGlibSignal *self);

const gchar *girst_glib_signal_get_version (GirstGlibSignal *self);

const gchar *girst_glib_signal_get_stability (GirstGlibSignal *self);

const gchar *girst_glib_signal_get_name (GirstGlibSignal *self);

const gchar *girst_glib_signal_get_detailed (GirstGlibSignal *self);

const gchar *girst_glib_signal_get_when (GirstGlibSignal *self);

const gchar *girst_glib_signal_get_action (GirstGlibSignal *self);

const gchar *girst_glib_signal_get_no_hooks (GirstGlibSignal *self);

const gchar *girst_glib_signal_get_no_recurse (GirstGlibSignal *self);

G_END_DECLS

#endif /* GIRST_GLIB_SIGNAL */
