/* girst-field.h
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GIRST_FIELD_H
#define GIRST_FIELD_H

#include "girst-parser-types.h"

G_BEGIN_DECLS

GirstField *girst_field_new (GirstParserContext *parser_context);

const gchar *girst_field_get_introspectable (GirstField *self);

const gchar *girst_field_get_deprecated (GirstField *self);

const gchar *girst_field_get_deprecated_version (GirstField *self);

const gchar *girst_field_get_version (GirstField *self);

const gchar *girst_field_get_stability (GirstField *self);

const gchar *girst_field_get_name (GirstField *self);

const gchar *girst_field_get_writable (GirstField *self);

const gchar *girst_field_get_readable (GirstField *self);

const gchar *girst_field_get_private (GirstField *self);

const gchar *girst_field_get_bits (GirstField *self);

G_END_DECLS

#endif /* GIRST_FIELD */
