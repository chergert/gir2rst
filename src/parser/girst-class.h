/* girst-class.h
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GIRST_CLASS_H
#define GIRST_CLASS_H

#include "girst-parser-types.h"

G_BEGIN_DECLS

GirstClass *girst_class_new (GirstParserContext *parser_context);

const gchar *girst_class_get_introspectable (GirstClass *self);

const gchar *girst_class_get_deprecated (GirstClass *self);

const gchar *girst_class_get_deprecated_version (GirstClass *self);

const gchar *girst_class_get_version (GirstClass *self);

const gchar *girst_class_get_stability (GirstClass *self);

const gchar *girst_class_get_name (GirstClass *self);

const gchar *girst_class_get_glib_type_name (GirstClass *self);

const gchar *girst_class_get_glib_get_type (GirstClass *self);

const gchar *girst_class_get_parent (GirstClass *self);

const gchar *girst_class_get_glib_type_struct (GirstClass *self);

const gchar *girst_class_get_glib_ref_func (GirstClass *self);

const gchar *girst_class_get_glib_unref_func (GirstClass *self);

const gchar *girst_class_get_glib_set_value_func (GirstClass *self);

const gchar *girst_class_get_glib_get_value_func (GirstClass *self);

const gchar *girst_class_get_c_type (GirstClass *self);

const gchar *girst_class_get_c_symbol_prefix (GirstClass *self);

const gchar *girst_class_get_abstract (GirstClass *self);

const gchar *girst_class_get_glib_fundamental (GirstClass *self);

G_END_DECLS

#endif /* GIRST_CLASS */
