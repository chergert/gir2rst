/* girst-function.h
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GIRST_FUNCTION_H
#define GIRST_FUNCTION_H

#include "girst-parser-types.h"

G_BEGIN_DECLS

GirstFunction *girst_function_new (GirstParserContext *parser_context);

const gchar *girst_function_get_introspectable (GirstFunction *self);

const gchar *girst_function_get_deprecated (GirstFunction *self);

const gchar *girst_function_get_deprecated_version (GirstFunction *self);

const gchar *girst_function_get_version (GirstFunction *self);

const gchar *girst_function_get_stability (GirstFunction *self);

const gchar *girst_function_get_name (GirstFunction *self);

const gchar *girst_function_get_c_identifier (GirstFunction *self);

const gchar *girst_function_get_shadowed_by (GirstFunction *self);

const gchar *girst_function_get_shadows (GirstFunction *self);

const gchar *girst_function_get_throws (GirstFunction *self);

const gchar *girst_function_get_moved_to (GirstFunction *self);

G_END_DECLS

#endif /* GIRST_FUNCTION */
