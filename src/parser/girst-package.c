/* girst-package.c
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "girst-package"

#include "girst-package.h"

struct _GirstPackage
{
  GObject parent_instance;
  const gchar *name;
};

G_DEFINE_TYPE (GirstPackage, girst_package, GIRST_TYPE_PARSER_OBJECT)

enum {
  PROP_0,
  PROP_NAME,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static gboolean
girst_package_ingest (GirstParserObject *object,
                      GMarkupParseContext *context,
                      const gchar *element_name,
                      const gchar **attribute_names,
                      const gchar **attribute_values,
                      GError **error)
{
  GirstPackage *self = (GirstPackage *)object;
  GirstParserContext *parser_context;
  const gchar *name = NULL;

  g_assert (GIRST_IS_PACKAGE (self));
  g_assert (g_str_equal (element_name, "package"));

  parser_context = girst_parser_object_get_parser_context (GIRST_PARSER_OBJECT (self));


  if (!girst_g_markup_collect_attributes (element_name, attribute_names, attribute_values, error,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "name", &name,
                                          G_MARKUP_COLLECT_INVALID, NULL, NULL))
    return FALSE;

  self->name = girst_parser_context_intern_string (parser_context, name);

  return TRUE;
}

static void
girst_package_printf (GirstParserObject *object,
                      GString *str,
                      guint depth)
{
  GirstPackage *self = (GirstPackage *)object;
  guint i;

  g_assert (GIRST_IS_PACKAGE (self));

  for (i = 0; i < depth; i++)
    g_string_append (str, "  ");
  g_string_append (str, "<package");

  if (self->name != NULL)
    g_string_append_printf (str, " name=\"%s\"", self->name);

  g_string_append (str, "/>\n");
}

static void
girst_package_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  GirstPackage *self = (GirstPackage *)object;

  switch (prop_id)
    {
    case PROP_NAME:
      g_value_set_string (value, self->name);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
girst_package_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  GirstPackage *self = (GirstPackage *)object;
  GirstParserContext *context = girst_parser_object_get_parser_context (GIRST_PARSER_OBJECT (self));

  switch (prop_id)
    {
    case PROP_NAME:
      self->name = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
girst_package_finalize (GObject *object)
{

  G_OBJECT_CLASS (girst_package_parent_class)->finalize (object);
}

static void
girst_package_class_init (GirstPackageClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GirstParserObjectClass *parent_class = GIRST_PARSER_OBJECT_CLASS (klass);

  object_class->get_property = girst_package_get_property;
  object_class->set_property = girst_package_set_property;
  object_class->finalize = girst_package_finalize;

  parent_class->ingest = girst_package_ingest;
  parent_class->printf = girst_package_printf;

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "name",
                         "name",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
girst_package_init (GirstPackage *self)
{
}

const gchar *
girst_package_get_name (GirstPackage *self)
{
  g_return_val_if_fail (GIRST_IS_PACKAGE (self), NULL);

  return self->name;
}

GirstPackage *
girst_package_new (GirstParserContext *parser_context)
{
  return g_object_new (GIRST_TYPE_PACKAGE,
                       "parser-context", parser_context,
                       NULL);
}
