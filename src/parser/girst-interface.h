/* girst-interface.h
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GIRST_INTERFACE_H
#define GIRST_INTERFACE_H

#include "girst-parser-types.h"

G_BEGIN_DECLS

GirstInterface *girst_interface_new (GirstParserContext *parser_context);

const gchar *girst_interface_get_introspectable (GirstInterface *self);

const gchar *girst_interface_get_deprecated (GirstInterface *self);

const gchar *girst_interface_get_deprecated_version (GirstInterface *self);

const gchar *girst_interface_get_version (GirstInterface *self);

const gchar *girst_interface_get_stability (GirstInterface *self);

const gchar *girst_interface_get_name (GirstInterface *self);

const gchar *girst_interface_get_glib_type_name (GirstInterface *self);

const gchar *girst_interface_get_glib_get_type (GirstInterface *self);

const gchar *girst_interface_get_c_symbol_prefix (GirstInterface *self);

const gchar *girst_interface_get_c_type (GirstInterface *self);

const gchar *girst_interface_get_glib_type_struct (GirstInterface *self);

G_END_DECLS

#endif /* GIRST_INTERFACE */
