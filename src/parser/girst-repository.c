/* girst-repository.c
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "girst-repository"

#include "girst-repository.h"

#include "girst-include.h"
#include "girst-c-include.h"
#include "girst-package.h"
#include "girst-namespace.h"

struct _GirstRepository
{
  GObject parent_instance;
  const gchar *version;
  const gchar *c_identifier_prefixes;
  const gchar *c_symbol_prefixes;
  GPtrArray *children;
};

G_DEFINE_TYPE (GirstRepository, girst_repository, GIRST_TYPE_PARSER_OBJECT)

enum {
  PROP_0,
  PROP_VERSION,
  PROP_C_IDENTIFIER_PREFIXES,
  PROP_C_SYMBOL_PREFIXES,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static GPtrArray *
girst_repository_get_children (GirstParserObject *object)
{
  GirstRepository *self = (GirstRepository *)object;

  g_assert (GIRST_IS_REPOSITORY (self));

  return self->children;
}

static void
girst_repository_start_element (GMarkupParseContext *context,
                                const gchar *element_name,
                                const gchar **attribute_names,
                                const gchar **attribute_values,
                                gpointer user_data,
                                GError **error)
{
  GirstRepository *self = user_data;
  GirstParserContext *parser_context;

  g_assert (GIRST_IS_REPOSITORY (self));
  g_assert (context != NULL);
  g_assert (element_name != NULL);
  g_assert (attribute_names != NULL);
  g_assert (attribute_values != NULL);

  parser_context = girst_parser_object_get_parser_context (GIRST_PARSER_OBJECT (self));

  if (FALSE) {}
  else if (g_str_equal (element_name, "include"))
    {
      g_autoptr(GirstInclude) child = NULL;

      child = girst_include_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "c:include"))
    {
      g_autoptr(GirstCInclude) child = NULL;

      child = girst_c_include_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "package"))
    {
      g_autoptr(GirstPackage) child = NULL;

      child = girst_package_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "namespace"))
    {
      g_autoptr(GirstNamespace) child = NULL;

      child = girst_namespace_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
}

static void
girst_repository_end_element (GMarkupParseContext *context,
                              const gchar *element_name,
                              gpointer user_data,
                              GError **error)
{
  g_assert (GIRST_IS_REPOSITORY (user_data));
  g_assert (context != NULL);
  g_assert (element_name != NULL);

  if (FALSE) {}
  else if (g_str_equal (element_name, "namespace"))
    {
      g_markup_parse_context_pop (context);
    }
}

static const GMarkupParser markup_parser = {
  girst_repository_start_element,
  girst_repository_end_element,
  NULL,
  NULL,
  NULL,
};

static gboolean
girst_repository_ingest (GirstParserObject *object,
                         GMarkupParseContext *context,
                         const gchar *element_name,
                         const gchar **attribute_names,
                         const gchar **attribute_values,
                         GError **error)
{
  GirstRepository *self = (GirstRepository *)object;
  GirstParserContext *parser_context;
  const gchar *version = NULL;
  const gchar *c_identifier_prefixes = NULL;
  const gchar *c_symbol_prefixes = NULL;

  g_assert (GIRST_IS_REPOSITORY (self));
  g_assert (g_str_equal (element_name, "repository"));

  parser_context = girst_parser_object_get_parser_context (GIRST_PARSER_OBJECT (self));


  if (!girst_g_markup_collect_attributes (element_name, attribute_names, attribute_values, error,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "version", &version,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "c:identifier-prefixes", &c_identifier_prefixes,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "c:symbol-prefixes", &c_symbol_prefixes,
                                          G_MARKUP_COLLECT_INVALID, NULL, NULL))
    return FALSE;

  self->version = girst_parser_context_intern_string (parser_context, version);
  self->c_identifier_prefixes = girst_parser_context_intern_string (parser_context, c_identifier_prefixes);
  self->c_symbol_prefixes = girst_parser_context_intern_string (parser_context, c_symbol_prefixes);

  g_markup_parse_context_push (context, &markup_parser, self);

  return TRUE;
}

static void
girst_repository_printf (GirstParserObject *object,
                         GString *str,
                         guint depth)
{
  GirstRepository *self = (GirstRepository *)object;
  guint i;

  g_assert (GIRST_IS_REPOSITORY (self));

  for (i = 0; i < depth; i++)
    g_string_append (str, "  ");
  g_string_append (str, "<repository");

  if (self->version != NULL)
    g_string_append_printf (str, " version=\"%s\"", self->version);
  if (self->c_identifier_prefixes != NULL)
    g_string_append_printf (str, " c:identifier-prefixes=\"%s\"", self->c_identifier_prefixes);
  if (self->c_symbol_prefixes != NULL)
    g_string_append_printf (str, " c:symbol-prefixes=\"%s\"", self->c_symbol_prefixes);

  if (self->children != NULL && self->children->len > 0)
    {
      g_string_append (str, ">\n");

      for (i = 0; i < self->children->len; i++)
        girst_parser_object_printf (g_ptr_array_index (self->children, i), str, depth + 1);

      for (i = 0; i < depth; i++)
        g_string_append (str, "  ");
      g_string_append (str, "</repository>\n");
    }
  else
    {
      g_string_append (str, "/>\n");
    }
}

static void
girst_repository_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  GirstRepository *self = (GirstRepository *)object;

  switch (prop_id)
    {
    case PROP_VERSION:
      g_value_set_string (value, self->version);
      break;

    case PROP_C_IDENTIFIER_PREFIXES:
      g_value_set_string (value, self->c_identifier_prefixes);
      break;

    case PROP_C_SYMBOL_PREFIXES:
      g_value_set_string (value, self->c_symbol_prefixes);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
girst_repository_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  GirstRepository *self = (GirstRepository *)object;
  GirstParserContext *context = girst_parser_object_get_parser_context (GIRST_PARSER_OBJECT (self));

  switch (prop_id)
    {
    case PROP_VERSION:
      self->version = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_C_IDENTIFIER_PREFIXES:
      self->c_identifier_prefixes = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_C_SYMBOL_PREFIXES:
      self->c_symbol_prefixes = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
girst_repository_finalize (GObject *object)
{
  GirstRepository *self = (GirstRepository *)object;

  g_clear_pointer (&self->children, g_ptr_array_unref);

  G_OBJECT_CLASS (girst_repository_parent_class)->finalize (object);
}

static void
girst_repository_class_init (GirstRepositoryClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GirstParserObjectClass *parent_class = GIRST_PARSER_OBJECT_CLASS (klass);

  object_class->get_property = girst_repository_get_property;
  object_class->set_property = girst_repository_set_property;
  object_class->finalize = girst_repository_finalize;

  parent_class->ingest = girst_repository_ingest;
  parent_class->printf = girst_repository_printf;
  parent_class->get_children = girst_repository_get_children;

  properties [PROP_VERSION] =
    g_param_spec_string ("version",
                         "version",
                         "version",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_C_IDENTIFIER_PREFIXES] =
    g_param_spec_string ("c-identifier-prefixes",
                         "c-identifier-prefixes",
                         "c-identifier-prefixes",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_C_SYMBOL_PREFIXES] =
    g_param_spec_string ("c-symbol-prefixes",
                         "c-symbol-prefixes",
                         "c-symbol-prefixes",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
girst_repository_init (GirstRepository *self)
{
  self->children = g_ptr_array_new_with_free_func (g_object_unref);
}

const gchar *
girst_repository_get_version (GirstRepository *self)
{
  g_return_val_if_fail (GIRST_IS_REPOSITORY (self), NULL);

  return self->version;
}

const gchar *
girst_repository_get_c_identifier_prefixes (GirstRepository *self)
{
  g_return_val_if_fail (GIRST_IS_REPOSITORY (self), NULL);

  return self->c_identifier_prefixes;
}

const gchar *
girst_repository_get_c_symbol_prefixes (GirstRepository *self)
{
  g_return_val_if_fail (GIRST_IS_REPOSITORY (self), NULL);

  return self->c_symbol_prefixes;
}

GirstRepository *
girst_repository_new (GirstParserContext *parser_context)
{
  return g_object_new (GIRST_TYPE_REPOSITORY,
                       "parser-context", parser_context,
                       NULL);
}
