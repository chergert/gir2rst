/* girst-class.c
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "girst-class"

#include "girst-class.h"

#include "girst-doc-version.h"
#include "girst-doc-stability.h"
#include "girst-doc.h"
#include "girst-doc-deprecated.h"
#include "girst-annotation.h"
#include "girst-implements.h"
#include "girst-constructor.h"
#include "girst-method.h"
#include "girst-function.h"
#include "girst-virtual-method.h"
#include "girst-field.h"
#include "girst-property.h"
#include "girst-glib-signal.h"
#include "girst-union.h"
#include "girst-constant.h"
#include "girst-record.h"
#include "girst-callback.h"

struct _GirstClass
{
  GObject parent_instance;
  const gchar *introspectable;
  const gchar *deprecated;
  const gchar *deprecated_version;
  const gchar *version;
  const gchar *stability;
  const gchar *name;
  const gchar *glib_type_name;
  const gchar *glib_get_type;
  const gchar *parent;
  const gchar *glib_type_struct;
  const gchar *glib_ref_func;
  const gchar *glib_unref_func;
  const gchar *glib_set_value_func;
  const gchar *glib_get_value_func;
  const gchar *c_type;
  const gchar *c_symbol_prefix;
  const gchar *abstract;
  const gchar *glib_fundamental;
  GPtrArray *children;
};

G_DEFINE_TYPE (GirstClass, girst_class, GIRST_TYPE_PARSER_OBJECT)

enum {
  PROP_0,
  PROP_INTROSPECTABLE,
  PROP_DEPRECATED,
  PROP_DEPRECATED_VERSION,
  PROP_VERSION,
  PROP_STABILITY,
  PROP_NAME,
  PROP_GLIB_TYPE_NAME,
  PROP_GLIB_GET_TYPE,
  PROP_PARENT,
  PROP_GLIB_TYPE_STRUCT,
  PROP_GLIB_REF_FUNC,
  PROP_GLIB_UNREF_FUNC,
  PROP_GLIB_SET_VALUE_FUNC,
  PROP_GLIB_GET_VALUE_FUNC,
  PROP_C_TYPE,
  PROP_C_SYMBOL_PREFIX,
  PROP_ABSTRACT,
  PROP_GLIB_FUNDAMENTAL,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static GPtrArray *
girst_class_get_children (GirstParserObject *object)
{
  GirstClass *self = (GirstClass *)object;

  g_assert (GIRST_IS_CLASS (self));

  return self->children;
}

static void
girst_class_start_element (GMarkupParseContext *context,
                           const gchar *element_name,
                           const gchar **attribute_names,
                           const gchar **attribute_values,
                           gpointer user_data,
                           GError **error)
{
  GirstClass *self = user_data;
  GirstParserContext *parser_context;

  g_assert (GIRST_IS_CLASS (self));
  g_assert (context != NULL);
  g_assert (element_name != NULL);
  g_assert (attribute_names != NULL);
  g_assert (attribute_values != NULL);

  parser_context = girst_parser_object_get_parser_context (GIRST_PARSER_OBJECT (self));

  if (FALSE) {}
  else if (g_str_equal (element_name, "doc-version"))
    {
      g_autoptr(GirstDocVersion) child = NULL;

      child = girst_doc_version_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "doc-stability"))
    {
      g_autoptr(GirstDocStability) child = NULL;

      child = girst_doc_stability_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "doc"))
    {
      g_autoptr(GirstDoc) child = NULL;

      child = girst_doc_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "doc-deprecated"))
    {
      g_autoptr(GirstDocDeprecated) child = NULL;

      child = girst_doc_deprecated_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "annotation"))
    {
      g_autoptr(GirstAnnotation) child = NULL;

      child = girst_annotation_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "implements"))
    {
      g_autoptr(GirstImplements) child = NULL;

      child = girst_implements_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "constructor"))
    {
      g_autoptr(GirstConstructor) child = NULL;

      child = girst_constructor_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "method"))
    {
      g_autoptr(GirstMethod) child = NULL;

      child = girst_method_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "function"))
    {
      g_autoptr(GirstFunction) child = NULL;

      child = girst_function_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "virtual-method"))
    {
      g_autoptr(GirstVirtualMethod) child = NULL;

      child = girst_virtual_method_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "field"))
    {
      g_autoptr(GirstField) child = NULL;

      child = girst_field_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "property"))
    {
      g_autoptr(GirstProperty) child = NULL;

      child = girst_property_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "glib:signal"))
    {
      g_autoptr(GirstGlibSignal) child = NULL;

      child = girst_glib_signal_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "union"))
    {
      g_autoptr(GirstUnion) child = NULL;

      child = girst_union_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "constant"))
    {
      g_autoptr(GirstConstant) child = NULL;

      child = girst_constant_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "record"))
    {
      g_autoptr(GirstRecord) child = NULL;

      child = girst_record_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "callback"))
    {
      g_autoptr(GirstCallback) child = NULL;

      child = girst_callback_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
}

static void
girst_class_end_element (GMarkupParseContext *context,
                         const gchar *element_name,
                         gpointer user_data,
                         GError **error)
{
  g_assert (GIRST_IS_CLASS (user_data));
  g_assert (context != NULL);
  g_assert (element_name != NULL);

  if (FALSE) {}
  else if (g_str_equal (element_name, "doc-version"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "doc-stability"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "doc"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "doc-deprecated"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "constructor"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "method"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "function"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "virtual-method"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "field"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "property"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "glib:signal"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "union"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "constant"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "record"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "callback"))
    {
      g_markup_parse_context_pop (context);
    }
}

static const GMarkupParser markup_parser = {
  girst_class_start_element,
  girst_class_end_element,
  NULL,
  NULL,
  NULL,
};

static gboolean
girst_class_ingest (GirstParserObject *object,
                    GMarkupParseContext *context,
                    const gchar *element_name,
                    const gchar **attribute_names,
                    const gchar **attribute_values,
                    GError **error)
{
  GirstClass *self = (GirstClass *)object;
  GirstParserContext *parser_context;
  const gchar *introspectable = NULL;
  const gchar *deprecated = NULL;
  const gchar *deprecated_version = NULL;
  const gchar *version = NULL;
  const gchar *stability = NULL;
  const gchar *name = NULL;
  const gchar *glib_type_name = NULL;
  const gchar *glib_get_type = NULL;
  const gchar *parent = NULL;
  const gchar *glib_type_struct = NULL;
  const gchar *glib_ref_func = NULL;
  const gchar *glib_unref_func = NULL;
  const gchar *glib_set_value_func = NULL;
  const gchar *glib_get_value_func = NULL;
  const gchar *c_type = NULL;
  const gchar *c_symbol_prefix = NULL;
  const gchar *abstract = NULL;
  const gchar *glib_fundamental = NULL;

  g_assert (GIRST_IS_CLASS (self));
  g_assert (g_str_equal (element_name, "class"));

  parser_context = girst_parser_object_get_parser_context (GIRST_PARSER_OBJECT (self));


  if (!girst_g_markup_collect_attributes (element_name, attribute_names, attribute_values, error,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "introspectable", &introspectable,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "deprecated", &deprecated,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "deprecated-version", &deprecated_version,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "version", &version,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "stability", &stability,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "name", &name,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "glib:type-name", &glib_type_name,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "glib:get-type", &glib_get_type,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "parent", &parent,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "glib:type-struct", &glib_type_struct,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "glib:ref-func", &glib_ref_func,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "glib:unref-func", &glib_unref_func,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "glib:set-value-func", &glib_set_value_func,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "glib:get-value-func", &glib_get_value_func,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "c:type", &c_type,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "c:symbol-prefix", &c_symbol_prefix,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "abstract", &abstract,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "glib:fundamental", &glib_fundamental,
                                          G_MARKUP_COLLECT_INVALID, NULL, NULL))
    return FALSE;

  self->introspectable = girst_parser_context_intern_string (parser_context, introspectable);
  self->deprecated = girst_parser_context_intern_string (parser_context, deprecated);
  self->deprecated_version = girst_parser_context_intern_string (parser_context, deprecated_version);
  self->version = girst_parser_context_intern_string (parser_context, version);
  self->stability = girst_parser_context_intern_string (parser_context, stability);
  self->name = girst_parser_context_intern_string (parser_context, name);
  self->glib_type_name = girst_parser_context_intern_string (parser_context, glib_type_name);
  self->glib_get_type = girst_parser_context_intern_string (parser_context, glib_get_type);
  self->parent = girst_parser_context_intern_string (parser_context, parent);
  self->glib_type_struct = girst_parser_context_intern_string (parser_context, glib_type_struct);
  self->glib_ref_func = girst_parser_context_intern_string (parser_context, glib_ref_func);
  self->glib_unref_func = girst_parser_context_intern_string (parser_context, glib_unref_func);
  self->glib_set_value_func = girst_parser_context_intern_string (parser_context, glib_set_value_func);
  self->glib_get_value_func = girst_parser_context_intern_string (parser_context, glib_get_value_func);
  self->c_type = girst_parser_context_intern_string (parser_context, c_type);
  self->c_symbol_prefix = girst_parser_context_intern_string (parser_context, c_symbol_prefix);
  self->abstract = girst_parser_context_intern_string (parser_context, abstract);
  self->glib_fundamental = girst_parser_context_intern_string (parser_context, glib_fundamental);

  g_markup_parse_context_push (context, &markup_parser, self);

  return TRUE;
}

static void
girst_class_printf (GirstParserObject *object,
                    GString *str,
                    guint depth)
{
  GirstClass *self = (GirstClass *)object;
  guint i;

  g_assert (GIRST_IS_CLASS (self));

  for (i = 0; i < depth; i++)
    g_string_append (str, "  ");
  g_string_append (str, "<class");

  if (self->introspectable != NULL)
    g_string_append_printf (str, " introspectable=\"%s\"", self->introspectable);
  if (self->deprecated != NULL)
    g_string_append_printf (str, " deprecated=\"%s\"", self->deprecated);
  if (self->deprecated_version != NULL)
    g_string_append_printf (str, " deprecated-version=\"%s\"", self->deprecated_version);
  if (self->version != NULL)
    g_string_append_printf (str, " version=\"%s\"", self->version);
  if (self->stability != NULL)
    g_string_append_printf (str, " stability=\"%s\"", self->stability);
  if (self->name != NULL)
    g_string_append_printf (str, " name=\"%s\"", self->name);
  if (self->glib_type_name != NULL)
    g_string_append_printf (str, " glib:type-name=\"%s\"", self->glib_type_name);
  if (self->glib_get_type != NULL)
    g_string_append_printf (str, " glib:get-type=\"%s\"", self->glib_get_type);
  if (self->parent != NULL)
    g_string_append_printf (str, " parent=\"%s\"", self->parent);
  if (self->glib_type_struct != NULL)
    g_string_append_printf (str, " glib:type-struct=\"%s\"", self->glib_type_struct);
  if (self->glib_ref_func != NULL)
    g_string_append_printf (str, " glib:ref-func=\"%s\"", self->glib_ref_func);
  if (self->glib_unref_func != NULL)
    g_string_append_printf (str, " glib:unref-func=\"%s\"", self->glib_unref_func);
  if (self->glib_set_value_func != NULL)
    g_string_append_printf (str, " glib:set-value-func=\"%s\"", self->glib_set_value_func);
  if (self->glib_get_value_func != NULL)
    g_string_append_printf (str, " glib:get-value-func=\"%s\"", self->glib_get_value_func);
  if (self->c_type != NULL)
    g_string_append_printf (str, " c:type=\"%s\"", self->c_type);
  if (self->c_symbol_prefix != NULL)
    g_string_append_printf (str, " c:symbol-prefix=\"%s\"", self->c_symbol_prefix);
  if (self->abstract != NULL)
    g_string_append_printf (str, " abstract=\"%s\"", self->abstract);
  if (self->glib_fundamental != NULL)
    g_string_append_printf (str, " glib:fundamental=\"%s\"", self->glib_fundamental);

  if (self->children != NULL && self->children->len > 0)
    {
      g_string_append (str, ">\n");

      for (i = 0; i < self->children->len; i++)
        girst_parser_object_printf (g_ptr_array_index (self->children, i), str, depth + 1);

      for (i = 0; i < depth; i++)
        g_string_append (str, "  ");
      g_string_append (str, "</class>\n");
    }
  else
    {
      g_string_append (str, "/>\n");
    }
}

static void
girst_class_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  GirstClass *self = (GirstClass *)object;

  switch (prop_id)
    {
    case PROP_INTROSPECTABLE:
      g_value_set_string (value, self->introspectable);
      break;

    case PROP_DEPRECATED:
      g_value_set_string (value, self->deprecated);
      break;

    case PROP_DEPRECATED_VERSION:
      g_value_set_string (value, self->deprecated_version);
      break;

    case PROP_VERSION:
      g_value_set_string (value, self->version);
      break;

    case PROP_STABILITY:
      g_value_set_string (value, self->stability);
      break;

    case PROP_NAME:
      g_value_set_string (value, self->name);
      break;

    case PROP_GLIB_TYPE_NAME:
      g_value_set_string (value, self->glib_type_name);
      break;

    case PROP_GLIB_GET_TYPE:
      g_value_set_string (value, self->glib_get_type);
      break;

    case PROP_PARENT:
      g_value_set_string (value, self->parent);
      break;

    case PROP_GLIB_TYPE_STRUCT:
      g_value_set_string (value, self->glib_type_struct);
      break;

    case PROP_GLIB_REF_FUNC:
      g_value_set_string (value, self->glib_ref_func);
      break;

    case PROP_GLIB_UNREF_FUNC:
      g_value_set_string (value, self->glib_unref_func);
      break;

    case PROP_GLIB_SET_VALUE_FUNC:
      g_value_set_string (value, self->glib_set_value_func);
      break;

    case PROP_GLIB_GET_VALUE_FUNC:
      g_value_set_string (value, self->glib_get_value_func);
      break;

    case PROP_C_TYPE:
      g_value_set_string (value, self->c_type);
      break;

    case PROP_C_SYMBOL_PREFIX:
      g_value_set_string (value, self->c_symbol_prefix);
      break;

    case PROP_ABSTRACT:
      g_value_set_string (value, self->abstract);
      break;

    case PROP_GLIB_FUNDAMENTAL:
      g_value_set_string (value, self->glib_fundamental);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
girst_class_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  GirstClass *self = (GirstClass *)object;
  GirstParserContext *context = girst_parser_object_get_parser_context (GIRST_PARSER_OBJECT (self));

  switch (prop_id)
    {
    case PROP_INTROSPECTABLE:
      self->introspectable = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_DEPRECATED:
      self->deprecated = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_DEPRECATED_VERSION:
      self->deprecated_version = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_VERSION:
      self->version = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_STABILITY:
      self->stability = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_NAME:
      self->name = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_GLIB_TYPE_NAME:
      self->glib_type_name = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_GLIB_GET_TYPE:
      self->glib_get_type = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_PARENT:
      self->parent = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_GLIB_TYPE_STRUCT:
      self->glib_type_struct = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_GLIB_REF_FUNC:
      self->glib_ref_func = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_GLIB_UNREF_FUNC:
      self->glib_unref_func = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_GLIB_SET_VALUE_FUNC:
      self->glib_set_value_func = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_GLIB_GET_VALUE_FUNC:
      self->glib_get_value_func = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_C_TYPE:
      self->c_type = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_C_SYMBOL_PREFIX:
      self->c_symbol_prefix = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_ABSTRACT:
      self->abstract = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_GLIB_FUNDAMENTAL:
      self->glib_fundamental = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
girst_class_finalize (GObject *object)
{
  GirstClass *self = (GirstClass *)object;

  g_clear_pointer (&self->children, g_ptr_array_unref);

  G_OBJECT_CLASS (girst_class_parent_class)->finalize (object);
}

static void
girst_class_class_init (GirstClassClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GirstParserObjectClass *parent_class = GIRST_PARSER_OBJECT_CLASS (klass);

  object_class->get_property = girst_class_get_property;
  object_class->set_property = girst_class_set_property;
  object_class->finalize = girst_class_finalize;

  parent_class->ingest = girst_class_ingest;
  parent_class->printf = girst_class_printf;
  parent_class->get_children = girst_class_get_children;

  properties [PROP_INTROSPECTABLE] =
    g_param_spec_string ("introspectable",
                         "introspectable",
                         "introspectable",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_DEPRECATED] =
    g_param_spec_string ("deprecated",
                         "deprecated",
                         "deprecated",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_DEPRECATED_VERSION] =
    g_param_spec_string ("deprecated-version",
                         "deprecated-version",
                         "deprecated-version",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_VERSION] =
    g_param_spec_string ("version",
                         "version",
                         "version",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_STABILITY] =
    g_param_spec_string ("stability",
                         "stability",
                         "stability",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "name",
                         "name",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_GLIB_TYPE_NAME] =
    g_param_spec_string ("glib-type-name",
                         "glib-type-name",
                         "glib-type-name",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_GLIB_GET_TYPE] =
    g_param_spec_string ("glib-get-type",
                         "glib-get-type",
                         "glib-get-type",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_PARENT] =
    g_param_spec_string ("parent",
                         "parent",
                         "parent",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_GLIB_TYPE_STRUCT] =
    g_param_spec_string ("glib-type-struct",
                         "glib-type-struct",
                         "glib-type-struct",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_GLIB_REF_FUNC] =
    g_param_spec_string ("glib-ref-func",
                         "glib-ref-func",
                         "glib-ref-func",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_GLIB_UNREF_FUNC] =
    g_param_spec_string ("glib-unref-func",
                         "glib-unref-func",
                         "glib-unref-func",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_GLIB_SET_VALUE_FUNC] =
    g_param_spec_string ("glib-set-value-func",
                         "glib-set-value-func",
                         "glib-set-value-func",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_GLIB_GET_VALUE_FUNC] =
    g_param_spec_string ("glib-get-value-func",
                         "glib-get-value-func",
                         "glib-get-value-func",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_C_TYPE] =
    g_param_spec_string ("c-type",
                         "c-type",
                         "c-type",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_C_SYMBOL_PREFIX] =
    g_param_spec_string ("c-symbol-prefix",
                         "c-symbol-prefix",
                         "c-symbol-prefix",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_ABSTRACT] =
    g_param_spec_string ("abstract",
                         "abstract",
                         "abstract",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_GLIB_FUNDAMENTAL] =
    g_param_spec_string ("glib-fundamental",
                         "glib-fundamental",
                         "glib-fundamental",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
girst_class_init (GirstClass *self)
{
  self->children = g_ptr_array_new_with_free_func (g_object_unref);
}

const gchar *
girst_class_get_introspectable (GirstClass *self)
{
  g_return_val_if_fail (GIRST_IS_CLASS (self), NULL);

  return self->introspectable;
}

const gchar *
girst_class_get_deprecated (GirstClass *self)
{
  g_return_val_if_fail (GIRST_IS_CLASS (self), NULL);

  return self->deprecated;
}

const gchar *
girst_class_get_deprecated_version (GirstClass *self)
{
  g_return_val_if_fail (GIRST_IS_CLASS (self), NULL);

  return self->deprecated_version;
}

const gchar *
girst_class_get_version (GirstClass *self)
{
  g_return_val_if_fail (GIRST_IS_CLASS (self), NULL);

  return self->version;
}

const gchar *
girst_class_get_stability (GirstClass *self)
{
  g_return_val_if_fail (GIRST_IS_CLASS (self), NULL);

  return self->stability;
}

const gchar *
girst_class_get_name (GirstClass *self)
{
  g_return_val_if_fail (GIRST_IS_CLASS (self), NULL);

  return self->name;
}

const gchar *
girst_class_get_glib_type_name (GirstClass *self)
{
  g_return_val_if_fail (GIRST_IS_CLASS (self), NULL);

  return self->glib_type_name;
}

const gchar *
girst_class_get_glib_get_type (GirstClass *self)
{
  g_return_val_if_fail (GIRST_IS_CLASS (self), NULL);

  return self->glib_get_type;
}

const gchar *
girst_class_get_parent (GirstClass *self)
{
  g_return_val_if_fail (GIRST_IS_CLASS (self), NULL);

  return self->parent;
}

const gchar *
girst_class_get_glib_type_struct (GirstClass *self)
{
  g_return_val_if_fail (GIRST_IS_CLASS (self), NULL);

  return self->glib_type_struct;
}

const gchar *
girst_class_get_glib_ref_func (GirstClass *self)
{
  g_return_val_if_fail (GIRST_IS_CLASS (self), NULL);

  return self->glib_ref_func;
}

const gchar *
girst_class_get_glib_unref_func (GirstClass *self)
{
  g_return_val_if_fail (GIRST_IS_CLASS (self), NULL);

  return self->glib_unref_func;
}

const gchar *
girst_class_get_glib_set_value_func (GirstClass *self)
{
  g_return_val_if_fail (GIRST_IS_CLASS (self), NULL);

  return self->glib_set_value_func;
}

const gchar *
girst_class_get_glib_get_value_func (GirstClass *self)
{
  g_return_val_if_fail (GIRST_IS_CLASS (self), NULL);

  return self->glib_get_value_func;
}

const gchar *
girst_class_get_c_type (GirstClass *self)
{
  g_return_val_if_fail (GIRST_IS_CLASS (self), NULL);

  return self->c_type;
}

const gchar *
girst_class_get_c_symbol_prefix (GirstClass *self)
{
  g_return_val_if_fail (GIRST_IS_CLASS (self), NULL);

  return self->c_symbol_prefix;
}

const gchar *
girst_class_get_abstract (GirstClass *self)
{
  g_return_val_if_fail (GIRST_IS_CLASS (self), NULL);

  return self->abstract;
}

const gchar *
girst_class_get_glib_fundamental (GirstClass *self)
{
  g_return_val_if_fail (GIRST_IS_CLASS (self), NULL);

  return self->glib_fundamental;
}

GirstClass *
girst_class_new (GirstParserContext *parser_context)
{
  return g_object_new (GIRST_TYPE_CLASS,
                       "parser-context", parser_context,
                       NULL);
}
