/* girst-annotation.c
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "girst-annotation"

#include "girst-annotation.h"

struct _GirstAnnotation
{
  GObject parent_instance;
  const gchar *key;
  const gchar *value;
};

G_DEFINE_TYPE (GirstAnnotation, girst_annotation, GIRST_TYPE_PARSER_OBJECT)

enum {
  PROP_0,
  PROP_KEY,
  PROP_VALUE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static gboolean
girst_annotation_ingest (GirstParserObject *object,
                         GMarkupParseContext *context,
                         const gchar *element_name,
                         const gchar **attribute_names,
                         const gchar **attribute_values,
                         GError **error)
{
  GirstAnnotation *self = (GirstAnnotation *)object;
  GirstParserContext *parser_context;
  const gchar *key = NULL;
  const gchar *value = NULL;

  g_assert (GIRST_IS_ANNOTATION (self));
  g_assert (g_str_equal (element_name, "annotation"));

  parser_context = girst_parser_object_get_parser_context (GIRST_PARSER_OBJECT (self));


  if (!girst_g_markup_collect_attributes (element_name, attribute_names, attribute_values, error,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "key", &key,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "value", &value,
                                          G_MARKUP_COLLECT_INVALID, NULL, NULL))
    return FALSE;

  self->key = girst_parser_context_intern_string (parser_context, key);
  self->value = girst_parser_context_intern_string (parser_context, value);

  return TRUE;
}

static void
girst_annotation_printf (GirstParserObject *object,
                         GString *str,
                         guint depth)
{
  GirstAnnotation *self = (GirstAnnotation *)object;
  guint i;

  g_assert (GIRST_IS_ANNOTATION (self));

  for (i = 0; i < depth; i++)
    g_string_append (str, "  ");
  g_string_append (str, "<annotation");

  if (self->key != NULL)
    g_string_append_printf (str, " key=\"%s\"", self->key);
  if (self->value != NULL)
    g_string_append_printf (str, " value=\"%s\"", self->value);

  g_string_append (str, "/>\n");
}

static void
girst_annotation_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  GirstAnnotation *self = (GirstAnnotation *)object;

  switch (prop_id)
    {
    case PROP_KEY:
      g_value_set_string (value, self->key);
      break;

    case PROP_VALUE:
      g_value_set_string (value, self->value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
girst_annotation_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  GirstAnnotation *self = (GirstAnnotation *)object;
  GirstParserContext *context = girst_parser_object_get_parser_context (GIRST_PARSER_OBJECT (self));

  switch (prop_id)
    {
    case PROP_KEY:
      self->key = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_VALUE:
      self->value = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
girst_annotation_finalize (GObject *object)
{

  G_OBJECT_CLASS (girst_annotation_parent_class)->finalize (object);
}

static void
girst_annotation_class_init (GirstAnnotationClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GirstParserObjectClass *parent_class = GIRST_PARSER_OBJECT_CLASS (klass);

  object_class->get_property = girst_annotation_get_property;
  object_class->set_property = girst_annotation_set_property;
  object_class->finalize = girst_annotation_finalize;

  parent_class->ingest = girst_annotation_ingest;
  parent_class->printf = girst_annotation_printf;

  properties [PROP_KEY] =
    g_param_spec_string ("key",
                         "key",
                         "key",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_VALUE] =
    g_param_spec_string ("value",
                         "value",
                         "value",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
girst_annotation_init (GirstAnnotation *self)
{
}

const gchar *
girst_annotation_get_key (GirstAnnotation *self)
{
  g_return_val_if_fail (GIRST_IS_ANNOTATION (self), NULL);

  return self->key;
}

const gchar *
girst_annotation_get_value (GirstAnnotation *self)
{
  g_return_val_if_fail (GIRST_IS_ANNOTATION (self), NULL);

  return self->value;
}

GirstAnnotation *
girst_annotation_new (GirstParserContext *parser_context)
{
  return g_object_new (GIRST_TYPE_ANNOTATION,
                       "parser-context", parser_context,
                       NULL);
}
