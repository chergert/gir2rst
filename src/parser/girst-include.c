/* girst-include.c
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "girst-include"

#include "girst-include.h"

struct _GirstInclude
{
  GObject parent_instance;
  const gchar *name;
  const gchar *version;
};

G_DEFINE_TYPE (GirstInclude, girst_include, GIRST_TYPE_PARSER_OBJECT)

enum {
  PROP_0,
  PROP_NAME,
  PROP_VERSION,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static gboolean
girst_include_ingest (GirstParserObject *object,
                      GMarkupParseContext *context,
                      const gchar *element_name,
                      const gchar **attribute_names,
                      const gchar **attribute_values,
                      GError **error)
{
  GirstInclude *self = (GirstInclude *)object;
  GirstParserContext *parser_context;
  const gchar *name = NULL;
  const gchar *version = NULL;

  g_assert (GIRST_IS_INCLUDE (self));
  g_assert (g_str_equal (element_name, "include"));

  parser_context = girst_parser_object_get_parser_context (GIRST_PARSER_OBJECT (self));


  if (!girst_g_markup_collect_attributes (element_name, attribute_names, attribute_values, error,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "name", &name,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "version", &version,
                                          G_MARKUP_COLLECT_INVALID, NULL, NULL))
    return FALSE;

  self->name = girst_parser_context_intern_string (parser_context, name);
  self->version = girst_parser_context_intern_string (parser_context, version);

  return TRUE;
}

static void
girst_include_printf (GirstParserObject *object,
                      GString *str,
                      guint depth)
{
  GirstInclude *self = (GirstInclude *)object;
  guint i;

  g_assert (GIRST_IS_INCLUDE (self));

  for (i = 0; i < depth; i++)
    g_string_append (str, "  ");
  g_string_append (str, "<include");

  if (self->name != NULL)
    g_string_append_printf (str, " name=\"%s\"", self->name);
  if (self->version != NULL)
    g_string_append_printf (str, " version=\"%s\"", self->version);

  g_string_append (str, "/>\n");
}

static void
girst_include_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  GirstInclude *self = (GirstInclude *)object;

  switch (prop_id)
    {
    case PROP_NAME:
      g_value_set_string (value, self->name);
      break;

    case PROP_VERSION:
      g_value_set_string (value, self->version);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
girst_include_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  GirstInclude *self = (GirstInclude *)object;
  GirstParserContext *context = girst_parser_object_get_parser_context (GIRST_PARSER_OBJECT (self));

  switch (prop_id)
    {
    case PROP_NAME:
      self->name = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_VERSION:
      self->version = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
girst_include_finalize (GObject *object)
{

  G_OBJECT_CLASS (girst_include_parent_class)->finalize (object);
}

static void
girst_include_class_init (GirstIncludeClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GirstParserObjectClass *parent_class = GIRST_PARSER_OBJECT_CLASS (klass);

  object_class->get_property = girst_include_get_property;
  object_class->set_property = girst_include_set_property;
  object_class->finalize = girst_include_finalize;

  parent_class->ingest = girst_include_ingest;
  parent_class->printf = girst_include_printf;

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "name",
                         "name",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_VERSION] =
    g_param_spec_string ("version",
                         "version",
                         "version",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
girst_include_init (GirstInclude *self)
{
}

const gchar *
girst_include_get_name (GirstInclude *self)
{
  g_return_val_if_fail (GIRST_IS_INCLUDE (self), NULL);

  return self->name;
}

const gchar *
girst_include_get_version (GirstInclude *self)
{
  g_return_val_if_fail (GIRST_IS_INCLUDE (self), NULL);

  return self->version;
}

GirstInclude *
girst_include_new (GirstParserContext *parser_context)
{
  return g_object_new (GIRST_TYPE_INCLUDE,
                       "parser-context", parser_context,
                       NULL);
}
