/* girst-member.c
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "girst-member"

#include "girst-member.h"

#include "girst-doc-version.h"
#include "girst-doc-stability.h"
#include "girst-doc.h"
#include "girst-doc-deprecated.h"
#include "girst-annotation.h"

struct _GirstMember
{
  GObject parent_instance;
  const gchar *introspectable;
  const gchar *deprecated;
  const gchar *deprecated_version;
  const gchar *version;
  const gchar *stability;
  const gchar *name;
  const gchar *value;
  const gchar *c_identifier;
  const gchar *glib_nick;
  GPtrArray *children;
};

G_DEFINE_TYPE (GirstMember, girst_member, GIRST_TYPE_PARSER_OBJECT)

enum {
  PROP_0,
  PROP_INTROSPECTABLE,
  PROP_DEPRECATED,
  PROP_DEPRECATED_VERSION,
  PROP_VERSION,
  PROP_STABILITY,
  PROP_NAME,
  PROP_VALUE,
  PROP_C_IDENTIFIER,
  PROP_GLIB_NICK,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static GPtrArray *
girst_member_get_children (GirstParserObject *object)
{
  GirstMember *self = (GirstMember *)object;

  g_assert (GIRST_IS_MEMBER (self));

  return self->children;
}

static void
girst_member_start_element (GMarkupParseContext *context,
                            const gchar *element_name,
                            const gchar **attribute_names,
                            const gchar **attribute_values,
                            gpointer user_data,
                            GError **error)
{
  GirstMember *self = user_data;
  GirstParserContext *parser_context;

  g_assert (GIRST_IS_MEMBER (self));
  g_assert (context != NULL);
  g_assert (element_name != NULL);
  g_assert (attribute_names != NULL);
  g_assert (attribute_values != NULL);

  parser_context = girst_parser_object_get_parser_context (GIRST_PARSER_OBJECT (self));

  if (FALSE) {}
  else if (g_str_equal (element_name, "doc-version"))
    {
      g_autoptr(GirstDocVersion) child = NULL;

      child = girst_doc_version_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "doc-stability"))
    {
      g_autoptr(GirstDocStability) child = NULL;

      child = girst_doc_stability_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "doc"))
    {
      g_autoptr(GirstDoc) child = NULL;

      child = girst_doc_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "doc-deprecated"))
    {
      g_autoptr(GirstDocDeprecated) child = NULL;

      child = girst_doc_deprecated_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
  else if (g_str_equal (element_name, "annotation"))
    {
      g_autoptr(GirstAnnotation) child = NULL;

      child = girst_annotation_new (parser_context);

      if (!girst_parser_object_ingest (GIRST_PARSER_OBJECT (child), context, element_name, attribute_names, attribute_values, error))
        return;

      _girst_parser_object_set_parent (GIRST_PARSER_OBJECT (child), GIRST_PARSER_OBJECT (self));

      g_ptr_array_add (self->children, g_steal_pointer (&child));
    }
}

static void
girst_member_end_element (GMarkupParseContext *context,
                          const gchar *element_name,
                          gpointer user_data,
                          GError **error)
{
  g_assert (GIRST_IS_MEMBER (user_data));
  g_assert (context != NULL);
  g_assert (element_name != NULL);

  if (FALSE) {}
  else if (g_str_equal (element_name, "doc-version"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "doc-stability"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "doc"))
    {
      g_markup_parse_context_pop (context);
    }
  else if (g_str_equal (element_name, "doc-deprecated"))
    {
      g_markup_parse_context_pop (context);
    }
}

static const GMarkupParser markup_parser = {
  girst_member_start_element,
  girst_member_end_element,
  NULL,
  NULL,
  NULL,
};

static gboolean
girst_member_ingest (GirstParserObject *object,
                     GMarkupParseContext *context,
                     const gchar *element_name,
                     const gchar **attribute_names,
                     const gchar **attribute_values,
                     GError **error)
{
  GirstMember *self = (GirstMember *)object;
  GirstParserContext *parser_context;
  const gchar *introspectable = NULL;
  const gchar *deprecated = NULL;
  const gchar *deprecated_version = NULL;
  const gchar *version = NULL;
  const gchar *stability = NULL;
  const gchar *name = NULL;
  const gchar *value = NULL;
  const gchar *c_identifier = NULL;
  const gchar *glib_nick = NULL;

  g_assert (GIRST_IS_MEMBER (self));
  g_assert (g_str_equal (element_name, "member"));

  parser_context = girst_parser_object_get_parser_context (GIRST_PARSER_OBJECT (self));


  if (!girst_g_markup_collect_attributes (element_name, attribute_names, attribute_values, error,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "introspectable", &introspectable,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "deprecated", &deprecated,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "deprecated-version", &deprecated_version,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "version", &version,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "stability", &stability,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "name", &name,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "value", &value,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "c:identifier", &c_identifier,
                                          G_MARKUP_COLLECT_STRING | G_MARKUP_COLLECT_OPTIONAL, "glib:nick", &glib_nick,
                                          G_MARKUP_COLLECT_INVALID, NULL, NULL))
    return FALSE;

  self->introspectable = girst_parser_context_intern_string (parser_context, introspectable);
  self->deprecated = girst_parser_context_intern_string (parser_context, deprecated);
  self->deprecated_version = girst_parser_context_intern_string (parser_context, deprecated_version);
  self->version = girst_parser_context_intern_string (parser_context, version);
  self->stability = girst_parser_context_intern_string (parser_context, stability);
  self->name = girst_parser_context_intern_string (parser_context, name);
  self->value = girst_parser_context_intern_string (parser_context, value);
  self->c_identifier = girst_parser_context_intern_string (parser_context, c_identifier);
  self->glib_nick = girst_parser_context_intern_string (parser_context, glib_nick);

  g_markup_parse_context_push (context, &markup_parser, self);

  return TRUE;
}

static void
girst_member_printf (GirstParserObject *object,
                     GString *str,
                     guint depth)
{
  GirstMember *self = (GirstMember *)object;
  guint i;

  g_assert (GIRST_IS_MEMBER (self));

  for (i = 0; i < depth; i++)
    g_string_append (str, "  ");
  g_string_append (str, "<member");

  if (self->introspectable != NULL)
    g_string_append_printf (str, " introspectable=\"%s\"", self->introspectable);
  if (self->deprecated != NULL)
    g_string_append_printf (str, " deprecated=\"%s\"", self->deprecated);
  if (self->deprecated_version != NULL)
    g_string_append_printf (str, " deprecated-version=\"%s\"", self->deprecated_version);
  if (self->version != NULL)
    g_string_append_printf (str, " version=\"%s\"", self->version);
  if (self->stability != NULL)
    g_string_append_printf (str, " stability=\"%s\"", self->stability);
  if (self->name != NULL)
    g_string_append_printf (str, " name=\"%s\"", self->name);
  if (self->value != NULL)
    g_string_append_printf (str, " value=\"%s\"", self->value);
  if (self->c_identifier != NULL)
    g_string_append_printf (str, " c:identifier=\"%s\"", self->c_identifier);
  if (self->glib_nick != NULL)
    g_string_append_printf (str, " glib:nick=\"%s\"", self->glib_nick);

  if (self->children != NULL && self->children->len > 0)
    {
      g_string_append (str, ">\n");

      for (i = 0; i < self->children->len; i++)
        girst_parser_object_printf (g_ptr_array_index (self->children, i), str, depth + 1);

      for (i = 0; i < depth; i++)
        g_string_append (str, "  ");
      g_string_append (str, "</member>\n");
    }
  else
    {
      g_string_append (str, "/>\n");
    }
}

static void
girst_member_get_property (GObject    *object,
                           guint       prop_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  GirstMember *self = (GirstMember *)object;

  switch (prop_id)
    {
    case PROP_INTROSPECTABLE:
      g_value_set_string (value, self->introspectable);
      break;

    case PROP_DEPRECATED:
      g_value_set_string (value, self->deprecated);
      break;

    case PROP_DEPRECATED_VERSION:
      g_value_set_string (value, self->deprecated_version);
      break;

    case PROP_VERSION:
      g_value_set_string (value, self->version);
      break;

    case PROP_STABILITY:
      g_value_set_string (value, self->stability);
      break;

    case PROP_NAME:
      g_value_set_string (value, self->name);
      break;

    case PROP_VALUE:
      g_value_set_string (value, self->value);
      break;

    case PROP_C_IDENTIFIER:
      g_value_set_string (value, self->c_identifier);
      break;

    case PROP_GLIB_NICK:
      g_value_set_string (value, self->glib_nick);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
girst_member_set_property (GObject      *object,
                           guint         prop_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
  GirstMember *self = (GirstMember *)object;
  GirstParserContext *context = girst_parser_object_get_parser_context (GIRST_PARSER_OBJECT (self));

  switch (prop_id)
    {
    case PROP_INTROSPECTABLE:
      self->introspectable = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_DEPRECATED:
      self->deprecated = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_DEPRECATED_VERSION:
      self->deprecated_version = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_VERSION:
      self->version = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_STABILITY:
      self->stability = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_NAME:
      self->name = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_VALUE:
      self->value = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_C_IDENTIFIER:
      self->c_identifier = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    case PROP_GLIB_NICK:
      self->glib_nick = girst_parser_context_intern_string (context, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
girst_member_finalize (GObject *object)
{
  GirstMember *self = (GirstMember *)object;

  g_clear_pointer (&self->children, g_ptr_array_unref);

  G_OBJECT_CLASS (girst_member_parent_class)->finalize (object);
}

static void
girst_member_class_init (GirstMemberClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GirstParserObjectClass *parent_class = GIRST_PARSER_OBJECT_CLASS (klass);

  object_class->get_property = girst_member_get_property;
  object_class->set_property = girst_member_set_property;
  object_class->finalize = girst_member_finalize;

  parent_class->ingest = girst_member_ingest;
  parent_class->printf = girst_member_printf;
  parent_class->get_children = girst_member_get_children;

  properties [PROP_INTROSPECTABLE] =
    g_param_spec_string ("introspectable",
                         "introspectable",
                         "introspectable",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_DEPRECATED] =
    g_param_spec_string ("deprecated",
                         "deprecated",
                         "deprecated",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_DEPRECATED_VERSION] =
    g_param_spec_string ("deprecated-version",
                         "deprecated-version",
                         "deprecated-version",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_VERSION] =
    g_param_spec_string ("version",
                         "version",
                         "version",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_STABILITY] =
    g_param_spec_string ("stability",
                         "stability",
                         "stability",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "name",
                         "name",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_VALUE] =
    g_param_spec_string ("value",
                         "value",
                         "value",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_C_IDENTIFIER] =
    g_param_spec_string ("c-identifier",
                         "c-identifier",
                         "c-identifier",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties [PROP_GLIB_NICK] =
    g_param_spec_string ("glib-nick",
                         "glib-nick",
                         "glib-nick",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
girst_member_init (GirstMember *self)
{
  self->children = g_ptr_array_new_with_free_func (g_object_unref);
}

const gchar *
girst_member_get_introspectable (GirstMember *self)
{
  g_return_val_if_fail (GIRST_IS_MEMBER (self), NULL);

  return self->introspectable;
}

const gchar *
girst_member_get_deprecated (GirstMember *self)
{
  g_return_val_if_fail (GIRST_IS_MEMBER (self), NULL);

  return self->deprecated;
}

const gchar *
girst_member_get_deprecated_version (GirstMember *self)
{
  g_return_val_if_fail (GIRST_IS_MEMBER (self), NULL);

  return self->deprecated_version;
}

const gchar *
girst_member_get_version (GirstMember *self)
{
  g_return_val_if_fail (GIRST_IS_MEMBER (self), NULL);

  return self->version;
}

const gchar *
girst_member_get_stability (GirstMember *self)
{
  g_return_val_if_fail (GIRST_IS_MEMBER (self), NULL);

  return self->stability;
}

const gchar *
girst_member_get_name (GirstMember *self)
{
  g_return_val_if_fail (GIRST_IS_MEMBER (self), NULL);

  return self->name;
}

const gchar *
girst_member_get_value (GirstMember *self)
{
  g_return_val_if_fail (GIRST_IS_MEMBER (self), NULL);

  return self->value;
}

const gchar *
girst_member_get_c_identifier (GirstMember *self)
{
  g_return_val_if_fail (GIRST_IS_MEMBER (self), NULL);

  return self->c_identifier;
}

const gchar *
girst_member_get_glib_nick (GirstMember *self)
{
  g_return_val_if_fail (GIRST_IS_MEMBER (self), NULL);

  return self->glib_nick;
}

GirstMember *
girst_member_new (GirstParserContext *parser_context)
{
  return g_object_new (GIRST_TYPE_MEMBER,
                       "parser-context", parser_context,
                       NULL);
}
