/* girst-parser-types.h
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GIRST_PARSER_TYPES_H
#define GIRST_PARSER_TYPES_H

#include <gio/gio.h>

G_BEGIN_DECLS

typedef struct _GirstParserContext GirstParserContext;

#define GIRST_TYPE_PARSER_OBJECT (girst_parser_object_get_type())
#define GIRST_TYPE_PARSER_CONTEXT (girst_parser_context_get_type())

G_DECLARE_DERIVABLE_TYPE (GirstParserObject, girst_parser_object, GIRST, PARSER_OBJECT, GObject)

struct _GirstParserObjectClass
{
  GObjectClass parent_class;

  GPtrArray *(*get_children) (GirstParserObject *self);
  gboolean   (*ingest)       (GirstParserObject *self,
                              GMarkupParseContext *context,
                              const gchar *element_name,
                              const gchar **attribute_names,
                              const gchar **attribute_values,
                              GError **error);
  void       (*printf)       (GirstParserObject *self,
                              GString *str,
                              guint depth);
};

#define GIRST_TYPE_REPOSITORY (girst_repository_get_type())
#define GIRST_TYPE_NAMESPACE (girst_namespace_get_type())
#define GIRST_TYPE_ANNOTATION (girst_annotation_get_type())
#define GIRST_TYPE_C_INCLUDE (girst_c_include_get_type())
#define GIRST_TYPE_INCLUDE (girst_include_get_type())
#define GIRST_TYPE_PACKAGE (girst_package_get_type())
#define GIRST_TYPE_ALIAS (girst_alias_get_type())
#define GIRST_TYPE_INTERFACE (girst_interface_get_type())
#define GIRST_TYPE_CLASS (girst_class_get_type())
#define GIRST_TYPE_GLIB_BOXED (girst_glib_boxed_get_type())
#define GIRST_TYPE_RECORD (girst_record_get_type())
#define GIRST_TYPE_DOC_VERSION (girst_doc_version_get_type())
#define GIRST_TYPE_DOC_STABILITY (girst_doc_stability_get_type())
#define GIRST_TYPE_DOC (girst_doc_get_type())
#define GIRST_TYPE_DOC_DEPRECATED (girst_doc_deprecated_get_type())
#define GIRST_TYPE_CONSTANT (girst_constant_get_type())
#define GIRST_TYPE_PROPERTY (girst_property_get_type())
#define GIRST_TYPE_GLIB_SIGNAL (girst_glib_signal_get_type())
#define GIRST_TYPE_FIELD (girst_field_get_type())
#define GIRST_TYPE_CALLBACK (girst_callback_get_type())
#define GIRST_TYPE_IMPLEMENTS (girst_implements_get_type())
#define GIRST_TYPE_PREREQUISITE (girst_prerequisite_get_type())
#define GIRST_TYPE_TYPE (girst_type_get_type())
#define GIRST_TYPE_ARRAY (girst_array_get_type())
#define GIRST_TYPE_CONSTRUCTOR (girst_constructor_get_type())
#define GIRST_TYPE_VARARGS (girst_varargs_get_type())
#define GIRST_TYPE_PARAMETERS (girst_parameters_get_type())
#define GIRST_TYPE_PARAMETER (girst_parameter_get_type())
#define GIRST_TYPE_INSTANCE_PARAMETER (girst_instance_parameter_get_type())
#define GIRST_TYPE_RETURN_VALUE (girst_return_value_get_type())
#define GIRST_TYPE_FUNCTION (girst_function_get_type())
#define GIRST_TYPE_METHOD (girst_method_get_type())
#define GIRST_TYPE_VIRTUAL_METHOD (girst_virtual_method_get_type())
#define GIRST_TYPE_UNION (girst_union_get_type())
#define GIRST_TYPE_BITFIELD (girst_bitfield_get_type())
#define GIRST_TYPE_ENUMERATION (girst_enumeration_get_type())
#define GIRST_TYPE_MEMBER (girst_member_get_type())
G_DECLARE_FINAL_TYPE (GirstRepository, girst_repository, GIRST, REPOSITORY, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstNamespace, girst_namespace, GIRST, NAMESPACE, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstAnnotation, girst_annotation, GIRST, ANNOTATION, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstCInclude, girst_c_include, GIRST, C_INCLUDE, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstInclude, girst_include, GIRST, INCLUDE, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstPackage, girst_package, GIRST, PACKAGE, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstAlias, girst_alias, GIRST, ALIAS, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstInterface, girst_interface, GIRST, INTERFACE, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstClass, girst_class, GIRST, CLASS, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstGlibBoxed, girst_glib_boxed, GIRST, GLIB_BOXED, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstRecord, girst_record, GIRST, RECORD, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstDocVersion, girst_doc_version, GIRST, DOC_VERSION, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstDocStability, girst_doc_stability, GIRST, DOC_STABILITY, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstDoc, girst_doc, GIRST, DOC, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstDocDeprecated, girst_doc_deprecated, GIRST, DOC_DEPRECATED, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstConstant, girst_constant, GIRST, CONSTANT, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstProperty, girst_property, GIRST, PROPERTY, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstGlibSignal, girst_glib_signal, GIRST, GLIB_SIGNAL, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstField, girst_field, GIRST, FIELD, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstCallback, girst_callback, GIRST, CALLBACK, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstImplements, girst_implements, GIRST, IMPLEMENTS, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstPrerequisite, girst_prerequisite, GIRST, PREREQUISITE, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstType, girst_type, GIRST, TYPE, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstArray, girst_array, GIRST, ARRAY, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstConstructor, girst_constructor, GIRST, CONSTRUCTOR, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstVarargs, girst_varargs, GIRST, VARARGS, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstParameters, girst_parameters, GIRST, PARAMETERS, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstParameter, girst_parameter, GIRST, PARAMETER, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstInstanceParameter, girst_instance_parameter, GIRST, INSTANCE_PARAMETER, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstReturnValue, girst_return_value, GIRST, RETURN_VALUE, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstFunction, girst_function, GIRST, FUNCTION, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstMethod, girst_method, GIRST, METHOD, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstVirtualMethod, girst_virtual_method, GIRST, VIRTUAL_METHOD, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstUnion, girst_union, GIRST, UNION, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstBitfield, girst_bitfield, GIRST, BITFIELD, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstEnumeration, girst_enumeration, GIRST, ENUMERATION, GirstParserObject)
G_DECLARE_FINAL_TYPE (GirstMember, girst_member, GIRST, MEMBER, GirstParserObject)

GirstParserObject *girst_parser_object_first_typed (GirstParserObject *self, GType child_type);
GPtrArray  *girst_parser_object_get_children       (GirstParserObject *self);
gboolean    girst_parser_object_ingest             (GirstParserObject *self,
                                                    GMarkupParseContext *context,
                                                    const gchar *element_name,
                                                    const gchar **attribute_names,
                                                    const gchar **attribute_values,
                                                    GError **error);
void        girst_parser_object_printf             (GirstParserObject *self,
                                                    GString *str,
                                                    guint depth);
GListModel *girst_parser_object_get_children_typed (GirstParserObject *self,
                                                      GType type);
gboolean    girst_parser_object_has_child_typed    (GirstParserObject *self,
                                                     GType type);

void _girst_parser_object_set_parent (GirstParserObject *self,
                                      GirstParserObject *parent) G_GNUC_INTERNAL;
GirstParserObject *girst_parser_object_get_parent (GirstParserObject *self);

GirstParserContext *girst_parser_object_get_parser_context (GirstParserObject *self);

gboolean   girst_g_markup_collect_attributes (const gchar *element_name,
                                              const gchar **attribute_names,
                                              const gchar **attribute_values,
                                              GError **error,
                                              GMarkupCollectType first_type,
                                              const gchar *first_attr,
                                              ...);

GType girst_parser_context_get_type (void);
GirstParserContext *girst_parser_context_new (void);
GirstParserContext *girst_parser_context_ref (GirstParserContext *self);
void girst_parser_context_unref (GirstParserContext *self);
const gchar *girst_parser_context_intern_string (GirstParserContext *self, const gchar *string);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (GirstParserContext, girst_parser_context_unref)

G_END_DECLS

#endif /* GIRST_PARSER_TYPES_H */
