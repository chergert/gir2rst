/* girst-bitfield.h
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GIRST_BITFIELD_H
#define GIRST_BITFIELD_H

#include "girst-parser-types.h"

G_BEGIN_DECLS

GirstBitfield *girst_bitfield_new (GirstParserContext *parser_context);

const gchar *girst_bitfield_get_introspectable (GirstBitfield *self);

const gchar *girst_bitfield_get_deprecated (GirstBitfield *self);

const gchar *girst_bitfield_get_deprecated_version (GirstBitfield *self);

const gchar *girst_bitfield_get_version (GirstBitfield *self);

const gchar *girst_bitfield_get_stability (GirstBitfield *self);

const gchar *girst_bitfield_get_name (GirstBitfield *self);

const gchar *girst_bitfield_get_c_type (GirstBitfield *self);

const gchar *girst_bitfield_get_glib_type_name (GirstBitfield *self);

const gchar *girst_bitfield_get_glib_get_type (GirstBitfield *self);

G_END_DECLS

#endif /* GIRST_BITFIELD */
