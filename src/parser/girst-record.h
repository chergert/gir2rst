/* girst-record.h
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GIRST_RECORD_H
#define GIRST_RECORD_H

#include "girst-parser-types.h"

G_BEGIN_DECLS

GirstRecord *girst_record_new (GirstParserContext *parser_context);

const gchar *girst_record_get_introspectable (GirstRecord *self);

const gchar *girst_record_get_deprecated (GirstRecord *self);

const gchar *girst_record_get_deprecated_version (GirstRecord *self);

const gchar *girst_record_get_version (GirstRecord *self);

const gchar *girst_record_get_stability (GirstRecord *self);

const gchar *girst_record_get_name (GirstRecord *self);

const gchar *girst_record_get_c_type (GirstRecord *self);

const gchar *girst_record_get_disguised (GirstRecord *self);

const gchar *girst_record_get_glib_type_name (GirstRecord *self);

const gchar *girst_record_get_glib_get_type (GirstRecord *self);

const gchar *girst_record_get_c_symbol_prefix (GirstRecord *self);

const gchar *girst_record_get_foreign (GirstRecord *self);

const gchar *girst_record_get_glib_is_gtype_struct_for (GirstRecord *self);

G_END_DECLS

#endif /* GIRST_RECORD */
