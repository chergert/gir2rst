/* girst-constructor.h
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GIRST_CONSTRUCTOR_H
#define GIRST_CONSTRUCTOR_H

#include "girst-parser-types.h"

G_BEGIN_DECLS

GirstConstructor *girst_constructor_new (GirstParserContext *parser_context);

const gchar *girst_constructor_get_introspectable (GirstConstructor *self);

const gchar *girst_constructor_get_deprecated (GirstConstructor *self);

const gchar *girst_constructor_get_deprecated_version (GirstConstructor *self);

const gchar *girst_constructor_get_version (GirstConstructor *self);

const gchar *girst_constructor_get_stability (GirstConstructor *self);

const gchar *girst_constructor_get_name (GirstConstructor *self);

const gchar *girst_constructor_get_c_identifier (GirstConstructor *self);

const gchar *girst_constructor_get_shadowed_by (GirstConstructor *self);

const gchar *girst_constructor_get_shadows (GirstConstructor *self);

const gchar *girst_constructor_get_throws (GirstConstructor *self);

const gchar *girst_constructor_get_moved_to (GirstConstructor *self);

G_END_DECLS

#endif /* GIRST_CONSTRUCTOR */
