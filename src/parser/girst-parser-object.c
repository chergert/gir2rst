/* girst-parser-object.c
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "girst-parser-object"

#include <gio/gio.h>

#include "girst-parser-types.h"

typedef struct
{
  GirstParserObject  *parent;
  GirstParserContext *parser_context;
} GirstParserObjectPrivate;

struct _GirstParserContext
{
  volatile gint ref_count;
  GStringChunk *strings;
};

enum {
  PROP_0,
  PROP_PARSER_CONTEXT,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static guint
get_n_items (GListModel *model)
{
  GirstParserObject *self = (GirstParserObject *)model;
  GPtrArray *children = girst_parser_object_get_children (self);
  return children ? children->len : 0;
}

static GType
get_item_type (GListModel *model)
{
  return GIRST_TYPE_PARSER_OBJECT;
}

static gpointer
get_item (GListModel *model,
          guint       position)
{
  GirstParserObject *self = (GirstParserObject *)model;
  GPtrArray *children = girst_parser_object_get_children (self);
  if (children && position < children->len)
    return g_object_ref (g_ptr_array_index (children, position));
  return NULL;
}

static void
list_model_iface_init (GListModelInterface *iface)
{
  iface->get_n_items = get_n_items;
  iface->get_item_type = get_item_type;
  iface->get_item = get_item;
}

G_DEFINE_TYPE_WITH_CODE (GirstParserObject, girst_parser_object, G_TYPE_OBJECT,
                         G_ADD_PRIVATE (GirstParserObject)
                         G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL, list_model_iface_init))
G_DEFINE_BOXED_TYPE (GirstParserContext, girst_parser_context, girst_parser_context_ref, girst_parser_context_unref)

GirstParserContext *
girst_parser_context_new (void)
{
  GirstParserContext *ret;

  ret = g_slice_new0 (GirstParserContext);
  ret->ref_count = 1;
  ret->strings = g_string_chunk_new (4096);

  return ret;
}

GirstParserContext *
girst_parser_context_ref (GirstParserContext *self)
{
  g_return_val_if_fail (self != NULL, NULL);
  g_return_val_if_fail (self->ref_count > 0, NULL);

  g_atomic_int_inc (&self->ref_count);

  return self;
}

void
girst_parser_context_unref (GirstParserContext *self)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (self->ref_count > 0);

  if (g_atomic_int_dec_and_test (&self->ref_count))
    {
      g_string_chunk_free (self->strings);
      g_slice_free (GirstParserContext, self);
    }
}

const gchar *
girst_parser_context_intern_string (GirstParserContext *self,
                                    const gchar *string)
{
  g_return_val_if_fail (self != NULL, NULL);

  /* treat empty as null to simplify code */
  if (string == NULL || *string == '\0')
    return NULL;
  return g_string_chunk_insert_const (self->strings, string);
}

/**
 * girst_parser_object_get_parser_context:
 * @self: A #GirstParserObject
 *
 * Gets the parser context associated with this object.
 *
 * Returns: (transfer none): A #GirstParserContext
 */
GirstParserContext *
girst_parser_object_get_parser_context (GirstParserObject *self)
{
  GirstParserObjectPrivate *priv = girst_parser_object_get_instance_private (self);

  g_return_val_if_fail (GIRST_IS_PARSER_OBJECT (self), NULL);

  return priv->parser_context;
}

void
_girst_parser_object_set_parent (GirstParserObject *self,
                                 GirstParserObject *parent)
{
  GirstParserObjectPrivate *priv = girst_parser_object_get_instance_private (self);

  g_return_if_fail (GIRST_IS_PARSER_OBJECT (self));
  g_return_if_fail (!parent || GIRST_IS_PARSER_OBJECT (parent));

  priv->parent = parent;
}

/**
 * girst_parser_object_get_parent:
 *
 * Gets the parent #GirstParserObject that was parsed
 * from the underlying document.
 *
 * Returns: (transfer none) (nullable): A #GirstParserObject or %NULL
 *   if the object is the root of the tree.
 */
GirstParserObject *
girst_parser_object_get_parent (GirstParserObject *self)
{
  GirstParserObjectPrivate *priv = girst_parser_object_get_instance_private (self);

  g_return_val_if_fail (GIRST_IS_PARSER_OBJECT (self), NULL);

  return priv->parent;
}

static void
girst_parser_object_finalize (GObject *object)
{
  GirstParserObject *self = (GirstParserObject *)object;
  GirstParserObjectPrivate *priv = girst_parser_object_get_instance_private (self);

  g_clear_pointer (&priv->parser_context, girst_parser_context_unref);

  G_OBJECT_CLASS (girst_parser_object_parent_class)->finalize (object);
}

static void
girst_parser_object_get_property (GObject    *object,
                                  guint       prop_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
  GirstParserObject *self = (GirstParserObject *)object;

  switch (prop_id)
    {
    case PROP_PARSER_CONTEXT:
      g_value_set_boxed (value, girst_parser_object_get_parser_context (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
girst_parser_object_set_property (GObject      *object,
                                  guint         prop_id,
                                  const GValue *value,
                                  GParamSpec   *pspec)
{
  GirstParserObject *self = (GirstParserObject *)object;
  GirstParserObjectPrivate *priv = girst_parser_object_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_PARSER_CONTEXT:
      priv->parser_context = g_value_dup_boxed (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
girst_parser_object_class_init (GirstParserObjectClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = girst_parser_object_finalize;
  object_class->get_property = girst_parser_object_get_property;
  object_class->set_property = girst_parser_object_set_property;

  properties [PROP_PARSER_CONTEXT] =
    g_param_spec_boxed ("parser-context",
                        "Parser Context",
                        "The parser context used for shared allocations",
                        GIRST_TYPE_PARSER_CONTEXT,
                        (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
girst_parser_object_init (GirstParserObject *self)
{
}

/**
 * girst_parser_object_first_typed:
 *
 * Gets the first child matching @child_type.
 *
 * Returns: (nullable) (transfer none): A #GirstParserObject or %NULL
 */
GirstParserObject *
girst_parser_object_first_typed (GirstParserObject *self, GType child_type)
{
  GPtrArray *ar = girst_parser_object_get_children (self);

  if (ar != NULL)
    {
      for (guint i = 0; i < ar->len; i++)
        {
          GirstParserObject *obj = g_ptr_array_index (ar, i);
          if (g_type_is_a (G_OBJECT_TYPE (obj), child_type))
            return obj;
        }
    }

  return NULL;
}

/**
 * girst_parser_object_get_children:
 * @self: An #GirstParserObject
 *
 * Gets all of the children of @self in the order they were parsed.
 *
 * Returns: (nullable) (transfer none) (element-type Girst.ParserObject):
 *   An array of GirstParserObject or %NULL.
 */
GPtrArray *
girst_parser_object_get_children (GirstParserObject *self)
{
  g_return_val_if_fail (GIRST_IS_PARSER_OBJECT (self), NULL);

  if (GIRST_PARSER_OBJECT_GET_CLASS (self)->get_children)
    return GIRST_PARSER_OBJECT_GET_CLASS (self)->get_children (self);

  return NULL;
}

/**
 * girst_parser_object_get_children_typed:
 * @self: An #GirstParserObject
 * @type: A #GType
 *
 * Gets all children of @self which are of type @type.
 *
 * Returns: (transfer full): An #GListStore of children matching @type.
 */
GListModel *
girst_parser_object_get_children_typed (GirstParserObject *self,
                                        GType type)
{
  GListStore *ret;
  GPtrArray *ar;

  g_return_val_if_fail (GIRST_IS_PARSER_OBJECT (self), NULL);

  ret = g_list_store_new (type);

  ar = girst_parser_object_get_children (self);

  if (ar != NULL)
    {
      guint i;

      for (i = 0; i < ar->len; i++)
        {
          GirstParserObject *item = g_ptr_array_index (ar, i);

          if (g_type_is_a (G_OBJECT_TYPE (item), type))
            g_list_store_append (ret, item);
        }
    }

  return G_LIST_MODEL (ret);
}

gboolean
girst_parser_object_has_child_typed (GirstParserObject *self,
                                     GType type)
{
  GPtrArray *ar;
  guint i;

  g_return_val_if_fail (GIRST_IS_PARSER_OBJECT (self), FALSE);

  ar = girst_parser_object_get_children (self);

  if (ar != NULL)
    {
      for (i = 0; i < ar->len; i++)
        {
          GObject *object = g_ptr_array_index (ar, i);

          if (g_type_is_a (G_TYPE_FROM_INSTANCE (object), type))
            return TRUE;
        }
    }

  return FALSE;
}

gboolean
girst_parser_object_ingest (GirstParserObject *self,
                            GMarkupParseContext *context,
                            const gchar *element_name,
                            const gchar **attribute_names,
                            const gchar **attribute_values,
                            GError **error)
{
  g_return_val_if_fail (GIRST_IS_PARSER_OBJECT (self), FALSE);
  g_return_val_if_fail (context != NULL, FALSE);
  g_return_val_if_fail (element_name != NULL, FALSE);
  g_return_val_if_fail (attribute_names != NULL, FALSE);
  g_return_val_if_fail (attribute_values != NULL, FALSE);

  if (GIRST_PARSER_OBJECT_GET_CLASS (self)->ingest)
    return GIRST_PARSER_OBJECT_GET_CLASS (self)->ingest (self, context, element_name, attribute_names, attribute_values, error);

  return TRUE;
}

void
girst_parser_object_printf (GirstParserObject *self,
                            GString *str,
                            guint depth)
{
  g_return_if_fail (GIRST_IS_PARSER_OBJECT (self));
  g_return_if_fail (str != NULL);

  if (GIRST_PARSER_OBJECT_GET_CLASS (self)->printf)
    GIRST_PARSER_OBJECT_GET_CLASS (self)->printf (self, str, depth);
}
