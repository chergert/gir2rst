/* girst-virtual-method.h
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GIRST_VIRTUAL_METHOD_H
#define GIRST_VIRTUAL_METHOD_H

#include "girst-parser-types.h"

G_BEGIN_DECLS

GirstVirtualMethod *girst_virtual_method_new (GirstParserContext *parser_context);

const gchar *girst_virtual_method_get_introspectable (GirstVirtualMethod *self);

const gchar *girst_virtual_method_get_deprecated (GirstVirtualMethod *self);

const gchar *girst_virtual_method_get_deprecated_version (GirstVirtualMethod *self);

const gchar *girst_virtual_method_get_version (GirstVirtualMethod *self);

const gchar *girst_virtual_method_get_stability (GirstVirtualMethod *self);

const gchar *girst_virtual_method_get_name (GirstVirtualMethod *self);

const gchar *girst_virtual_method_get_c_identifier (GirstVirtualMethod *self);

const gchar *girst_virtual_method_get_shadowed_by (GirstVirtualMethod *self);

const gchar *girst_virtual_method_get_shadows (GirstVirtualMethod *self);

const gchar *girst_virtual_method_get_throws (GirstVirtualMethod *self);

const gchar *girst_virtual_method_get_moved_to (GirstVirtualMethod *self);

const gchar *girst_virtual_method_get_invoker (GirstVirtualMethod *self);

G_END_DECLS

#endif /* GIRST_VIRTUAL_METHOD */
