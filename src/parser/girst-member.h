/* girst-member.h
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GIRST_MEMBER_H
#define GIRST_MEMBER_H

#include "girst-parser-types.h"

G_BEGIN_DECLS

GirstMember *girst_member_new (GirstParserContext *parser_context);

const gchar *girst_member_get_introspectable (GirstMember *self);

const gchar *girst_member_get_deprecated (GirstMember *self);

const gchar *girst_member_get_deprecated_version (GirstMember *self);

const gchar *girst_member_get_version (GirstMember *self);

const gchar *girst_member_get_stability (GirstMember *self);

const gchar *girst_member_get_name (GirstMember *self);

const gchar *girst_member_get_value (GirstMember *self);

const gchar *girst_member_get_c_identifier (GirstMember *self);

const gchar *girst_member_get_glib_nick (GirstMember *self);

G_END_DECLS

#endif /* GIRST_MEMBER */
