/* girst-instance-parameter.h
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GIRST_INSTANCE_PARAMETER_H
#define GIRST_INSTANCE_PARAMETER_H

#include "girst-parser-types.h"

G_BEGIN_DECLS

GirstInstanceParameter *girst_instance_parameter_new (GirstParserContext *parser_context);

const gchar *girst_instance_parameter_get_name (GirstInstanceParameter *self);

const gchar *girst_instance_parameter_get_nullable (GirstInstanceParameter *self);

const gchar *girst_instance_parameter_get_allow_none (GirstInstanceParameter *self);

const gchar *girst_instance_parameter_get_direction (GirstInstanceParameter *self);

const gchar *girst_instance_parameter_get_caller_allocates (GirstInstanceParameter *self);

const gchar *girst_instance_parameter_get_transfer_ownership (GirstInstanceParameter *self);

G_END_DECLS

#endif /* GIRST_INSTANCE_PARAMETER */
