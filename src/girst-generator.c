/* girst-generator.c
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "girst-generator"

#include <errno.h>
#include <glib/gstdio.h>
#include <tmpl-glib.h>

#include "girst-generator.h"
#include "parser/girst-namespace.h"

struct _GirstGenerator
{
  GObject              parent_instance;

  TmplTemplateLocator *locator;
  GirstRepository     *repository;
  gchar               *outdir;
  GHashTable          *templates;

  GirstLanguage        language;
};

G_DEFINE_TYPE (GirstGenerator, girst_generator, G_TYPE_OBJECT)

static void
girst_generator_finalize (GObject *object)
{
  GirstGenerator *self = (GirstGenerator *)object;

  g_clear_pointer (&self->templates, g_hash_table_unref);
  g_clear_pointer (&self->outdir, g_free);
  g_clear_object (&self->repository);
  g_clear_object (&self->locator);

  G_OBJECT_CLASS (girst_generator_parent_class)->finalize (object);
}

static void
girst_generator_class_init (GirstGeneratorClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = girst_generator_finalize;
}

static void
girst_generator_init (GirstGenerator *self)
{
  self->templates = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_object_unref);
  self->locator = tmpl_template_locator_new ();
  tmpl_template_locator_prepend_search_path (self->locator, "resource:///templates/");
}

static TmplTemplate *
girst_generator_create_template (GirstGenerator  *self,
                                 const gchar     *name,
                                 GError         **error)
{
  g_autoptr(GInputStream) stream = NULL;
  g_autofree gchar *path = NULL;
  TmplTemplate *template;

  g_assert (GIRST_IS_GENERATOR (self));

  template = g_hash_table_lookup (self->templates, name);

  if (template == NULL)
    {
      template = tmpl_template_new (self->locator);
      path = g_strdup_printf ("/templates/%s", name);
      if (!tmpl_template_parse_resource (template, path, NULL, error))
        return NULL;
      g_hash_table_insert (self->templates, g_strdup (name), g_object_ref (template));
    }

  return g_object_ref (template);
}

G_GNUC_NULL_TERMINATED static GOutputStream *
girst_generator_create_stream (GirstGenerator *self,
                               const gchar    *first_path,
                               ...)
{
  g_autoptr(GPtrArray) ar = NULL;
  g_autofree gchar *path = NULL;
  g_autofree gchar *dir = NULL;
  g_autoptr(GFile) file = NULL;
  const gchar *name;
  va_list args;

  g_assert (GIRST_IS_GENERATOR (self));
  g_assert (first_path != NULL);

  ar = g_ptr_array_new ();

  va_start (args, first_path);
  g_ptr_array_add (ar, (gchar *)self->outdir);
  g_ptr_array_add (ar, (gchar *)first_path);
  while (NULL != (name = va_arg (args, const gchar *)))
    g_ptr_array_add (ar, (gchar *)name);
  g_ptr_array_add (ar, NULL);
  va_end (args);

  path = g_build_filenamev ((gchar **)ar->pdata);
  file = g_file_new_for_path (path);

  dir = g_path_get_dirname (path);
  g_mkdir_with_parents (dir, 0750);

  return G_OUTPUT_STREAM (g_file_replace (file, NULL, FALSE,
                                          G_FILE_CREATE_REPLACE_DESTINATION,
                                          NULL, NULL));
}

GirstGenerator *
girst_generator_new (GirstRepository *repository,
                     const gchar     *outdir,
                     GirstLanguage    language)
{
  GirstGenerator *self;

  g_return_val_if_fail (GIRST_IS_REPOSITORY (repository), NULL);
  g_return_val_if_fail (language != 0, NULL);

  self = g_object_new (GIRST_TYPE_GENERATOR, NULL);
  self->outdir = g_strdup (outdir ? outdir : "generated");
  self->repository = g_object_ref (repository);
  self->language = language;

  return self;
}

static gboolean
resolver_cb (TmplScope    *scope,
             const gchar  *name,
             TmplSymbol  **symbol,
             gpointer      user_data)
{
  g_debug ("Missing symbol %s", name);
  *symbol = tmpl_symbol_new ();
  tmpl_symbol_assign_string (*symbol, NULL);
  return TRUE;
}

static gboolean
girst_generator_generate_namespace (GirstGenerator  *self,
                                    GirstRepository *repository,
                                    GirstNamespace  *ns,
                                    GError         **error)
{
  g_autofree gchar *dir = NULL;
  struct {
    const gchar *in_name;
    const gchar *out_name;
    const gchar *subdir;
    const gchar *subrst;
    const gchar *var;
    GType check_type;
  } info[] = {
    { "aliases.rst.tmpl",       "aliases.rst",      "aliases",      NULL,                   NULL,        GIRST_TYPE_ALIAS },
    { "bitfields.rst.tmpl",     "bitfields.rst",    "bitfields",    "bitfield.rst.tmpl",    "bitfield",  GIRST_TYPE_BITFIELD },
    { "callbacks.rst.tmpl",     "callbacks.rst",    NULL,           NULL,                   NULL,        GIRST_TYPE_CALLBACK },
    { "classes.rst.tmpl",       "classes.rst",      "classes",      "object.rst.tmpl",      "object",    GIRST_TYPE_CLASS },
    { "constants.rst.tmpl",     "constants.rst",    NULL,           NULL,                   NULL,        GIRST_TYPE_CONSTANT },
    { "enumerations.rst.tmpl",  "enumerations.rst", "enumerations", "enumeration.rst.tmpl", "enum",      GIRST_TYPE_ENUMERATION },
    { "functions.rst.tmpl",     "functions.rst",    NULL,           NULL,                   NULL,        GIRST_TYPE_FUNCTION },
    { "namespace.rst.tmpl",     "index.rst",        NULL,           NULL,                   NULL,        G_TYPE_INVALID },
    { "interfaces.rst.tmpl",    "interfaces.rst",   "interfaces",   "object.rst.tmpl",      "object",    GIRST_TYPE_INTERFACE },
    { "records.rst.tmpl",       "structs.rst",      "structs",      "object.rst.tmpl",      "object",    GIRST_TYPE_RECORD },
    { "boxes.rst.tmpl",         "boxes.rst",        "boxes",        "object.rst.tmpl",      "object",    GIRST_TYPE_GLIB_BOXED },
    { "unions.rst.tmpl",        "unions.rst",       "unions",       "object.rst.tmpl",      "object",    GIRST_TYPE_UNION },
  };

  g_assert (GIRST_IS_GENERATOR (self));
  g_assert (GIRST_IS_REPOSITORY (repository));
  g_assert (GIRST_IS_NAMESPACE (ns));

  dir = g_strdup_printf ("%s-%s",
                         girst_namespace_get_name (ns),
                         girst_namespace_get_version (ns));

  for (guint i = 0; i < G_N_ELEMENTS (info); i++)
    {
      const gchar *in_name = info[i].in_name;
      const gchar *out_name = info[i].out_name;
      const gchar *subdir = info[i].subdir;
      const gchar *subrst = info[i].subrst;
      const gchar *var = info[i].var;
      GType type = info[i].check_type;
      g_autoptr(GOutputStream) stream = NULL;
      g_autoptr(TmplTemplate) template = NULL;
      g_autoptr(TmplScope) scope = NULL;
      g_autoptr(GListModel) typed_children = NULL;
      guint len;

      if (type && !girst_parser_object_has_child_typed (GIRST_PARSER_OBJECT (ns), type))
        continue;

      if (!(stream = girst_generator_create_stream (self, dir, out_name, NULL)))
        {
          g_set_error (error,
                       G_FILE_ERROR,
                       G_FILE_ERROR_FAILED,
                       "Failed to create IO stream in %s", dir);
          return FALSE;
        }

      if (!(template = girst_generator_create_template (self, in_name, error)))
        return FALSE;

      scope = tmpl_scope_new ();

      tmpl_scope_set_object (scope, "repository", repository);
      tmpl_scope_set_object (scope, "namespace", ns);

      /* Member missing fallback */
      tmpl_scope_set_resolver (scope, resolver_cb, NULL, NULL);

      if (!tmpl_template_expand (template, stream, scope, NULL, error))
        return FALSE;

      if (!g_output_stream_close (stream, NULL, error))
        return FALSE;

      g_clear_object (&stream);
      g_clear_object (&template);

      if (!subdir || !var)
        continue;

      typed_children = girst_parser_object_get_children_typed (GIRST_PARSER_OBJECT (ns), type);
      len = g_list_model_get_n_items (typed_children);

      for (guint j = 0; j < len; j++)
        {
          g_autoptr(GirstParserObject) obj = g_list_model_get_item (typed_children, j);
          g_autofree gchar *name = NULL;
          g_autofree gchar *filename = NULL;

          g_object_get (obj, "c-type", &name, NULL);
          filename = g_strdup_printf ("%s.rst", name);

          if (!(stream = girst_generator_create_stream (self, dir, subdir, filename, NULL)))
            {
              g_set_error (error,
                           G_FILE_ERROR,
                           G_FILE_ERROR_FAILED,
                           "Failed to create IO stream in %s", dir);
              return FALSE;
            }

          tmpl_scope_set_object (scope, var, obj);

          if (!(template = girst_generator_create_template (self, subrst, error)))
            return FALSE;

          if (!tmpl_template_expand (template, stream, scope, NULL, error))
            return FALSE;

          if (!g_output_stream_close (stream, NULL, error))
            return FALSE;

          tmpl_scope_set_object (scope, var, NULL);

          g_clear_object (&template);
          g_clear_object (&stream);
        }
    }

  return TRUE;
}

static gboolean
girst_generator_generate_repository (GirstGenerator   *self,
                                     GirstRepository  *repository,
                                     GError          **error)
{
  GPtrArray *children;

  g_assert (GIRST_IS_GENERATOR (self));
  g_assert (GIRST_IS_REPOSITORY (repository));

  children = girst_parser_object_get_children (GIRST_PARSER_OBJECT (repository));

  for (guint i = 0; i < children->len; i++)
    {
      GirstParserObject *child = g_ptr_array_index (children, i);

      if (GIRST_IS_NAMESPACE (child))
        return girst_generator_generate_namespace (self, repository, GIRST_NAMESPACE (child), error);
    }

  return TRUE;
}

static void
girst_generator_generate_worker (GTask        *task,
                                 gpointer      source_object,
                                 gpointer      task_data,
                                 GCancellable *cancellable)
{
  GirstGenerator *self = source_object;
  g_autoptr(GError) error = NULL;

  g_assert (G_IS_TASK (task));
  g_assert (GIRST_IS_GENERATOR (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  errno = 0;
  if (g_mkdir_with_parents (self->outdir, 0750) != 0)
    g_task_return_new_error (task,
                             G_FILE_ERROR,
                             g_file_error_from_errno (errno),
                             "%s", g_strerror (errno));

  if (!girst_generator_generate_repository (self, self->repository, &error))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_boolean (task, TRUE);
}

void
girst_generator_generate_async (GirstGenerator      *self,
                                GCancellable        *cancellable,
                                GAsyncReadyCallback  callback,
                                gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;

  g_return_if_fail (GIRST_IS_GENERATOR (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, girst_generator_generate_async);
  g_task_run_in_thread (task, girst_generator_generate_worker);
}

gboolean
girst_generator_generate_finish (GirstGenerator  *self,
                                 GAsyncResult    *result,
                                 GError         **error)
{
  g_return_val_if_fail (GIRST_IS_GENERATOR (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}
