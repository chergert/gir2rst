/* girst-cache.c
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "girst-cache"

#include "parser/girst-alias.h"
#include "parser/girst-bitfield.h"
#include "parser/girst-class.h"
#include "parser/girst-callback.h"
#include "parser/girst-enumeration.h"
#include "parser/girst-interface.h"
#include "parser/girst-record.h"
#include "parser/girst-union.h"

#include "girst-cache.h"
#include "girst-util.h"

static GRWLock cache_rw_lock;
static GHashTable *cache;
static GHashTable *resolve_cache;
static GHashTable *ignored;

void
girst_cache_set (const gchar     *ns_name,
                 GirstRepository *repository)
{
  g_return_if_fail (ns_name);
  g_return_if_fail (GIRST_IS_REPOSITORY (repository));

  g_rw_lock_writer_lock (&cache_rw_lock);

  if G_UNLIKELY (cache == NULL)
    {
      cache = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_object_unref);

      /* Create an negative cache for things we can't resolve */
      ignored = g_hash_table_new (g_str_hash, g_str_equal);
      g_hash_table_insert (ignored, "GType", NULL);
      g_hash_table_insert (ignored, "filename", NULL);
      g_hash_table_insert (ignored, "gboolean", NULL);
      g_hash_table_insert (ignored, "gchar", NULL);
      g_hash_table_insert (ignored, "gdouble", NULL);
      g_hash_table_insert (ignored, "gint", NULL);
      g_hash_table_insert (ignored, "gint16", NULL);
      g_hash_table_insert (ignored, "gint32", NULL);
      g_hash_table_insert (ignored, "gint64", NULL);
      g_hash_table_insert (ignored, "gint8", NULL);
      g_hash_table_insert (ignored, "gpointer", NULL);
      g_hash_table_insert (ignored, "gsize", NULL);
      g_hash_table_insert (ignored, "gssize", NULL);
      g_hash_table_insert (ignored, "guint", NULL);
      g_hash_table_insert (ignored, "guint16", NULL);
      g_hash_table_insert (ignored, "guint32", NULL);
      g_hash_table_insert (ignored, "guint64", NULL);
      g_hash_table_insert (ignored, "guint8", NULL);
      g_hash_table_insert (ignored, "none", NULL);
      g_hash_table_insert (ignored, "utf8", NULL);
    }

  g_hash_table_insert (cache, g_strdup (ns_name), g_object_ref (repository));

  g_rw_lock_writer_unlock (&cache_rw_lock);
}

/**
 * girst_cache_get:
 *
 * Returns: (nullable) (transfer none): A #GirstRepository
 */
GirstRepository *
girst_cache_get (const gchar *name)
{
  GirstRepository *ret;

  g_return_val_if_fail (name != NULL, NULL);

  g_rw_lock_reader_lock (&cache_rw_lock);
  ret = cache ? g_hash_table_lookup (cache, name) : NULL;
  g_rw_lock_reader_unlock (&cache_rw_lock);

  return ret;
}

/**
 * girst_cache_resolve:
 *
 * Returns: (transfer none) (nullable): A #GirstParserObject or %NULL
 */
GirstParserObject *
girst_cache_resolve (const gchar *ns_name,
                     const gchar *type_name)
{
  g_autofree gchar *resolve_key = NULL;
  GirstRepository *repository;
  GirstParserObject *ns;
  GPtrArray *children;
  GirstParserObject *ret = NULL;

  if G_UNLIKELY (cache == NULL)
    return NULL;

  g_rw_lock_reader_lock (&cache_rw_lock);

  if (g_hash_table_contains (ignored, type_name))
    goto release_reader_and_fail;

  resolve_key = g_strdup_printf ("%s|%s", ns_name, type_name);

  if G_UNLIKELY (resolve_cache == NULL)
    {
      g_rw_lock_reader_unlock (&cache_rw_lock);
      g_rw_lock_writer_lock (&cache_rw_lock);
      resolve_cache = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, NULL);
      g_rw_lock_writer_unlock (&cache_rw_lock);
      g_rw_lock_reader_lock (&cache_rw_lock);
    }

  if ((ret = g_hash_table_lookup (resolve_cache, resolve_key)))
    {
      g_rw_lock_reader_unlock (&cache_rw_lock);
      return ret;
    }

  if (!(repository = g_hash_table_lookup (cache, ns_name)) ||
      !(ns = girst_parser_object_first_typed (GIRST_PARSER_OBJECT (repository), GIRST_TYPE_NAMESPACE)))
    {
      g_rw_lock_reader_unlock (&cache_rw_lock);
      return NULL;
    }

  g_rw_lock_reader_unlock (&cache_rw_lock);

  children = girst_parser_object_get_children (ns);

  for (guint i = 0; i < children->len; i++)
    {
      GirstParserObject *obj = g_ptr_array_index (children, i);

#define CHECK_SIMPLE(T, t, func) \
      else if (GIRST_IS_##T (obj) && \
               girst_str_equal0 (type_name, \
                                 girst_##t##_get_##func (GIRST_##T (obj)))) \
      { \
        g_rw_lock_writer_lock (&cache_rw_lock); \
        g_hash_table_insert (resolve_cache, g_steal_pointer (&resolve_key), obj); \
        g_rw_lock_writer_unlock (&cache_rw_lock); \
        return obj; \
      }

      if (0) {}
      CHECK_SIMPLE (ALIAS, alias, name)
      CHECK_SIMPLE (BITFIELD, bitfield, name)
      CHECK_SIMPLE (CALLBACK, callback, name)
      CHECK_SIMPLE (CLASS, class, name)
      CHECK_SIMPLE (INTERFACE, interface, name)
      CHECK_SIMPLE (ENUMERATION, enumeration, name)
      CHECK_SIMPLE (RECORD, record, name)
      CHECK_SIMPLE (UNION, union, name)

#undef CHECK_SIMPLE
    }

  return NULL;

release_reader_and_fail:
  g_rw_lock_reader_unlock (&cache_rw_lock);

  return NULL;
}

/**
 * girst_cache_foreach: (skip)
 * @foreach_func: (scope call): A func to call
 * @user_data: user data for @foreach_func
 *
 * Calls @foreach_func for each GirstRepository cached.
 */
void
girst_cache_foreach (GFunc    foreach_func,
                     gpointer user_data)
{
  GList *values;

  if (cache == NULL)
    return;

  /* Need to be re-entrant safe */

  g_rw_lock_reader_lock (&cache_rw_lock);
  values = g_hash_table_get_values (cache);
  g_rw_lock_reader_unlock (&cache_rw_lock);

  for (const GList *iter = values; iter != NULL; iter = iter->next)
    foreach_func (iter->data, user_data);

  g_list_free (values);
}
