/* girst-cache.h
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gio/gio.h>

#include "parser/girst-parser-types.h"

G_BEGIN_DECLS

void               girst_cache_set     (const gchar     *ns_name,
                                        GirstRepository *repository);
GirstRepository   *girst_cache_get     (const gchar     *name);
GirstParserObject *girst_cache_resolve (const gchar     *ns_name,
                                        const gchar     *type_name);
void               girst_cache_foreach (GFunc            foreach_func,
                                        gpointer         user_data);

G_END_DECLS
