/* girst-signature.h
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <glib-object.h>

#include "parser/girst-parser-types.h"

G_BEGIN_DECLS

gchar             *girst_alias_get_lhs_ctype              (GirstAlias             *self);
gchar             *girst_array_get_lhs_ctype              (GirstArray             *self);
gchar             *girst_callback_get_lhs_ctype           (GirstCallback          *self);
gchar             *girst_class_get_lhs_ctype              (GirstClass             *self);
gchar             *girst_constant_get_lhs_ctype           (GirstConstant          *self);
gchar             *girst_constructor_get_lhs_ctype        (GirstConstructor       *self);
gchar             *girst_function_get_lhs_ctype           (GirstFunction          *self);
gchar             *girst_glib_signal_get_lhs_ctype        (GirstGlibSignal        *self);
gchar             *girst_instance_parameter_get_lhs_ctype (GirstInstanceParameter *self);
gchar             *girst_interface_get_lhs_ctype          (GirstInterface         *self);
gchar             *girst_method_get_lhs_ctype             (GirstMethod            *self);
gchar             *girst_parameter_get_lhs_ctype          (GirstParameter         *self);
GirstParserObject *girst_parser_object_get_lhs            (GirstParserObject      *self);
gchar             *girst_parser_object_get_lhs_ctype      (GirstParserObject      *self);
gchar             *girst_property_get_lhs_ctype           (GirstProperty          *self);
gchar             *girst_record_get_lhs_ctype             (GirstRecord            *self);
gchar             *girst_return_value_get_lhs_ctype       (GirstReturnValue       *self);
gchar             *girst_union_get_lhs_ctype              (GirstUnion             *self);
gchar             *girst_virtual_method_get_lhs_ctype     (GirstVirtualMethod     *self);
gchar             *girst_type_get_lhs_ctype               (GirstType              *self);
gchar             *girst_varargs_get_lhs_ctype            (GirstVarargs           *self);
gchar             *girst_clink                            (GirstParserObject      *self);
gchar             *girst_clink_full                       (const gchar            *word,
                                                           GirstParserObject      *dest,
                                                           const gchar            *section);
gchar             *girst_parser_object_short_cdoc         (GirstParserObject      *self);
GListModel        *girst_parser_object_get_parameters     (GirstParserObject      *self);
gboolean           girst_parser_object_has_parent_type    (GirstParserObject      *self);
gboolean           girst_parser_object_has_signals        (GirstParserObject      *self);
gboolean           girst_parser_object_has_vfuncs         (GirstParserObject      *self);
gboolean           girst_parser_object_has_props          (GirstParserObject      *self);
gboolean           girst_parser_object_has_methods        (GirstParserObject      *self);
gboolean           girst_parser_object_has_functions      (GirstParserObject      *self);
gchar             *girst_parser_object_get_signature      (GirstParserObject      *self);

G_END_DECLS
