/* girst-main.c
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gio/gio.h>
#include <girepository.h>
#include <glib/gi18n.h>
#include <string.h>
#include <stdlib.h>

#include "girst-cache.h"
#include "girst-generator.h"

#include "parser/girst-include.h"
#include "parser/girst-parser.h"
#include "parser/girst-repository.h"

static gchar *language;
static gchar *outdir;
static gchar **girdirs;
static gint exit_code = EXIT_SUCCESS;
static gint active;
static const GOptionEntry main_entries[] = {
  { "outdir", 'o', 0, G_OPTION_ARG_FILENAME, &outdir,
    "Location to store generated files",
    "DIR" },
  { "language", 'l', 0, G_OPTION_ARG_STRING, &language,
    "Source code language to generate (C)",
    "LANG" },
  { "girdir", 'g', 0, G_OPTION_ARG_STRING_ARRAY, &girdirs,
    "A directory to locate additional .gir files from. May be specified more than once.",
    "DIR" },
  { NULL }
};

static void
girst_main_generate_cb (GObject      *object,
                        GAsyncResult *result,
                        gpointer      user_data)
{
  GirstGenerator *generator = (GirstGenerator *)object;
  g_autoptr(GMainLoop) main_loop = user_data;
  g_autoptr(GError) error = NULL;

  g_assert (GIRST_IS_GENERATOR (generator));
  g_assert (main_loop != NULL);

  if (!girst_generator_generate_finish (generator, result, &error))
    g_error ("%s", error->message);

  active--;

  if (active == 0)
    g_main_loop_quit (main_loop);
}

static void
foreach_repository_cb (gpointer data,
                       gpointer user_data)
{
  g_autoptr(GirstGenerator) generator = NULL;
  GirstRepository *repository = data;
  g_autoptr(GError) error = NULL;
  GMainLoop *main_loop = user_data;

  g_assert (GIRST_IS_REPOSITORY (repository));
  g_assert (main_loop != NULL);

  active++;

  generator = girst_generator_new (repository, outdir, GIRST_LANGUAGE_C);
  girst_generator_generate_async (generator,
                                  NULL,
                                  girst_main_generate_cb,
                                  g_main_loop_ref (main_loop));
}

static GFile *
locate_gir_file (const gchar *name,
                 const gchar *version)
{
  g_autofree gchar *filename = g_strdup_printf ("%s-%s.gir", name, version);

  for (guint i = 0; girdirs[i]; i++)
    {
      g_autofree gchar *path = g_build_filename (girdirs[i], filename, NULL);

      if (g_file_test (path, G_FILE_TEST_IS_REGULAR))
        return g_file_new_for_path (path);
    }

  return NULL;
}

static void
load_dependencies_cb (gpointer data,
                      gpointer user_data)
{
  GirstRepository *repository = data;
  g_autoptr(GError) error = NULL;
  GPtrArray *children;
  gboolean *found_new = user_data;

  g_assert (GIRST_IS_REPOSITORY (repository));
  g_assert (found_new != NULL);

  children = girst_parser_object_get_children (GIRST_PARSER_OBJECT (repository));

  for (guint i = 0; i < children->len; i++)
    {
      GirstParserObject *child = g_ptr_array_index (children, i);

      if (GIRST_IS_INCLUDE (child))
        {
          GirstRepository *peer;
          const gchar *name;
          const gchar *version;

          name = girst_include_get_name (GIRST_INCLUDE (child));
          version = girst_include_get_version (GIRST_INCLUDE (child));

          if (!(peer = girst_cache_get (name)))
            {
              g_autoptr(GFile) file = locate_gir_file (name, version);
              g_autoptr(GirstParser) parser = girst_parser_new ();
              g_autoptr(GirstRepository) dep = NULL;

              if (file == NULL)
                continue;

              if (!(dep = girst_parser_parse_file (parser, file, NULL, &error)))
                {
                  g_autofree gchar *path = g_file_get_path (file);
                  g_printerr ("%s: Error: %s\n", path, error->message);
                  exit (EXIT_FAILURE);
                }

              g_object_set_data_full (G_OBJECT (dep), "NAME", g_strdup (name), g_free);

              girst_cache_set (name, dep);

              *found_new = TRUE;
            }
        }
    }
}

gint
main (gint   argc,
      gchar *argv[])
{
  g_autoptr(GOptionContext) context = NULL;
  g_autoptr(GHashTable) loaded = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GMainLoop) main_loop = NULL;

  girst_parser_ensure_types ();

  context = g_option_context_new ("GIRFILE... - Generate RST documentation from .gir files");
  g_option_context_add_main_entries (context, main_entries, NULL);
  g_option_context_add_group (context, g_irepository_get_option_group ());

  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_printerr ("%s\n", error->message);
      return EXIT_FAILURE;
    }

  if (argc == 1)
    {
      g_printerr ("%s requires one or more gir files.\n", argv[0]);
      return EXIT_FAILURE;
    }

  if (language != NULL && strcasecmp (language, "c") != 0)
    {
      g_printerr ("Only the C language is currently supported\n");
      return EXIT_FAILURE;
    }

  main_loop = g_main_loop_new (NULL, FALSE);

  g_irepository_prepend_search_path (".");
  g_irepository_require (g_irepository_get_default (), "Girst", "1.0", 0, NULL);

  loaded = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, NULL);

  /*
   * The first thing we want to do is parse all of the gir files that
   * were provided to us so that when we go to render the rst files, we
   * can resolve types across gir files.
   */
  for (guint i = 1; i < argc; i++)
    {
      const gchar *gir = argv[i];
      g_autoptr(GFile) file = g_file_new_for_commandline_arg (gir);
      g_autoptr(GirstParser) parser = girst_parser_new ();
      g_autoptr(GirstRepository) repository = NULL;
      GirstNamespace *ns;
      const gchar *name;

      if (!(repository = girst_parser_parse_file (parser, file, NULL, &error)))
        {
          g_printerr ("%s: Error: %s\n", gir, error->message);
          return EXIT_FAILURE;
        }

      ns = GIRST_NAMESPACE (girst_parser_object_first_typed (GIRST_PARSER_OBJECT (repository), GIRST_TYPE_NAMESPACE));
      g_return_val_if_fail (ns, EXIT_FAILURE);

      name = girst_namespace_get_name (ns);
      g_return_val_if_fail (name, EXIT_FAILURE);

      g_object_set_data_full (G_OBJECT (repository), "NAME", g_strdup (name), g_free);

      girst_cache_set (name, repository);
    }

  /* Now try and load any dependencies */
  if (girdirs != NULL)
    {
      for (;;)
        {
          gboolean found_new = FALSE;
          girst_cache_foreach (load_dependencies_cb, &found_new);
          if (found_new)
            continue;
          break;
        }
    }

  girst_cache_foreach (foreach_repository_cb, main_loop);

  g_main_loop_run (main_loop);

  return exit_code;
}
