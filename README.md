# gir2rst

This tool is used to generate Sphinx-style reStructuredText from a GObject
Introspection file (.gir). It uses a custom parser in C for the .gir files and
uses Template-GLib to generate the .rst files.

## Installation

```sh
meson build
cd build
ninja install
```

## Usage

```sh
gir2rst --outdir=generated /usr/share/gir-1.0/*.gir
```

You'll probably want to setup a sphinx conf.py or integrate the .rst files with
your existing documentation.

## Supported Languages

Currently, only C is supported.

